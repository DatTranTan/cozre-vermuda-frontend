<div id="top"></div>
<br />
<div align="center">
  <h2 align="center">Cozre</h2>
  <p align="center">
  </p>
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
        <li><a href="#branches">Branches</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#how-to-deploy">How To Deploy</a></li>
    <li><a href="#release-information">Release Infomation</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

##### Manage page

- Create User.
- Create Event (users can go to the LandingPage link of the Event to spin prizes).
- Add prize Video.
- Create Questionnaire (when entering form to create customer)
- Manage LandingPage templates.
- Manage Customer spun prizes.

##### Landing page

- Show Video in Event and show product in Event.
- Show the corresponding landing page based on the template.
- After winning user click a link to page web.

### Built With

This project was bootstrapped with:

- [React.js](https://reactjs.org/)
- [Antd Design](https://ant.design/)
- [Styled-Component](https://styled-components.com/)
- [GraphQL](https://graphql.org/)

### Branches

- `master` : The branch with the latest code has been approved and deployed to production enviroment.
- `develop` : The branch with the latest code in development enviroment.
<p align="right">(<a href="#top">back to top</a>)</p>

<!-- GETTING STARTED -->

## Getting Started

To get a local copy up and running follow these simple example steps.

### Prerequisites

- npm / yarn

### Installation

1. Clone the repo
   ```sh
   ```with HTTPS```   git clone https://gitlab.com/vermuda-llc/cozre-vermuda-frontend.git
   ```with SSH```   git@gitlab.com:vermuda-llc/cozre-vermuda-frontend.git
   ```
2. Install packages
   ```sh
   yarn
   ```
3. Run project
   - Run develop
   ```sh
   yarn dev
   ```
   - Build staging
   ```sh
   yarn build-stag
   ```

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- CONTRIBUTING -->

## Contributing

1. Clone the Project.
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`).
3. Commit your Changes (`git commit -m 'Add some code'`).
4. Push to the Branch (`git push origin feature/AmazingFeature`).
5. Create a Merge Request.
<p align="right">(<a href="#top">back to top</a>)</p>

<!-- How to deploy EXAMPLES -->

## How To Deploy

##### Staging

The latest code in the `develop` branch will be automatically deployed to vercel (`url staging`).

##### Production

Project has CI/CD configured. To deploy to production, follow these steps:

- From the commit which you want to deploy, create a new `tag`.
- The new tag is named `/^v*/` and is numbered based on the lastest tag (example: `v1.1.10`).
- After successful `build`, approve the `deploy to production` job to complete.
<p align="right">(<a href="#top">back to top</a>)</p>

<!-- Release Information -->

## Release Information

#### Staging

- URL: https://cozre-vermuda-frontend.vercel.app/login

#### Production

- URL:

<p align="right">(<a href="#top">back to top</a>)</p>
