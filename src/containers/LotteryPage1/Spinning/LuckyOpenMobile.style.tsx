import { styled } from 'baseui'
import { Button } from 'antd'

export const Wrapper = styled('div', () => ({
  textAlign: 'center',
  backgroundColor: '#fff',
  width: '100%',
  height: '100vh',
  background:
    'radial-gradient(circle, rgba(255,255,255,1) 0%, rgba(237,43,51,1) 100%)'
}))

export const EventTitleWrapper = styled('div', () => ({
  width: '100%',
  height: '20vh',
  position: 'absolute',
  top: 0,
  left: 0,
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  padding: '0 0.3rem'
}))

export const NameEvent = styled('div', () => ({
  fontSize: '4.5rem',
  color: '#fd0711',
  fontWeight: 'bold',
  marginBottom: '0.5rem',

  '@media only screen and (max-width: 1440px)': {
    fontSize: '4rem',
    marginBottom: '0.1rem'
  },

  textShadow: `2px 0 0 #fff, -2px 0 0 #fff, 0 2px 0 #fff, 0 -2px 0 #fff,
    1px 1px #fff, -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff`,

  '@media only screen and (max-width: 768px)': {
    fontSize: '3.7rem'
  }
}))

export const StatusEvent = styled('div', () => ({
  fontSize: '2.4rem',
  color: '#26578a',
  fontWeight: 'bold',

  '@media only screen and (max-width: 768px)': {
    fontSize: '2.2rem'
  }
}))

export const VideoWrapper = styled('div', () => ({
  height: 'calc(100% - 40vh)',
  display: 'flex',
  justifyContent: 'center',
  alignContent: 'center',
  alignItems: 'center',

  '@media only screen and (max-width: 1024px) and (max-height: 768px)': {
    height: 'calc(100% - 30vh)',
    width: '100%'
  },

  '@media only screen and (max-width: 1024px)': {
    height: 'calc(100% - 30vh)',
    width: '100%'
  }
}))

export const VideoCustom = styled('video', () => ({
  height: 'calc(100% - 3rem)',
  width: '50%',
  display: 'block',
  objectFit: 'cover',

  '@media only screen and (max-width: 1440px)': {
    height: 'calc(100% - 3rem)',
    width: '55%'
  },

  '@media only screen and (max-width: 1024px) and (max-height: 780px)': {
    height: 'calc(100% - 7rem)',
    width: '60%'
  },

  '@media only screen and (max-width: 1024px)': {
    height: 'calc(70% - 3rem)',
    width: '90%'
  },

  '@media only screen and (max-width: 768px)': {
    height: 'calc(70% - 3rem)',
    width: '90%'
  }
}))

export const ButtonWrapper = styled('div', () => ({
  width: '100%',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center'
}))

export const PrizeWrapper = styled('div', () => ({
  position: 'relative',
  height: 'calc(100% - 1rem)',
  width: '50%',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: '#ead608',

  '@media only screen and (max-width: 1440px)': {
    height: 'calc(100% - 1rem)',
    width: '60%'
  },

  // '@media only screen and (max-width: 1280px)': {
  //   height: '100%',
  //   width: '60%'
  // },

  '@media only screen and (max-width: 1024px) and (max-height: 780px)': {
    height: 'calc(100% - 7rem)',
    width: '60%'
  },

  '@media only screen and (max-width: 1024px)': {
    height: 'calc(70% - 3rem)',
    width: '90%'
  },

  '@media only screen and (max-width: 768px)': {
    height: 'calc(70% - 3rem)',
    width: '90%'
  }
}))

export const WrapperArmorial = styled('div', () => ({
  position: 'absolute',
  width: '180px',
  height: '180px',
  top: 0,
  right: 0,

  '@media only screen and (max-width: 768px) and (max-height: 1024px)': {
    position: 'absolute',
    width: '160px',
    height: '160px',
    top: 0,
    right: 0
  }
}))

export const Ranking = styled('div', () => ({
  position: 'absolute',
  width: '100px',
  display: 'flex',
  justifyContent: 'center',
  top: '45px',
  right: '40px',
  color: '#ef3e46',
  fontWeight: 'bold',
  fontSize: '2rem',

  '@media only screen and (max-width: 768px) and (max-height: 1024px)': {
    top: '39px',
    right: '31px'
  }
}))
export const ImageWrapper = styled('div', () => ({
  width: '400px',
  height: '310px',

  '@media only screen and (max-width: 1280px)': {
    height: '250px'
  },
  '@media only screen and (max-width: 1024px) and (max-height: 768px)': {
    height: '200px'
  },

  '@media only screen and (max-width: 1024px)': {
    height: '320px'
  },

  '@media only screen and (max-width: 768px)': {
    height: '250px'
  }
}))

export const RankPrize = styled('div', () => ({
  fontSize: '2.7rem',
  color: '#fd0711',

  '@media only screen and (max-width: 1024px)': {
    fontSize: '2rem'
  }
}))

export const NamePrize = styled('div', () => ({
  fontSize: '2.7rem',
  color: ' #26578a',

  '@media only screen and (max-width: 1024px)': {
    fontSize: '2rem'
  }
}))

export const TriangleTop = styled('div', () => ({
  width: 0,
  height: 0,
  borderTop: '20vh solid #ead608',
  borderRight: '45vw solid transparent',

  '@media only screen and (max-width: 1440px)': {
    borderTop: '20vh solid #ead608',
    borderRight: '55vw solid transparent'
  },

  '@media only screen and (max-width: 1280px)': {
    borderTop: '20vh solid #ead608',
    borderRight: '55vw solid transparent'
  },

  '@media only screen and (max-width: 1024px)': {
    borderTop: '15vh solid #ead608',
    borderRight: '75vw solid transparent'
  }
}))

export const TriangleBottom = styled('div', () => ({
  position: 'absolute',
  bottom: 0,
  right: 0,
  width: 0,
  height: 0,
  borderBottom: '20vh solid #ead608',
  borderLeft: '45vw solid transparent',

  '@media only screen and (max-width: 1440px)': {
    borderBottom: '20vh solid #ead608',
    borderLeft: '55vw solid transparent'
  },

  '@media only screen and (max-width: 1280px)': {
    borderBottom: '20vh solid #ead608',
    borderLeft: '55vw solid transparent'
  },

  '@media only screen and (max-width: 1024px)': {
    borderBottom: '15vh solid #ead608',
    borderLeft: '75vw solid transparent'
  }
}))

export const ButtonCustom = styled(Button, () => ({
  fontSize: '25px',
  height: '60px',
  fontWeight: 'bold',

  '@media only screen and (max-width: 1024px) and (max-height: 768px)': {
    fontSize: '15px',
    height: '30px'
  },

  '@media only screen and (max-width: 768px)': {
    fontSize: '15px',
    height: '30px'
  }
}))

export const EventEndsTime = styled('div', () => ({
  position: 'fixed',
  bottom: ' 1.4rem',
  right: '2.8rem',
  zIndex: 1,
  fontSize: '2rem',
  fontWeight: 'bold',
  color: ' #fd0711',
  textShadow: `2px 0 0 #fff, -2px 0 0 #fff, 0 2px 0 #fff, 0 -2px 0 #fff,
    1px 1px #fff, -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff`,

  '@media only screen and (max-width: 1280px)': {
    fontSize: '1.8rem',
    bottom: ' 1rem',
    right: '2rem'
  },

  '@media only screen and (max-width: 1024px)': {
    fontSize: '1.5rem',
    bottom: ' 1rem',
    right: '2rem'
  }
}))
