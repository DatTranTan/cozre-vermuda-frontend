import React from 'react'

//import CongratulationsMB from './lucky-day/LuckyDay/LuckyOpenMobile'
import LuckyOpen from './Spinning/LuckyOpen'
import LuckyOpenIpad from './Spinning/LuckyOpenIpad'
import { MainContent } from './LotteryPage.style'
import { useDeviceType } from '../../settings/useDeviceType'

function LotteryPage() {
  const { mobile, tablet, desktop } = useDeviceType()
  console.log(mobile, tablet, desktop)

  return (
    <MainContent>
      {/* <Header /> */}
      {/* <Container> */}
      {/* <CongratulationsM /> */}
      {/* {mobile ? <LuckyOpen /> : <LuckyOpenIpad />} */}
      <LuckyOpenIpad />
      {/* {desktop && <LuckyOpen />} */}
      {/* {(mobile || tablet) && <CongratulationsM />}
      {desktop && <Congratulations />} */}
      {/* </Container> */}
      {/* <div id='footer' style={{ marginTop: '1rem' }}>
        <Footer />
      </div> */}
    </MainContent>
  )
}

export default LotteryPage
