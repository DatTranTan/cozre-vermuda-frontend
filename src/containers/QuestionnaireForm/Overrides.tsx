import { PLACEMENT } from 'baseui/popover'

export const OverideSelect = {
  Dropdown: {
    style: ({ $theme }) => ({
      maxHeight: '300px'
    })
  },
  DropdownContainer: {
    style: ({ $theme }) => ({
      position: 'absolute',
      left: '5px',
      minWidth: '300px',
      maxWidth: '500px',
      top: '2px'
    })
  },
  Placeholder: {
    style: ({ $theme }) => {
      return {
        ...$theme.typography.fontBold14,
        color: $theme.colors.textNormal
      }
    }
  },
  DropdownListItem: {
    style: ({ $theme }) => {
      return {
        ...$theme.typography.fontBold14,
        color: $theme.colors.textNormal
      }
    }
  },
  OptionContent: {
    style: ({ $theme, $selected }) => {
      return {
        ...$theme.typography.fontBold14,
        color: $selected ? $theme.colors.textDark : $theme.colors.textNormal
      }
    }
  },
  SingleValue: {
    style: ({ $theme }) => {
      return {
        ...$theme.typography.fontBold14,
        color: $theme.colors.textDark
      }
    }
  },
  Popover: {
    props: {
      placement: PLACEMENT.leftBottom,
      overrides: {
        Body: {
          style: { zIndex: 2 }
        }
        // Inner: {
        //   style: { width: '300px' }
        // }
      }
    }
  }
}

export const OverideCancel = {
  BaseButton: {
    style: ({ $theme }) => ({
      width: '50%',
      borderTopLeftRadius: '3px',
      borderTopRightRadius: '3px',
      borderBottomRightRadius: '3px',
      borderBottomLeftRadius: '3px',
      backgroundColor: '#f5f5f5',
      marginRight: '15px',
      color: $theme.colors.red400
    })
  }
}

export const OverideSave = {
  BaseButton: {
    style: ({ $theme }) => ({
      width: '50%',
      borderTopLeftRadius: '3px',
      borderTopRightRadius: '3px',
      borderBottomRightRadius: '3px',
      borderBottomLeftRadius: '3px'
    })
  }
}

export const OverideAdd = {
  BaseButton: {
    style: ({ $theme }) => ({
      width: '100%',
      borderTopLeftRadius: '3px',
      borderTopRightRadius: '3px',
      borderBottomRightRadius: '3px',
      borderBottomLeftRadius: '3px'
    })
  }
}
