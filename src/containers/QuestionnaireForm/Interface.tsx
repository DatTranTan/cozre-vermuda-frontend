export interface UserType {
  id: string
  role: string
  name: string
}

export interface Agency {
  id: string
  nameKanji: string
}

export interface UserFormInput {
  userid: string
  password: string
  namekanji: string
  company: string
  contact_number: string
  email: string
  address: string
  usertype: string
  agency: string
  maxClient: number
}

export interface User {
  id: string
  pwd: string
  nameKanji: string
  companyName: string
  userType: UserType
  tel: string
  email: string
  address: string
  agency: Agency
}

// value: [1, 2, 3]
// label: ['ADMIN', '代理店', '企業']
