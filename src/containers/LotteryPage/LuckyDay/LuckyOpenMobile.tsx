import React, { useEffect } from 'react'
import Confetti from 'react-confetti'
import Fade from 'react-reveal/Fade'
import { Wheel } from 'react-custom-roulette'
import {
  ButtonCustom,
  Input,
  Heading,
  Wrapper,
  Container,
  Divider,
  SpinnerContainer,
  InputContainer
} from './Roultte.style'
import { data } from './data'
import { Button } from 'baseui/button'

var useCanvas: any
export default function Congratulations() {
  const [serialCode, setSerialCode] = React.useState('123456789')
  const [loading, setLoading] = React.useState(false)
  const [spinning, setSpinning] = React.useState(false)
  const [prize, setPrize] = React.useState(false)
  const [position, setPosition] = React.useState(0)
  const [checkError, setcheckError] = React.useState(false)
  const [point, setPoint] = React.useState('')
  const [toggleReload, setToggleReload] = React.useState(false)
  const [spinCount, setSpinCount] = React.useState(0)
  const canvas = React.useRef()
  const serialCodeRef = React.useRef(null)

  useEffect(() => {
    useCanvas = canvas.current
    serialCodeRef.current.focus()
  }, [])

  const postSerialCode = () => {
    // setToggleReload(!toggleReload);
    if (spinning) return
    setSpinning(true)
    setSpinCount(spinCount + 1)
  }
  function checkPrizeInData(point: any) {
    for (let i = 0; i < data.length; i++) {
      if (parseInt(point) === parseInt(data[i].option)) {
        setPosition(i)
        break
      }
    }
  }

  const generateNumber = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1) + min)
  }

  const onCheckSerialCode = () => {
    const rand = generateNumber(1, 10)

    setLoading(true)
    checkPrizeInData('' + rand)
    setPoint('' + rand)
    setcheckError(false)
    postSerialCode()
    setLoading(false)
  }

  const drawCanvas = (
    text1: string,
    text2: string,
    text3: any,
    text4: string
  ) => {
    var ctx: any
    if (useCanvas.getContext) {
      ctx = useCanvas.getContext('2d')
      ctx.font = 'bold 14px Helvetica, Arial'
      ctx.fillStyle = 'blue'
      ctx.fillText(text1, 15, 15)
      ctx.fillText(text2, 12, 28)

      //Point
      ctx.fillStyle = 'FF6700'
      ctx.font = 'bold 31px Helvetica, Arial'

      if (parseInt(text3) < 10) {
        ctx.fillText(text3, 40, 57)
      } else {
        ctx.fillText(text3, 34, 57)
      }
      //point
      ctx.font = 'bold 14px Helvetica, Arial'
      ctx.fillText(text4, 23, 73)
      ctx.fillStyle = 'red'

      ctx.lineWidth = 1
      ctx.restore()
    }
  }
  const clearCanvas = () => {
    const ctx = useCanvas.getContext('2d')
    ctx.save()

    // Use the identity matrix while clearing the canvas
    ctx.setTransform(1, 0, 0, 1, 0, 0)
    ctx.clearRect(0, 0, useCanvas.width, useCanvas.height)

    // Restore the transform
    ctx.restore()
  }

  return (
    <Wrapper>
      <canvas
        style={{
          position: 'absolute',
          top: '19%',
          left: '40%',
          zIndex: 14,
          height: '40%',
          width: '60%'
        }}
        ref={canvas}
      />
      <Container>
        {!prize ? (
          <SpinnerContainer>
            <Wheel
              mustStartSpinning={spinning}
              prizeNumber={position}
              data={data}
              fontSize={20}
              textDistance={80}
              backgroundColors={['#3e3e3e', '#df3428']}
              textColors={['#ffffff']}
              onStopSpinning={function () {
                setTimeout(function () {
                  setPrize(true), setSpinning(false)
                }, 1000)
              }}
            />
          </SpinnerContainer>
        ) : (
          <div
            style={{
              display: 'flex',
              width: '100%',
              height: 'auto',
              alignItems: 'center',
              alignContent: 'center'
            }}
          >
            <Confetti
              id='confetti'
              gravity={0.1}
              run={prize}
              tweenDuration={2}
              style={{
                height: '75%',
                width: '100%',
                zIndex: 1
              }}
            />
            <div>
              <video
                height={'auto'}
                width={'90%'}
                src='https://media.giphy.com/media/XyIveZZnnuNwksEkKm/source.mp4'
                autoPlay={true}
                playsInline={true}
                muted={true}
                preload='yes'
                // controls={true}
                onEnded={() => {
                  setToggleReload(true)
                  drawCanvas('おめでとう', 'ございます!', point, 'ポイント')
                }}
              ></video>
            </div>
          </div>
        )}
        {/* 
        <div
          style={{
            display: 'flex',
            width: '100%',
            flexDirection: 'column',
            alignContent: 'center',
            alignItems: 'center',
          }}
        >
          <span style={{ marginLeft: 4, marginRight: 4 }}>
            <FormattedMessage
              id='orTextd'
              defaultMessage='シリアルコードの入力'
            />
          </span>
        </div> */}

        <InputContainer>
          <Button
            onClick={() => {
              if (toggleReload) {
                setPrize(false)
                setSpinning(false)
                clearCanvas()
                setSerialCode('123456789')
              } else {
                onCheckSerialCode()
              }
            }}
            disabled={(serialCode == '' || spinning || prize) && !toggleReload}
            overrides={{
              BaseButton: {
                style: ({ $theme }) => ({
                  width: '50%',
                  borderTopLeftRadius: '3px',
                  borderTopRightRadius: '3px',
                  borderBottomRightRadius: '3px',
                  borderBottomLeftRadius: '3px'
                })
              }
            }}
          >
            {toggleReload ? '再度抽選する' : '抽選スタート'}
          </Button>
        </InputContainer>
      </Container>
    </Wrapper>
  )
}
