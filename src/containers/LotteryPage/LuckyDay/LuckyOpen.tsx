import React, { useEffect, useState } from 'react'
import Confetti from 'react-confetti'
import Fade from 'react-reveal/Fade'
import { Button } from 'baseui/button'
import { Wheel } from 'react-custom-roulette'
import { data } from './data'

var useCanvas: any
var height = 500
var width = 200
export default function Congratulations() {
  const [serialCode, setSerialCode] = React.useState('123456789')
  const [loading, setLoading] = React.useState(false)
  const [spinning, setSpinning] = React.useState(false)
  const [prize, setPrize] = React.useState(false)
  const [position, setPosition] = React.useState(0)
  const [msgError, setMsgError] = React.useState('')
  const [checkError, setcheckError] = React.useState(false)
  const [point, setPoint] = React.useState('')
  const [toggleReload, setToggleReload] = React.useState(false)
  const [spinCount, setSpinCount] = React.useState(0)
  const canvas = React.useRef()

  useEffect(() => {
    useCanvas = canvas.current
    console.log(msgError)
  }, [msgError])

  const postSerialCode = () => {
    // setToggleReload(!toggleReload);
    if (spinning) return
    setSpinning(true)
    setSpinCount(spinCount + 1)
  }

  function checkPrizeInData(point: any) {
    for (let i = 0; i < data.length; i++) {
      if (parseInt(point) === parseInt(data[i].option)) {
        setPosition(i)
        break
      }
    }
  }

  const generateNumber = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1) + min)
  }

  const onCheckSerialCode = () => {
    const rand = generateNumber(1, 10)

    setLoading(true)
    checkPrizeInData('' + rand)
    setPoint('' + rand)
    setcheckError(false)
    postSerialCode()
    setLoading(false)
  }
  const drawCanvas = (
    text1: string,
    text2: string,
    text3: any,
    text4: string
  ) => {
    var ctx: any
    if (useCanvas.getContext) {
      ctx = useCanvas.getContext('2d')
      ctx.font = 'bold 32px Helvetica, Arial'
      ctx.fillStyle = 'blue'
      ctx.fillText(text1, 322, 150)
      ctx.fillText(text2, 315, 190)
      //point
      ctx.fillStyle = '#FF6700'
      ctx.font = 'bold 90px Helvetica, Arial'
      if (parseInt(text3) < 10) {
        ctx.fillText(text3, 380, 270)
      } else {
        ctx.fillText(text3, 350, 270)
      }

      //point
      ctx.font = 'bold 20px Helvetica, Arial'
      ctx.fillText(text4, 370, 310)

      ctx.fillStyle = 'red'

      ctx.lineWidth = 1
      ctx.restore()
    }
  }

  const clearCanvas = () => {
    const ctx = useCanvas.getContext('2d')
    ctx.save()

    // Use the identity matrix while clearing the canvas
    ctx.setTransform(1, 0, 0, 1, 0, 0)
    ctx.clearRect(0, 0, useCanvas.width, useCanvas.height)

    // Restore the transform
    ctx.restore()
  }

  return (
    <div>
      <canvas
        style={{
          width: width,
          position: 'absolute',
          top: 1,
          zIndex: 2
        }}
        ref={canvas}
        width={200}
        height={350}
      ></canvas>
      <div>
        {!prize ? (
          <div>
            <div
              style={{
                width: 200,
                height: 400,
                position: 'absolute',
                bottom: 170,
                right: 150
              }}
            >
              <div>
                <Wheel
                  mustStartSpinning={spinning}
                  prizeNumber={position}
                  data={data}
                  fontSize={20}
                  textDistance={80}
                  backgroundColors={['#3e3e3e', '#df3428']}
                  textColors={['#ffffff']}
                  onStopSpinning={() => {
                    setTimeout(() => {
                      setPrize(true)
                      setSpinning(false)
                    }, 1000)
                  }}
                />
              </div>
            </div>
          </div>
        ) : (
          <div
            style={{
              width: 200,
              height: 490
            }}
          >
            <Confetti
              id='confetti'
              gravity={0.1}
              run={true}
              tweenDuration={2}
              style={{
                width: 200,
                height: 490,
                zIndex: 1
              }}
            />
            <video
              style={{
                width: width,
                height: 490,
                zIndex: 0
              }}
              src='https://media.giphy.com/media/XyIveZZnnuNwksEkKm/source.mp4'
              autoPlay
              onEnded={() => {
                setToggleReload(true)
                drawCanvas('おめでとう', 'ございます!', point, 'ポイント')
              }}
            ></video>
          </div>
        )}
      </div>
      <div
        style={{
          position: 'absolute',
          bottom: '2%',
          left: '30%',
          right: '30%',
          width: '40%',
          display: prize && !toggleReload ? 'none' : 'flex',
          flexDirection: 'column',
          zIndex: 11
        }}
      >
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            width: '100%',
            alignContent: 'center',
            alignItems: 'center'
          }}
        >
          {/* <div
            style={{
              display: toggleReload ? 'none' : 'flex',
              width: '100%',
              flexDirection: 'column',
              alignContent: 'center',
              alignItems: 'center',
            }}
          >
            <span style={{ marginLeft: 4, marginRight: 4 }}>
              <FormattedMessage
                id='orTextd'
                defaultMessage='シリアルコードの入力'
              />
            </span>
          </div> */}
          {/* <ButtonCustom
            title={toggleReload ? '再度抽選する' : '抽選スタート'}
            loader={<Loader />}
            isLoading={loading}
            onClick={() => {
              if (toggleReload) {
                setPrize(false)
                setSpinning(false)
                clearCanvas()
                setSerialCode('123456789')
              } else {
                onCheckSerialCode(false)
              }
            }}
            content=
            disabled={(serialCode == '' || spinning || prize) && !toggleReload}
            style={{ color: '#fff', margin: 5, width: '100%' }}
          /> */}
          <Button
            onClick={() => {
              if (toggleReload) {
                setPrize(false)
                setSpinning(false)
                clearCanvas()
                setSerialCode('123456789')
              } else {
                onCheckSerialCode()
              }
            }}
            disabled={(serialCode == '' || spinning || prize) && !toggleReload}
            overrides={{
              BaseButton: {
                style: ({ $theme }) => ({
                  width: '50%',
                  borderTopLeftRadius: '3px',
                  borderTopRightRadius: '3px',
                  borderBottomRightRadius: '3px',
                  borderBottomLeftRadius: '3px'
                })
              }
            }}
          >
            {toggleReload ? '再度抽選する' : '抽選スタート'}
          </Button>
        </div>
      </div>
    </div>
  )
}
