import styled from 'styled-components';
import { Button } from 'antd';
export const Wrapper = styled.div`
  text-align: center;
  background-color: #fff;
`;

export const ButtonCustom = styled(Button)`
  &.google {
    background-color: #4285f4;
  }

  &.facebook {
    background-color: #4267b2;
    margin-bottom: 10px;
  }

  @media (max-width: 767px) {
    font-size: 15px;
  }
`;

export const Container = styled.div`
  padding: 20px 5px 0;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  @media (max-width: 768px) {
    padding: 20px 5px 0;
    margin: 0;
  }
`;
export const ContainerVideo = styled.div`
  height: 60%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const SpinnerContainer = styled.div`
  // padding-top: 30rem !important;
  display: flex !important;
  flex-direction: column !important;
  height: auto !important;
  weight: 100% !important;
  top: 29rem !important;
  left: 10rem !important;
  /* width: 100vw; */
  @media (max-width: 768px) {
    padding: 5px 5px 0;
  }
  .eSwYtm {
    top: 1rem !important;
    left: 22rem !important;
  }
  .wheel > div:last-child {
    width: 50% !important;
    max-height: 700px !important;
  }
`;

export const InputContainer = styled.div`
  padding: 5px 5px 0;
  z-index: 1;
  @media (max-width: 768px) {
    padding: 5px 5px 0;
  }
`;

export const LogoWrapper = styled.div`
  margin-bottom: 30px;

  img {
    max-width: 160px;
  }
`;

export const Heading = styled.h3`
  color: #009e7f;
  margin-bottom: 10px;
  font-family: 'Poppins', sans-serif;
  font-size: 21px;
  font-weight: 700;

  @media (max-width: 767px) {
    font-size: 15px;
  }
`;

export const SubHeading = styled.span`
  margin-bottom: 40px;
  font-family: 'Lato', sans-serif;
  font-size: 15px;
  font-weight: 400;
  color: #77798c;
  display: block;

  @media (max-width: 767px) {
    font-size: 15px;
  }
`;

export const OfferSection = styled.div`
  padding: 20px;
  background-color: #f7f7f7;
  color: #009e7f;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Offer = styled.p`
  font-family: 'Lato', sans-serif;
  font-size: 15px;
  font-weight: 400;
  margin: 0;

  @media (max-width: 767px) {
    font-size: 15px;
  }
`;

export const HelperText = styled.p`
  font-family: 'Lato', sans-serif;
  font-size: 15px;
  font-weight: 400;
  color: #77798c;
  margin: 0;
  text-align: center;
  width: 100%;

  a {
    font-weight: 700;
    color: #4285f4;
    text-decoration: underline;
  }
`;

export const Input = styled.input`
  width: 100%;
  height: 48px;
  border-radius: 0px;
  background-color: #f7f7f7;
  border: 1px solid#E6E6E6;
  font-family: 'Lato', sans-serif;
  font-size: 15px;
  font-weight: 400;
  color: #0d1136;
  line-height: 19px;
  padding: 0 18px;
  box-sizing: border-box;
  transition: border-color 0.25s ease;
  margin-bottom: 20px;

  &:hover,
  &:focus {
    outline: 0;
  }

  &:focus {
    border-color: #009e7f;
  }

  &::placeholder {
    color: #77798c;
    font-size: 14px;
  }

  &::-webkit-inner-spin-button,
  &::-webkit-outer-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  &.disabled {
    .inner-wrap {
      cursor: not-allowed;
      opacity: 0.6;
    }
  }

  @media (max-width: 767px) {
    font-size: 14px;
  }
`;

export const Divider = styled.div`
  padding: 2px 0;
  width: 80%;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;

  span {
    font-family: 'Lato', sans-serif;
    font-size: 15px;
    font-weight: 400;
    color: #0d1136;
    line-height: 2;
    z-index: 1;
    position: relative;
    padding: 0 10px;
  }

  &::before {
    content: '';
    width: 100%;
    height: 1px;
    background-color: #e6e6e6;
    position: absolute;
    top: 50%;
  }
`;

export const LinkButton = styled.button`
  background-color: transparent;
  border: 0;
  outline: 0;
  box-shadow: none;
  padding: 0;
  font-size: 14px;
  font-weight: 700;
  color: #009e7f;
  text-decoration: underline;
  cursor: pointer;

  @media (max-width: 767px) {
    font-size: 15px;
  }
`;
