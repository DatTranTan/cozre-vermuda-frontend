/* eslint-disable */
/* eslint-disable react-hooks/exhaustive-deps */
import { useMutation, useQuery } from '@apollo/react-hooks'
import { Button } from 'antd'
import queryString from 'query-string'
import React, { useEffect, useState } from 'react'
import { useLocation } from 'react-router-dom'
import { MUTATION_SPIN } from '../../../graphql/mutation/spin'
import { styled, useStyletron } from 'baseui'
import dayjs from 'dayjs'
import Prize from './prize.png'
// import Confetti from 'react-confetti'
import { ReloadOutlined } from '@ant-design/icons'
import Gift from '../../../assets/images/Lucky.png'
import {
  ContainerVideo,
  Wrapper,
  TriangleTop,
  TriangleBottom,
  WrapperTitleEvent,
  NameEvent,
  StatusEvent,
  EventEndsTime,
  WrapperVideo,
  WrapperButton,
  WrapperPrize,
  RankPrize,
  NamePrize,
  WrapperImg,
  WrapperArmorial,
  Ranking,
  VideoXXX,
  ImageXXX,
  ImageGift,
  ModalCustom,
  DemoDiv,
  DemoDiv1,
  DemoDiv2,
  DemoDiv3,
  BlockSunLight,
  ImgSun,
  LightImage,
  LightImage1,
  LightImage2,
  LightImage3,
  Balloons,
  DivLamp,
  LampOut,
  LampIn
} from './LuckyOpenIpad.style'
import { Notification } from '../../../components/Notification/NotificationCustom'
import { QUERY_EVENT_VIDEO } from '../../../graphql/query/event'
import Modal from 'antd/lib/modal/Modal'
import FormPrize from '../Form/FormPrize'
import confetti from 'canvas-confetti'
import AOS from 'aos'
import 'aos/dist/aos.css'
import backgroundSpin from '../../../assets/images/background-spin.png'
import lightStart from '../../../assets/images/star.png'
import SunLight from '../../../assets/images/sunLight.png'
import lanterns from '../../../assets/images/lanterns.png'
import { v4 as uuidv4 } from 'uuid'
import Cookies from 'universal-cookie'
import { MUTATION_SPUN } from '../../../graphql/mutation/spun'

import Balloon from '../../../assets/images/Soft_Pink_Balloon_PNG_Clipart.png'

var duration = 5 * 1000
var end
var useCanvas: any
export default function LuckyOpenIpad() {
  const NewID = uuidv4()
  const cookies = new Cookies()

  console.log(NewID, 'Log_UUDIV4')

  let customerID
  async function clickCookies() {
    ;(await cookies.get('userID')) == undefined && cookies.set('userID', NewID)
    if (!customerID) customerID = await cookies.get('userID')
  }

  // var customerID
  console.log(cookies.get('userID'), 'SSSSSSSSSSSSSSSSSS')

  AOS.init()
  const [css] = useStyletron()

  const location = useLocation()
  const eventId = queryString.parse(location.search).eventId
  const [showPrize, setShowPrize] = React.useState(true)
  // const [confetti, setConfetti] = React.useState(false)
  const [play, setPlay] = useState(false)
  const [reload, setReload] = useState(false)
  const [visibleForm, setvisibleForm] = React.useState(false)
  const [idspin, setIdspin] = useState('')

  const [dataSpinByEvent, setDataSpinByEvent] = useState<any>()

  const [wait, setWait] = useState(true)

  const [checkSpun] = useMutation(MUTATION_SPUN, {
    fetchPolicy: 'no-cache'
  })

  const canvas = React.useRef()
  function randomInRange(min, max) {
    return Math.random() * (max - min) + min
  }
  const cannon = () => {
    // confetti({
    //   particleCount: 100,
    //   spread: 70,
    //   origin: { y: 0.6 },
    //   ticks :3000
    // });
    // confetti({
    //   angle: randomInRange(55, 125),
    //   spread: randomInRange(50, 70),
    //   particleCount: randomInRange(50, 100),
    //   origin: { y: 0.6 }
    // });
    confetti({
      particleCount: 7,
      angle: 60,
      spread: 55,
      origin: { x: 0, y: 0.7 },
      resize: true,
      zIndex: 10000
    })
    // and launch a few from the right edge
    confetti({
      particleCount: 7,
      angle: 120,
      spread: 55,
      origin: { x: 1, y: 0.7 },
      resize: 100,
      zIndex: 10000
    })
    if (Date.now() < end) {
      requestAnimationFrame(cannon)
    }
  }

  const { data } = useQuery(QUERY_EVENT_VIDEO, {
    variables: {
      id: eventId
    }
  })

  const [spinCircle] = useMutation(MUTATION_SPIN, {
    fetchPolicy: 'no-cache'
  })
  useEffect(() => {
    useCanvas = canvas.current
  }, [])
  console.log(customerID, eventId, 'GGGGGGGGG')

  const reloadButton = () => {
    checkSpun({
      variables: {
        eventId,
        userId: customerID
      }
    }).then(({ data }) => {
      console.log(data, 'checkSpun')
      data?.checkSpun == true && setPlay(false)
    })
    spinCircle({
      variables: {
        eventId,
        userId: customerID
      }
    })
      .then(({ data }) => {
        setDataSpinByEvent(data.spin)
        console.log(data)
        setIdspin(data.spin.id)
      })
      .catch(({ graphQLErrors }) => {
        const mess = graphQLErrors.map((item) => item.message)
        if (mess.includes('このイベントはまだ開始されていません')) {
          Notification({
            type: 'error',
            message: ' エラー',
            description: 'イベントはまだ開始されていません。'
          })
          setShowPrize(true)
          setPlay(false)
        } else if (mess.includes('このイベントは既に終了しました')) {
          Notification({
            type: 'error',
            message: ' エラー',
            description: 'このイベントは既に終了しました'
          })
          setShowPrize(true)
          setPlay(false)
        } else {
          Notification({
            type: 'error',
            message: ' エラー',
            description: graphQLErrors[0]?.message
          })
        }
      })
  }

  // console.log(dataSpinByEvent?.id, idspin, 'testIdspin')

  return (
    <Wrapper>
      {/* <Balloons>
        <img
          src={Balloon}
          alt=''
          style={{ objectFit: 'contain', width: '100%', height: 'auto' }}
        />
      </Balloons> */}
      <div
        style={{
          width: '100%',
          backgroundImage: 'url(' + backgroundSpin + ')',
          backgroundSize: 'cover'
          // background: 'radial-gradient(circle, #fff 0%,#c10621 100%)'
        }}
      >
        <DivLamp>
          <div>
            <LampOut src={lanterns} alt='' />
          </div>
          <div>
            <LampIn src={lanterns} alt='' />
          </div>
        </DivLamp>

        <div style={{ width: '100%' }}>
          <TriangleTop />
        </div>
        <DemoDiv>
          <LightImage src={lightStart} alt='' />
        </DemoDiv>
        <WrapperTitleEvent>
          <NameEvent data-aos={'zoom-in'} data-aos-easing={'ease-in-sine'}>
            {data ? data.event.name : ''}
          </NameEvent>
          <StatusEvent
            data-aos={'zoom-in'}
            data-aos-duration={'300'}
            data-aos-easing={'ease-in-sine'}
          >
            {showPrize
              ? 'ただいま抽選中!!'
              : dataSpinByEvent?.rank !== 0 && 'ご当選おめでとうございます!!'}
          </StatusEvent>
        </WrapperTitleEvent>
        <DemoDiv1>
          <LightImage1 src={lightStart} alt='' />
        </DemoDiv1>
        <ContainerVideo>
          <WrapperVideo>
            {!showPrize ? (
              <WrapperPrize>
                {dataSpinByEvent?.rank !== 0 ? (
                  <>
                    <WrapperArmorial>
                      <ImageXXX alt='' src={Prize} />
                      <Ranking>
                        {dataSpinByEvent ? `${dataSpinByEvent.rank}` : ''}
                      </Ranking>
                    </WrapperArmorial>
                    <WrapperImg>
                      <img
                        src={dataSpinByEvent ? dataSpinByEvent.imageUrl : ''}
                        alt='prize'
                        width='100%'
                        height='100%'
                        style={{
                          objectFit: 'contain'
                        }}
                      />
                    </WrapperImg>
                    <RankPrize>
                      {dataSpinByEvent ? `${dataSpinByEvent.rank}等賞` : ''}
                    </RankPrize>
                    <NamePrize>
                      {' '}
                      {dataSpinByEvent ? dataSpinByEvent.name : ''}
                    </NamePrize>
                  </>
                ) : (
                  <NamePrize>はずれ...</NamePrize>
                )}
              </WrapperPrize>
            ) : (
              <>
                {play === false ? (
                  <BlockSunLight>
                    <ImageGift src={Gift} alt='' />
                    <ImgSun src={SunLight} alt='' />
                  </BlockSunLight>
                ) : (
                  <VideoXXX
                    src={
                      data?.event.video !== null &&
                      data?.event.video !== undefined
                        ? data?.event.video?.url
                        : 'https://media.giphy.com/media/XyIveZZnnuNwksEkKm/source.mp4'
                    }
                    autoPlay={true}
                    playsInline={true}
                    muted={true}
                    preload='yes'
                    controls={false}
                    onEnded={() => {
                      setShowPrize(false)
                      setTimeout(() => {
                        dataSpinByEvent?.rank !== 0 && setvisibleForm(true)
                        setPlay(false)
                      }, 3000)
                      if (dataSpinByEvent?.rank !== 0) {
                        end = Date.now() + duration
                        cannon()
                        // setConfetti(true)
                        // setTimeout(() => {
                        //   setWait(false)
                        // }, 3000)
                      }
                    }}
                  />
                )}
              </>
            )}
          </WrapperVideo>
        </ContainerVideo>
        <DemoDiv2>
          <LightImage2 src={lightStart} alt='' />
        </DemoDiv2>
        <DemoDiv3>
          <LightImage3 src={lightStart} alt='' />
        </DemoDiv3>
        <WrapperButton id='videos'>
          <Button
            type='primary'
            shape='round'
            disabled={play}
            className={css({
              fontSize: '25px',
              height: '60px',
              fontWeight: 'bold',
              width: '18rem',

              '@media only screen and (max-width: 768px)': {
                fontSize: '15px',
                height: '45px',
                fontWeight: 'bold',
                width: '12rem'
              },
              '@media only screen and (max-width: 414px)': {
                width: '11rem'
              }
              // '@media only screen and (max-width: 1024px) and (max-height: 768px)': {
              //   fontSize: '15px',
              //   height: '30px',
              //   fontWeight: 'bold'
              // }
            })}
            danger
            icon={reload === true && <ReloadOutlined />}
            onClick={async () => {
              await clickCookies()
              reloadButton()
              setShowPrize(true)
              setPlay(true)
              // setConfetti(false)
              setReload(true)

              // setIdspin(dataSpinByEvent?.id)
            }}
          >
            {reload === false ? '抽選スタート' : 'もう一回抽選する'}
          </Button>
        </WrapperButton>
        <div
          style={{ width: '100%', display: 'flex', justifyContent: 'flex-end' }}
        >
          <TriangleBottom />
        </div>
        <ModalCustom
          title={
            <div
              className={css({
                fontSize: '1.7rem',
                fontWeight: 900,
                color: 'rgb(52 45 121)',
                '@media only screen and (max-width:500px)': {
                  fontSize: '1.4rem'
                }
              })}
            >
              ご当選者情報のフォーム
            </div>
          }
          visible={visibleForm}
          onCancel={() => setvisibleForm(false)}
          width={
            window.innerWidth < 768
              ? '100%'
              : window.innerWidth === 768
              ? '70%'
              : '50%'
          }
          footer={false}
          maskClosable={false}
          centered
          destroyOnClose
        >
          <FormPrize setVisible={setvisibleForm} idspin={idspin} />
        </ModalCustom>
      </div>
    </Wrapper>
  )
}
