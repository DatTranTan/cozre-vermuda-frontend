import { styled } from 'baseui'
import Img from 'react-image'

export const WrapperFooter = styled('div', () => ({
  width: '100%',
  display: 'flex',
  justifyContent: 'center',
  height: '9rem',
  // backgroundColor: ' #3B3B3B',
  backgroundColor: ' #c10621',
  color: '#E1E1E1'
}))

export const FooterLayout = styled('div', () => ({
  width: '100%',
  height: '100%',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center'
  // backgroundColor: ' #c10621'
}))

export const ImgRight = styled('div', () => ({}))
export const ImgLeft = styled('div', () => ({}))

export const imgCustom = styled(Img, () => ({}))
