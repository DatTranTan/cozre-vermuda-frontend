import React from 'react'
import { HeaderWrapper, LeftTitle, RightTitle } from './Header.style'
import { FormBody } from '../LotteryPage.style'
import HeaderBird from '../../../assets/images/bird.png'

function Header() {
  return (
    <HeaderWrapper
      style={{
        backgroundImage: 'url(' + HeaderBird + ')',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat'
      }}
    >
      <FormBody>
        <LeftTitle>
        コズレ
          <RightTitle>抽選管理者ページ</RightTitle>
        </LeftTitle>
      </FormBody>
    </HeaderWrapper>
  )
}

export default Header
