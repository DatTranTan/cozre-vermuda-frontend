/* eslint-disable */
import {
    colTitleRender,
    renderCreateDate,
    renderRowCenter,
    renderRowText,
    renderContentCampaign
  } from '../../components/TableCell/TableCellRender'
  
  export const columns: any = [
    {
      title: colTitleRender('項番'),
      dataIndex: 'idx',
      width: 60,
      ellipsis: true,
      align: 'center',
      render: (value: any) => renderRowCenter(value)
    },
    {
      title: colTitleRender('概要テンプレート名'),
      dataIndex: 'name',
      width: '70%',
      ellipsis: true,
      render: (value: any) => renderRowText(value)
    },
    {
      title: colTitleRender('作成日'),
      dataIndex: 'createdAt',
      width: '20%',
      ellipsis: true,
      align: 'center',
      render: (value: any) => renderCreateDate(value)
    },
    // {
    //   title: colTitleRender('内容'),
    //   dataIndex: 'content',
    //   width: 95,
    //   ellipsis: false,
    //   render: (value: any) => renderContentCampaign(value)
    // }
  ]
  
  