import { styled, withStyle } from 'baseui'
import { StyledBodyCell as BaseStyledCell } from 'baseui/table-grid'
import { StyledRoot } from 'baseui/list'
import { Col as Column } from '../../components/FlexBox/FlexBox'

export const Container = styled('div', () => ({
  // display: 'flex',
  // position: 'absolute',
  // height: 'calc(100% - 70px)',
  // flexDirection: 'column'
  // paddingBottom: '100px'
}))

export const Header = styled('header', () => ({
  display: 'flex',
  alignItems: 'center',
  flex: '0 1 auto',
  flexDirection: 'row',
  flexWrap: 'wrap',
  backgroundColor: '#FFF',
  padding: '0 1rem',
  marginBottom: '1.5rem',
  minHeight: '70px',
  boxShadow: '0 0 8px rgba(0, 0 ,0, 0.1)',

  '@media only screen and (max-width: 1024px)': {
    padding: '0 0.5rem'
  },

  '@media only screen and (max-width: 990px)': {
    marginBottom: '1rem'
  },

  '@media only screen and (max-width: 767px)': {
    marginBottom: '0.5rem'
  }
}))
export const Heading = styled('a', ({ $theme }) => ({
  ...$theme.typography.fontBold18,
  color: $theme.colors.textDark,
  marginLeft: '16px',
  '@media only screen and (max-width: 768px)': {
    marginLeft: '0'
  }
}))

export const ImageWrapper = styled('div', ({ $theme }) => ({
  width: '40px',
  height: '40px',
  overflow: 'hidden',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: '20px',
  backgroundColor: $theme.colors.backgroundF7,
  boxShadow: '0 0 5px rgba(0, 0 , 0, 0.05)'
}))

export const Icon = styled('span', () => ({
  width: '100%',
  height: 'auto'
}))

export const TableWrapper = styled('div', () => ({
  width: '100%'
}))

export const StyledCell = withStyle(BaseStyledCell, () => ({
  fontFamily: "'Lato', sans-serif",
  fontWeight: 400,
  color: '#161F6A !important',
  display: 'flex',
  alignSelf: 'center',
  justifyContent: 'flex-start',
  alignItems: 'center',
  wordWrap: 'break-word',
  padding: '0 10px',
  borderTop: '0.3px solid rgba(224, 224, 224, 0.6)',
  height: '62px',

  '@media only screen and (max-width: 1440px)': {
    height: '56px'
  },

  '@media only screen and (max-width: 1280px)': {
    height: '45px'
  },

  '@media only screen and (max-width: 1024px)': {
    height: '40px'
  },

  '@media only screen and (max-width: 767px)': {
    height: '50px'
  }
}))

export const StyledListItem = styled(StyledRoot, () => ({
  margin: 0,
  padding: '0 10px',
  borderBottom: '1px solid #ddd',
  alignContent: 'center',
  justifyContent: 'center',
  height: '10%',

  ':hover': {
    backgroundColor: 'rgb(246, 246, 246)'
  }
}))
export const StyledHead = styled('thead', () => ({
  boxShadow: 'rgba(0, 0, 0, 0.16) 0px 1px 4px'
}))

export const ToolWrapper = styled('div', () => ({
  justifyContent: 'flex-end',
  display: 'flex',
  flexDirection: 'row',
  marginBottom: '1rem',
  overflow: 'hidden'
}))

export const ActionWrapper = styled('div', () => ({
  display: 'flex',
  alignItems: 'center',

  span: {
    cursor: 'pointer',
    marginLeft: '1rem'
  },

  'span:hover': {
    color: '#40a9ff'
  }
}))

export const TitleCol = styled(Column, () => ({
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
  height: '100%',

  '@media only screen and (max-width: 767px)': {
    marginBottom: '10px',

    ':last-child': {
      marginBottom: 0
    }
  }
}))
export const ActionCol = styled(Column, () => ({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'flex-end',
  alignItems: 'center',

  '@media only screen and (max-width: 767px)': {
    marginBottom: '10px',

    ':last-child': {
      marginBottom: 0
    }
  },

  height: '100%'
}))
export const SearchWrapper = styled('div', () => ({
  width: '70%',

  '@media only screen and (max-width: 800px)': {
    width: '100%'
  }
}))
export const QuestionBox = styled('div', () => ({
  width: "100%",
  height: "auto",
  padding: "16px",
  borderRadius: "3px",
  backgroundColor: "#ffffff",
  border: "3px solid green",
}))

export const SelectWrapper = styled('div', () => ({
  paddingRight: '1rem',
  width: '40%',

  ':last-child': {
    paddingRight: 0
  },

  '@media only screen and (max-width: 1024px)': {
    paddingRight: '0.5rem'
  }
}))
