/* eslint-disable */
import React, { useState, useCallback, useRef } from "react";
import { useDrawerDispatch, useDrawerState } from "../../context/DrawerContext";
import { useDeviceType } from "../../settings/useDeviceType";
import { Table, Tooltip, Badge, Row, Col } from "antd";
import {
  Header,
  Heading,
  SearchWrapper,
  SelectWrapper,
  TableWrapper,
  Container,
} from "../OverviewTemplate/OverviewTemplate.style";
import {
  ActionCol,
  ActionWrapper,
  TitleCol,
} from "../OverviewTemplate/OverviewTemplate.style";
import Input from "../../components/Input/Input";
import {
  // HomeOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import { useMutation, useQuery } from "@apollo/react-hooks";
import { QUERY_CAMPAIGNS } from "../../graphql/query/campaigns";
import { DELETE_CAMPAIGNS } from "../../graphql/mutation/delete-camp";
import confirm from "antd/lib/modal/confirm";
import SeachText from "../../components/SeachText/BaseSeachText";
import { Notification } from "../../components/Notification/NotificationCustom";
import {
  AddIcon,
  EditIcon,
  DeleteIcon,
} from "../../components/ActionIcon/ActionIcon";
import Select from "../../components/Select/HeaderSelect";
import { columns } from "./Columns";

export default function Users() {
  const [searchText, setSearchText] = useState("");
  const isFinished = useDrawerState("isFinished");
  const [selectValue, setSelectValue] = useState([]);
  const [disableEdit, setDisableEdit] = useState(true);
  const [disableDelete, setDisableDelete] = useState(true);
  const [selectedRows, setSelectedRows] = useState([]);
  const [pageIndex, setPageIndex] = useState(0);

  const { tablet } = useDeviceType(window.navigator.userAgent);
  const pageSize = tablet ? 12 : 10;
  const { data, loading, error, refetch } = useQuery(QUERY_CAMPAIGNS, {
    variables: {
      name: searchText,
      offset: pageSize * pageIndex,
      limit: pageSize,
    },
    fetchPolicy: "network-only",
  });

  const [deleteQestion] = useMutation(DELETE_CAMPAIGNS, {
    fetchPolicy: "no-cache",
  });

  const campaignsData = data?.Campaigns?.campaign.map((item, idx) => ({
    ...item,
    idx: pageSize * pageIndex + idx + 1,
    key: item.id,
  }));

  const dispatch = useDrawerDispatch();

  const openAddCampaingForm = useCallback(
    (data) =>
      dispatch({
        type: "OPEN_DRAWER",
        drawerComponent: "OVERVIEW_TEMPLATE_FORM",
        data: data,
        actionType: "create",
      }),
    [dispatch]
  );
  const openEditCampaingForm = useCallback(
    (data) =>
      dispatch({
        type: "OPEN_DRAWER",
        drawerComponent: "OVERVIEW_TEMPLATE_FORM",
        data: data[0],
        actionType: "edit",
      }),
    [dispatch]
  );

  const total = data?.Campaigns.count ?? 0;

  const rowSelection = {
    selectedRowKeys: selectedRows.map((item) => item.key),
    onChange: (selectedRowKeys, selectedRows) => {
      setSelectedRows(selectedRows);
      if (selectedRowKeys.length > 1) {
        setDisableEdit(true);
        setDisableDelete(false);
      } else if (selectedRowKeys.length > 0) {
        setDisableEdit(false);
        setDisableDelete(false);
      } else {
        setDisableEdit(true);
        setDisableDelete(true);
      }
    },
  };

  const deleteHandler = () => {
    confirm({
      type: "warning",
      title: "確認",
      content:
        "選択された概要テンプレートを削除します。よろしいですか?",
      icon: <ExclamationCircleOutlined />,
      cancelText: "キャンセル",
      okText: "OK",
      onOk: () =>
        deleteQestion({
          variables: {
            ids: selectedRows.map((item) => item.id),
          },
        })
          .then(() => {
            setSelectedRows([]);
            setDisableDelete(true);
            setDisableEdit(true);
            refetch();
            Notification({
              type: "success",
              message: "成功",
              description: "削除に成功しました。",
            });
          })
          .catch(({ graphQLErrors }) => {
            console.log("graphQLErrors: ", graphQLErrors);
            Notification({
              type: "error",
              message: " エラー",
              description: graphQLErrors[0]?.message.includes(
                "a foreign key constraint fails"
              )
                ? "このアンケートはイベントに存在していますから、削除出来ません。"
                : graphQLErrors[0]?.message.includes("can not delete")
                ? "アンケートを追加した人しか編集出来ません。"
                : graphQLErrors[0]?.message,
            });
          }),
    });
  };

  React.useEffect(() => {
    refetch();
    setSelectedRows([]);
    setDisableDelete(true);
    setDisableEdit(true)
  }, [isFinished, refetch]);

  return (
    <>
      <Header>
        <TitleCol xl={6} lg={6} md={4}>
          <ActionWrapper>
            <AddIcon
              color={"#00C58D"}
              title={"追加する"}
              onClick={() => openAddCampaingForm("add")}
            />
            <EditIcon
              disabled={disableEdit}
              color={disableEdit && "#ccc"}
              title={"編集する"}
              onClick={
                !disableEdit
                  ? () => openEditCampaingForm(selectedRows)
                  : undefined
              }
            />
            <DeleteIcon
              disabled={disableDelete}
              color={disableDelete ? "#ccc" : "#f00"}
              onClick={!disableDelete ? () => deleteHandler() : undefined}
              title={"概要テンプレート削除"}
            />
          </ActionWrapper>
        </TitleCol>
        <ActionCol xl={6} lg={6} md={8}>
          <SearchWrapper>
            <SeachText
              setText={setSearchText}
              pageIndex={pageIndex}
              setPageIndex={setPageIndex}
            />
          </SearchWrapper>
        </ActionCol>
      </Header>
      <TableWrapper>
        <Table
          bordered
          loading={loading}
          style={{
            overflow: "auto",
            background: "white",
            border: "1px solid #ddd",
          }}
          rowSelection={{
            type: "checkbox",
            ...rowSelection,
          }}
          columns={columns}
          dataSource={campaignsData}
          // bordered
          pagination={{
            total: total,
            pageSize: pageSize,
            current: pageIndex + 1,
            onChange: (value) => {
              refetch({ offset: pageSize * value - 1, limit: pageSize }).then(
                () => {
                  setPageIndex(value - 1);
                }
              );
            },
            showSizeChanger: false,
            // onShowSizeChange: (current, size) => {
            //   setPageSize(size)
            //   console.log(current, size)
            // },
            position: ["bottomCenter"],
          }}
        />{" "}
      </TableWrapper>
    </>
  );
}
