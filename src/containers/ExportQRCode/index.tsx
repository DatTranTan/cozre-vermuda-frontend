/* eslint-disable */
import { PrinterOutlined, ReloadOutlined } from '@ant-design/icons'
import { useQuery } from '@apollo/react-hooks'
import { Result, Spin } from 'antd'
import Button from '../../components/Button/Button'
import QRCode from 'qrcode.react'
import React, { useEffect, useState } from 'react'
// import { QUERY_EVENTS } from '../../graphql/query/events'
import image from '../../assets/images/backgroundQR.png'
import { useDeviceType } from '../../settings/useDeviceType'
import giftIcon from '../../assets/images/giftIcon.png'
import {
  ButtonWrapper,
  Wrapper,
  QRCodeWrapper,
  ExportWrapper,
  PageWrapper
} from './export.style'
// import {
//   EventWrapper,
//   TitleWrapper,
//   DateWrapper,
//   PrimaryHeaderText,
//   PrimaryDateText
// } from './export.style'
import dayjs from 'dayjs'
import { QUERY_EVENT } from '../../graphql/query/event'
import { useLocation } from 'react-router-dom'
import * as qs from 'query-string'
import { LANDING_PAGE } from '../../settings/constants'

function ExportQRCode() {
  const location = useLocation()
  const search = qs.parse(location.search)
  const { desktop, tablet } = useDeviceType(window.navigator.userAgent)
  const { data, loading, error, refetch } = useQuery(QUERY_EVENT, {
    variables: {
      id: search.eventId
    },
    fetchPolicy: 'network-only'
  })
  const { id, name, startTime, endTime, template } = data?.event ?? {}
  let listQR = [
    {
      id: id,
      name: name,
      startTime: startTime,
      endTime: endTime,
      link: template?.id ? `${window.location.origin}/${template?.id }?eventId=${id}` :`${window.location.origin}${LANDING_PAGE}?eventId=${id}`
    }
  ]
  const [hideButton, setHideButton] = useState(true)
  console.log(data)

  function handleButton() {
    setHideButton(false)
    setTimeout(() => {
      window.print()
      setHideButton(true)
    }, 500)
  }

  return (
    <Wrapper style={{ height: `calc(100% * ${listQR.length})` }}>
      <Spin
        size={'large'}
        spinning={loading}
        style={{ marginTop: loading ? '40vh' : 0 }}
      >
        {!loading && data && (
          <ExportWrapper>
            {hideButton && (
              <ButtonWrapper>
                <Button onClick={handleButton}>
                  <PrinterOutlined />
                  &nbsp; 印刷
                </Button>
                {/* <Button
                  loading={loading}
                  icon={}
                  onClick={() => refetch()}
                >
                  Reload
                </Button> */}
              </ButtonWrapper>
            )}
            {listQR?.map((item) => {
              return (
                <PageWrapper>
                  {/* <img
                    src={image}
                    alt={'オンライン抽選管理ページ'}
                    height='100%'
                    style={{ position: 'absolute', zIndex: -1 }}
                  />
                  <TitleWrapper>
                    オンライン
                    <PrimaryHeaderText>抽選管理ページ</PrimaryHeaderText>{' '}
                  </TitleWrapper>
                  <EventWrapper>{item.name}</EventWrapper>
                  <DateWrapper>
                    <PrimaryDateText>
                      {dayjs(item.startTime).format('YYYY.MM.DD')}
                    </PrimaryDateText>
                    &nbsp; - &nbsp;
                    <PrimaryDateText>
                      {dayjs(item.endTime).format('YYYY.MM.DD')}
                    </PrimaryDateText>
                  </DateWrapper> */}
                  <QRCodeWrapper>
                    <QRCode
                      id='qr-code'
                      value={item.link}
                      size={
                        tablet
                          ? 570
                          : desktop && window.innerWidth > 1280
                          ? 475
                          : 400
                      }
                      level={'H'}
                      includeMargin={false}
                      imageSettings={{
                        src: giftIcon,
                        x: null,
                        y: null,
                        height: 64,
                        width: 64,
                        excavate: true
                      }}
                    />
                  </QRCodeWrapper>
                </PageWrapper>
              )
            })}
          </ExportWrapper>
        )}{' '}
        {!loading && error && (
          <Result
            style={{ paddingTop: '25vh' }}
            status='404'
            title='イベントに商品がありません。'
            // subTitle="イベントに商品がありません。"
          />
        )}
      </Spin>
    </Wrapper>
  )
}

export default ExportQRCode
