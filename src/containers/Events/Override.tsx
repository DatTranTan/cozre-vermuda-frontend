import { PLACEMENT } from 'baseui/popover'

export const OverideSelect = {
  Dropdown: {
    style: ({ $theme }) => ({
      maxHeight: '300px'
    })
  },
  DropdownContainer: {
    style: ({ $theme }) => ({
      minWidth: '300px',
      maxWidth: '500px'
    })
  },
  Placeholder: {
    style: ({ $theme }) => {
      return {
        ...$theme.typography.fontBold14,
        color: $theme.colors.textNormal
      }
    }
  },
  DropdownListItem: {
    style: ({ $theme }) => {
      return {
        ...$theme.typography.fontBold14,
        color: $theme.colors.textNormal
      }
    }
  },
  OptionContent: {
    style: ({ $theme, $selected }) => {
      return {
        ...$theme.typography.fontBold14,
        color: $selected ? $theme.colors.textDark : $theme.colors.textNormal
      }
    }
  },
  SingleValue: {
    style: ({ $theme }) => {
      return {
        ...$theme.typography.fontBold14,
        color: $theme.colors.textDark
      }
    }
  },
  Popover: {
    props: {
      overrides: {
        Body: {
          style: { zIndex: 5 }
        }
      }
    }
  }
}

export const OverideCancel = {
  BaseButton: {
    style: ({ $theme }) => ({
      width: '50%',
      borderTopLeftRadius: '3px',
      borderTopRightRadius: '3px',
      borderBottomRightRadius: '3px',
      borderBottomLeftRadius: '3px',
      backgroundColor: '#f5f5f5',
      marginRight: '15px',
      color: $theme.colors.red400
    })
  }
}
export const OverideReset = {
  BaseButton: {
    style: ({ $theme }) => ({
      width: '50%',
      borderTopLeftRadius: '3px',
      borderTopRightRadius: '3px',
      borderBottomRightRadius: '3px',
      borderBottomLeftRadius: '3px',
      backgroundColor: '#fff',
      marginRight: '15px',
      color: $theme.colors.red400
    })
  }
}
export const OverideSave = {
  BaseButton: {
    style: ({ $theme }) => ({
      width: '50%',
      borderTopLeftRadius: '3px',
      borderTopRightRadius: '3px',
      borderBottomRightRadius: '3px',
      borderBottomLeftRadius: '3px',
      marginLeft: '7px'
    })
  }
}
const getInputFontStyle = ({ $theme }) => {
  return {
    color: $theme.colors.textDark,
    ...$theme.typography.fontBold14
  }
}

export const OverideDatePicker = {
  Popover: {
    props: {
      overrides: {
        Body: {
          style: { zIndex: 10 }
        }
      }
    }
  },
  Input: {
    props: {
      overrides: {
        Input: {
          style: ({ $theme, $isFocused }) => {
            return {
              ...getInputFontStyle({ $theme })
            }
          }
        }
      }
    }
  },
  TimeSelect: {
    props: {
      overrides: {
        Select: {
          props: {
            overrides: {
              Dropdown: {
                style: ({ $theme }) => ({
                  maxHeight: '300px'
                })
              },
              DropdownContainer: {
                style: ({ $theme }) => ({
                  minWidth: '300px',
                  maxWidth: '500px'
                })
              },
              Placeholder: {
                style: ({ $theme }) => {
                  return {
                    ...$theme.typography.fontBold14,
                    color: $theme.colors.textNormal
                  }
                }
              },
              DropdownListItem: {
                style: ({ $theme }) => {
                  return {
                    ...$theme.typography.fontBold14,
                    color: $theme.colors.textNormal
                  }
                }
              },
              OptionContent: {
                style: ({ $theme, $selected }) => {
                  return {
                    ...$theme.typography.fontBold14,
                    color: $selected
                      ? $theme.colors.textDark
                      : $theme.colors.textNormal
                  }
                }
              },
              SingleValue: {
                style: ({ $theme }) => {
                  return {
                    ...$theme.typography.fontBold14,
                    color: $theme.colors.textDark
                  }
                }
              },
              Popover: {
                props: {
                  overrides: {
                    Body: {
                      style: { zIndex: 5 }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
export const OverideNameInput = {
  Root: {
    style: ({ $theme }) => ({
      paddingLeft: 0
    })
  },
  StartEnhancer: {
    style: ({ $theme, $isFocused }) => ({
      fontSize: $theme.sizing.scale550,
      color: $theme.colors.textDark,
      backgroundColor: $isFocused ? 'rgb(0, 197, 141)' : '#ddd',
      fontWeight: '600',
      lineHeight: 1.5,
      minWidth: '100px'
    })
  }
}
export const OverideNumberInput = {
  Root: {
    style: ({ $theme }) => ({
      paddingLeft: 0
    })
  },
  InputContainer: {
    style: ({ $theme, $isFocused, $disabled }) => ({
      minWidth: '100px',
      backgroundColor: $disabled
        ? 'rgb(238, 238, 238)'
        : $isFocused
        ? 'rgb(246, 246, 246)'
        : 'rgb(238, 238, 238)',
      borderTopColor: $isFocused ? $theme.colors.primary : '#EEE',
      borderLeftColor: $isFocused ? $theme.colors.primary : '#EEE',
      borderRightColor: $isFocused ? $theme.colors.primary : '#EEE',
      borderBottomColor: $isFocused ? $theme.colors.primary : '#EEE'
    })
  },
  StartEnhancer: {
    style: ({ $theme, $isFocused }) => ({
      fontSize: $theme.sizing.scale550,
      color: $theme.colors.textDark,
      backgroundColor: $isFocused ? 'rgb(0, 197, 141)' : '#ddd',
      fontWeight: '600',
      lineHeight: 1.5,
      minWidth: '100px'
    })
  }
}

export const OverideHeaderSelect = {
  Root: {
    style: ({ $theme, $isFocused, $width }) => ({
      width: $width > 1024 ? '30%' : '40%',
      border: $isFocused
        ? `2px solid ${$theme.colors.primary}`
        : '2px solid #ccc'
    })
  },
  InputContainer: {
    style: ({ $theme, $isFocused }) => ({
      borderTopColor: $isFocused ? '#f6f6f6' : '#eeeeee',
      borderBottomColor: $isFocused ? '#f6f6f6' : '#eeeeee',
      borderLeftColor: $isFocused ? '#f6f6f6' : '#eeeeee',
      borderRightColor: $isFocused ? '#f6f6f6' : '#eeeeee'
    })
  },
  ControlContainer: {
    style: ({ $theme, $isFocused }) => ({
      borderTopColor: $isFocused ? '#f6f6f6' : '#eeeeee',
      borderBottomColor: $isFocused ? '#f6f6f6' : '#eeeeee',
      borderLeftColor: $isFocused ? '#f6f6f6' : '#eeeeee',
      borderRightColor: $isFocused ? '#f6f6f6' : '#eeeeee'
    })
  },
  Dropdown: {
    style: ({ $theme }) => ({
      maxHeight: '300px'
    })
  },
  DropdownContainer: {
    style: ({ $theme }) => ({
      position: 'absolute',
      left: '5px',
      maxWidth: '300px',
      top: '2px'
    })
  },
  Placeholder: {
    style: ({ $theme }) => {
      return {
        ...$theme.typography.fontBold14,
        color: $theme.colors.textNormal
      }
    }
  },
  DropdownListItem: {
    style: ({ $theme }) => {
      return {
        ...$theme.typography.fontBold14,
        color: $theme.colors.textNormal
      }
    }
  },
  OptionContent: {
    style: ({ $theme, $selected }) => {
      return {
        ...$theme.typography.fontBold14,
        color: $selected ? $theme.colors.textDark : $theme.colors.textNormal
      }
    }
  },
  SingleValue: {
    style: ({ $theme }) => {
      return {
        ...$theme.typography.fontBold14,
        color: $theme.colors.textDark
      }
    }
  },
  Popover: {
    props: {
      placement: PLACEMENT.leftBottom,
      overrides: {
        Body: {
          style: { zIndex: 2 }
        }
        // Inner: {
        //   style: { width: '300px' }
        // }
      }
    }
  }
}
