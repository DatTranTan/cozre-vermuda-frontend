/* eslint-disable */
import { useLazyQuery, useMutation, useQuery } from "@apollo/react-hooks";
import { Checkbox, Row, Col } from "antd";
import { useStyletron } from "baseui";
import ja from "date-fns/locale/ja";
import dayjs from "dayjs";
import $ from "jquery";
import * as qs from "query-string";
import React, { useEffect, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { useHistory, useLocation } from "react-router-dom";
import { KIND } from "../../components/Button/Button";
import { DatePicker } from "../../components/DatePicker/DatePicker";
import {
  Error,
  FormFields,
  FormLabel,
} from "../../components/FormFields/FormFields";
import { QuillEditorWrapper } from "./ConfigQuillEditor.style";
import Input from "../../components/Input/Input";
import { Notification } from "../../components/Notification/NotificationCustom";
import Select from "../../components/Select/Select";
import Textarea from "../../components/Textarea/TextareaCustom";
import BannerUploader from "../../components/Uploader/BannerUploader";

import { QUERY_CAMPAIGNS, QUERY_CAMPAIGN } from "../../graphql/query/campaigns";
import { MUTATION_CREATE_CAMP } from "../../graphql/mutation/create-camp";
import { MUTATION_CREATE_EVENT } from "../../graphql/mutation/create-event";
import { MUTATION_UPDATE_EVENT } from "../../graphql/mutation/update-event";
import { QUERY_EVENT } from "../../graphql/query/event";
import { QUERY_QUESTIONNAIRE } from "../../graphql/query/questionnaire";
import { QUERY_TEMPLATES } from "../../graphql/query/template";
import { QUERY_VIDEO } from "../../graphql/query/video";
import { EVENT } from "../../settings/constants";
import addPhoto from "../../utils/S3/upload";
import { OverideSelect } from "../UserForm/Overrides";
import {
  ButtonCustom,
  ButtonGroup,
  ButtonWrapper,
  Column,
  Container,
  DateField,
  DateFieldsWrapper,
  EventFormWrapper,
  Form,
  Header,
  Heading2,
  InfoWrapper,
  PrizeWrapper,
  RequiredWrapper,
  Wrapper,
  GroupFieldWrapper,
  ContainerDateTime,
  DatePickerContainer,
} from "./EventForm.style";
import { Heading } from "./Events.style";
import { EventFormInput, PrizeInput } from "./Interface";
import { OverideCancel, OverideSave } from "./Override";
import PrizeForm from "./PrizeForm";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import QuillToolbar, {
  modules,
  formats,
} from "../../components/ToolbarQuill/ToolbarQuill";
import { CustomQuill } from "../../components/ToolbarQuill/CustomQuill";
import { isIE } from "react-device-detect";
import confirm from "antd/lib/modal/confirm";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import moment from "moment";
var myToday = dayjs().toDate();
export default function EventForm() {
  const history = useHistory();
  const location = useLocation();
  const [css] = useStyletron();
  let search = qs.parse(location.search);
  const isCreate = search.type === "create";
  const isEdit = search.type === "edit";
  const userRef = React.useRef<any>(null);
  const memoRef = React.useRef<any>(null);
  const selectVideoRef = React.useRef<HTMLInputElement | HTMLDivElement>(null);
  const selectCampaignRef = React.useRef<HTMLInputElement | HTMLDivElement>(
    null
  );
  const [dateStart, setDateStart] = useState<any>(myToday);
  const [dateEnd, setDateEnd] = useState<any>(myToday);
  const [prizes, setPrizes] = useState<[PrizeInput]>(null);
  const [videoLink, setVideoLink] = React.useState<any>(null);
  const [noPrizeVideoLink, setNoPrizeVideoLink] = useState<any[]>(null);
  const [question, setQuestion] = useState<any[]>(null);
  const [template, setTemplate] = useState<any[]>(null);
  // const [videoFailed, setVideoFailed] = useState<Value>(null)
  const [urlImages, setUrlImages] = useState(null);
  const [urlVideoBackground, setUrlVideoBackground] = useState(null);
  const [footerUrl1, setFooterUrl1] = useState(null);
  const [footerUrl2, setFooterUrl2] = useState(null);
  const [skip, setSkip] = useState("");
  const [searchTemplate, setSearchTemplate] = useState("");

  const [memoText, setMemoText] = useState("");
  const [searchText, setSearchText] = useState("");
  const [timeOut, setTimeOut] = useState(null);
  const [searchNoPrizeVideo, setSearchNoPrizeVideo] = useState("");
  const [searchQuestion, setSearchQuestion] = useState("");
  const [isUploading, setIsUploading] = useState(false);
  const [isUploading2, setIsUploading2] = useState(false);
  const [isUploading3, setIsUploading3] = useState(false);
  const [isUploading4, setIsUploading4] = useState(false);
  const UniqueID = Date.now() + "_" + Math.random().toString(36).substr(2, 34);
  const [albumName, setAlbumName] = useState(UniqueID);
  const [loadData, setLoadData] = useState({ loaded: 0, total: 0, unit: "" });
  const [loadData2, setLoadData2] = useState({ loaded: 0, total: 0, unit: "" });
  const [loadData3, setLoadData3] = useState({ loaded: 0, total: 0, unit: "" });
  const [loadData4, setLoadData4] = useState({ loaded: 0, total: 0, unit: "" });
  const [dataCamp, setDataCamp] = useState(null);
  const [selectValue, setSelectValue] = useState([]);
  //campaign state
  const [selectOption, setSelectOption] = useState<any>(null);
  const [dataEditor, setDataEditor] = useState<any>(null);
  const [fileName, setFileName] = useState<any>(null);
  const [searchCampaign, setSearchCampaign] = useState<any>(null);
  const [customerLostState, setCustomerLostState] = useState(false);

  const {
    register,
    handleSubmit,
    setValue,
    setError,
    errors,
    control,
    getValues,
  } = useForm<EventFormInput>({
    defaultValues: isCreate && {
      videoid: "",
      bannerUrl: "",
      videoBackground: "",
      footerUrl1: null,
      footerUrl2: null,
      startdate: Date.parse(myToday.toString()),
      enddate: Date.parse(myToday.toString()),
      checkDateStartandEnd: true,
      topBannerUrl: "",
      leftFooterBannerUrl: "",
      rightFooterBannerUrl: "",
      noPrizeVideoId: "",
      questionnaireId: "",
      customerLost: "",
      txtBtnWin: "",
      txtBtnLost: "",
      timeOut: null,
      spinLimit: null,
      campaignId: "",
    },
  });
  const callback1 = {
    onSuccess: (urls: any) => {
      setUrlImages(urls);
      setValue("bannerUrl", urls);
      setIsUploading(false);
    },
    onError: (urls: any) => {
      setIsUploading(false);
    },
    onLoad: (loaded: number, total: number, unit: string) => {
      setLoadData({ loaded: loaded, total: total, unit: unit });
    },
  };
  const callback2 = {
    onSuccess: (urls: any) => {
      setFooterUrl1(urls);
      setValue("footerUrl1", urls);
      setIsUploading2(false);
    },
    onError: (urls: any) => {
      setIsUploading2(false);
    },
    onLoad: (loaded: number, total: number, unit: string) => {
      setLoadData2({ loaded: loaded, total: total, unit: unit });
    },
  };
  const callback3 = {
    onSuccess: (urls: any) => {
      setFooterUrl2(urls);
      setValue("footerUrl2", urls);
      setIsUploading3(false);
    },
    onError: (urls: any) => {
      setIsUploading3(false);
    },
    onLoad: (loaded: number, total: number, unit: string) => {
      setLoadData3({ loaded: loaded, total: total, unit: unit });
    },
  };
  const callback4 = {
    onSuccess: (urls: any) => {
      setUrlVideoBackground(urls);
      setValue("videoBackground", urls);
      setIsUploading4(false);
    },
    onError: (urls: any) => {
      setIsUploading4(false);
    },
    onLoad: (loaded: number, total: number, unit: string) => {
      setLoadData4({ loaded: loaded, total: total, unit: unit });
    },
  };

  const handleUploader = (files: any, type: any) => {
    if (type === "banner") {
      setIsUploading(true);
      addPhoto(albumName, "vermuda-images/", files, callback1);
    }
    if (type === "footer1") {
      setIsUploading2(true);
      addPhoto(albumName, "vermuda-images/", files, callback2);
    }
    if (type === "footer2") {
      setIsUploading3(true);
      addPhoto(albumName, "vermuda-images/", files, callback3);
    }
    if (type === "videoBackground") {
      setIsUploading4(true);
      addPhoto(albumName, "vermuda-images/", files, callback4);
    }
  };

  const { data: questionnaire, loading: loadingQuestion } = useQuery(
    QUERY_QUESTIONNAIRE,
    {
      variables: {
        name: searchQuestion,
        offset: 0,
        limit: 0,
      },
      fetchPolicy: "network-only",
    }
  );
  const {
    data: dataCampaigns,
    loading: loadingCampaign,
    refetch: refetchCampaigns,
  } = useQuery(QUERY_CAMPAIGNS, {
    variables: {
      name: searchCampaign,
      offset: 0,
      limit: 0,
    },
    fetchPolicy: "no-cache",
  });
  const listCampaigns = dataCampaigns?.Campaigns?.campaign;
  const {
    data: dataDetail,
    loading: loadingDataDetail,
    refetch: refetchCampaignDetail,
  } = useQuery(QUERY_CAMPAIGN, {
    variables: {
      Id: "",
    },
    fetchPolicy: "no-cache",
  });

  const { data: templates, loading: loadingTemplates } = useQuery(
    QUERY_TEMPLATES,
    {
      variables: {
        name: "",
        offset: 0,
        limit: 0,
      },
      fetchPolicy: "network-only",
    }
  );

  const [createCamp, { loading: loadingCreateCampain }] = useMutation(
    MUTATION_CREATE_CAMP,
    {
      fetchPolicy: "no-cache",
    }
  );

  const listQuestion = questionnaire?.getAllQuestionnaire.questionnaires;
  const listTemplate = templates?.Templates?.templates;

  const [createEvent, { loading: createLoading }] = useMutation(
    MUTATION_CREATE_EVENT,
    {
      fetchPolicy: "no-cache",
    }
  );

  const [updateEvent, { loading: editLoading }] = useMutation(
    MUTATION_UPDATE_EVENT,
    {
      fetchPolicy: "no-cache",
    }
  );

  const [queryEventInfo, { data: eventdata }] = useLazyQuery(QUERY_EVENT, {
    fetchPolicy: "network-only",
    onCompleted: (data) => {
      let {
        name,
        startTime,
        endTime,
        video,
        noPrizeVideo,
        memo,
        banner,
        prizes,
        footerBanner,
        bannerUrl,
        videoBackground,
        footerBannerUrl,
        questionnaire,
        customerLost,
        timeOut,
        spinLimit,
        txtBtnWin,
        txtBtnLost,
        template,
        campaignContent,
      } = data?.event ?? {};
      setPrizes(
        prizes?.map((item: any) => ({
          id: item.id,
          name: item.name,
          rank: parseFloat(item.rank),
          rate: parseFloat(item.rate),
          quantity: parseFloat(item.quantity),
          message: item.message,
          imageUrl: item.imageUrl,
          url: item.url,
          createdAt:
            item.createdAt === item.updatedAt ? item.createdAt : item.updatedAt,
        }))
      );
      let startTimeParse = parseFloat(startTime);
      let endTimeParse = parseFloat(endTime);
      setValue("name", name);
      setValue("startdate", startTimeParse);
      setValue("enddate", endTimeParse);
      setDateStart(dayjs(startTimeParse).toDate());
      setDateEnd(dayjs(endTimeParse).toDate());
      setValue("videoid", video?.id);
      setVideoLink(video ? [video] : []);
      setValue("noPrizeVideoId", noPrizeVideo?.id);
      setNoPrizeVideoLink(noPrizeVideo ? [noPrizeVideo] : []);
      setValue("questionnaireId", questionnaire?.id);
      setQuestion(questionnaire ? [questionnaire] : []);
      setTemplate(template ? [template] : []);
      setValue("bannerUrl", banner);
      setValue("videoBackground", videoBackground);
      setValue("timeOut", timeOut);
      setValue("spinLimit", spinLimit);
      setValue("txtBtnWin", txtBtnWin);
      setValue("txtBtnLost", txtBtnLost);
      setTimeOut(timeOut);
      setUrlImages(banner);
      setUrlVideoBackground(videoBackground);
      setValue(
        "footerUrl1",
        data?.event.footerBanner !== null && footerBanner?.[0]
      );
      setFooterUrl1(data?.event.footerBanner !== null && footerBanner?.[0]);
      setFooterUrl2(data?.event.footerBanner !== null && footerBanner?.[1]);
      setValue(
        "footerUrl2",
        data?.event.footerBanner !== null && footerBanner?.[1]
      );
      setValue("topBannerUrl", bannerUrl);
      setValue(
        "leftFooterBannerUrl",
        data?.event.footerBannerUrl === null
          ? footerBannerUrl
          : footerBannerUrl[0]
      );
      setValue(
        "rightFooterBannerUrl",
        data?.event.footerBannerUrl === null
          ? footerBannerUrl
          : footerBannerUrl[1]
      );
      setValue("memo", memo);
      setMemoText(memo);
      setValue("customerLost", customerLost === "Win" ? true : false);
      setCustomerLostState(customerLost === "Win" ? true : false);
      // setValue("campaignContent", campaignContent);
      // setDataCamp(campaignContent);
      setDataEditor(campaignContent);
    },
  });

  useEffect(() => {
    setSelectOption(dataCamp ? [dataCamp] : []);
    setDataEditor(dataCamp?.content);
  }, [dataCamp]);

  useEffect(() => {
    let data = dataDetail?.Campaign.content;
    if (data) {
      setDataEditor(data);
    }
  }, [dataDetail]);

  const { data, loading } = useQuery(QUERY_VIDEO, {
    variables: {
      name: searchText,
      offset: 0,
      limit: 0,
    },
    fetchPolicy: "no-cache",
  });

  $("#button-submit").click(function (e) {
    setSkip("submit");
  });
  $("#button-childe").click(function (e) {
    setSkip("");
  });

  const handleConfirmCamp = () => {
    confirm({
      type: "warning",
      title: "確認",
      content: "現在の概要文を置き換えます。よろしいですか。",
      icon: <ExclamationCircleOutlined />,
      cancelText: "いいえ",
      okText: "はい",
      onOk: async () => {
        await setDataEditor(selectValue[0]?.content);
      },
    });
  };

  const handleCheck = async (data: EventFormInput) => {
    if (skip === "submit") {
      onSubmit(data);
    }
  };

  const ChangeContent = (e: any) => {
    setDataEditor(e);
  };

  const onSubmit = (data: EventFormInput) => {
    const prizeAdd = prizes.map((item, index) => ({
      id: item.id,
      name: item.name,
      rank: item.rank,
      rate: item.rate,
      quantity: item.quantity,
      message: item.message,
      imageUrl: item.imageUrl ?? "",
      url: item.url ?? "",
    }));

    const values = {
      id: search.eventId,
      name: data.name,
      memo: data.memo,
      startTime: data.startdate.toString(),
      endTime: data.enddate.toString(),
      videoId: data.videoid,
      noPrizeVideoId: data.noPrizeVideoId,
      questionnaireId: data.questionnaireId ?? null,
      banner: data.bannerUrl,
      videoBackground: data.videoBackground,
      footerBanner: [
        data.footerUrl1 ||
          "https://vermuda-images.s3-ap-northeast-1.amazonaws.com/vermuda-images/1641367747673_gpbq4sbblb/download.jpg",
        data.footerUrl2 ||
          "https://vermuda-images.s3-ap-northeast-1.amazonaws.com/vermuda-images/1641367747673_gpbq4sbblb/download.jpg",
      ],
      prizes: prizeAdd,
      bannerUrl: data.topBannerUrl,
      footerBannerUrl: [data.leftFooterBannerUrl, data.rightFooterBannerUrl],
      txtBtnWin: data.txtBtnWin,
      txtBtnLost: data.txtBtnLost,
      customerLost: customerLostState ? "Win" : "Lost",
      timeOut: Number(data.timeOut) > 0 ? Number(data.timeOut) : null,
      spinLimit: Number(data.spinLimit) > 0 ? Number(data.spinLimit) : null,
      templateId: template?.[0]?.id || null,
      campaignContent: dataEditor,
    };

    let checkStart = dayjs(data.startdate).isValid();
    let checkEnd = dayjs(data.enddate).isValid();

    let checkDateStartandEndBoolean = true;
    if (!checkStart) setError("startdate", "required");
    if (!checkEnd) setError("enddate", "required");
    // if (Date.parse(data.startdate) > Date.parse(data.enddate)) {
    //   checkDateStartandEnd = false
    // }
    if (data.startdate > data.enddate) {
      setError("checkDateStartandEnd", "change");
      checkDateStartandEndBoolean = false;
    }
    if (isCreate && checkStart && checkEnd && checkDateStartandEndBoolean) {
      createEvent({
        variables: {
          createEventInput: {
            ...values,
          },
        },
      })
        .then(() => {
          Notification({
            type: "success",
            message: "成功",
            description: " イベント追加に成功しました。",
          });
          history.push(EVENT);
        })
        .catch(({ graphQLErrors }) =>
          Notification({
            type: "error",
            message: " エラー",
            description: graphQLErrors[0]?.message,
          })
        );
    }
    if (isEdit && checkStart && checkEnd && checkDateStartandEndBoolean) {
      updateEvent({
        variables: {
          updateEventInput: {
            ...values,
            numberOfLotteryPeople: eventdata?.event?.numberOfLotteryPeople,
          },
        },
      })
        .then(() => {
          Notification({
            type: "success",
            message: "成功",
            description: "イベントの更新に成功しました。",
          });
          history.push(EVENT);
        })
        .catch(({ graphQLErrors }) =>
          Notification({
            type: "error",
            message: " エラー",
            description: graphQLErrors[0]?.message,
          })
        );
    }
  };

  useEffect(() => {
    if (localStorage.getItem("role") === "agency") {
      history.push("/event");
    }
  }, []);
  useEffect(() => {
    if (isEdit && search.eventId) {
      queryEventInfo({
        variables: {
          id: search.eventId,
        },
      });
    }
  }, [isEdit]);

  useEffect(() => {
    if (userRef.current) userRef.current.focus();
  }, [userRef]);

  useEffect(() => {
    register({ name: "name" }, { required: true, maxLength: 100 });
    register({ name: "startdate" }, { required: true });
    register({ name: "enddate" }, { required: true });
    register({ name: "videoid" }, { required: true });
    register({ name: "noPrizeVideoId" });
    register({ name: "footerUrl1" });
    register({ name: "footerUrl2" });
    register({ name: "bannerUrl" }, { required: true });
    register({ name: "videoBackground" }, { required: true });
    register({ name: "topBannerUrl" }, { maxLength: 500 });
    register({ name: "memo" }, { maxLength: 200 });
    register({ name: "txtBtnWin" }, { maxLength: 255 });
    register({ name: "txtBtnLost" }, { maxLength: 255 });
    register({ name: "leftFooterBannerUrl" }, { maxLength: 500 });
    register({ name: "rightFooterBannerUrl" }, { maxLength: 500 });
    register({ name: "questionnaireId" });
  }, [register]);

  const debounce = (fn: any, delay: number) => {
    return (args: any) => {
      clearTimeout(fn.id);

      fn.id = setTimeout(() => {
        fn.call(this, args);
      }, delay);
    };
  };
  const searchHandler = (search: any) => {
    // setSearchText(value)
    if (search?.input === 0) {
      setSearchText(search?.value);
    }
    if (search?.input === 1) {
      setSearchNoPrizeVideo(search?.value);
    }
    if (search?.input === 2) {
      setSearchQuestion(search?.value);
    }
    if (search?.input === 3) {
      setSearchCampaign(search?.value);
    }
  };

  const debounceAjax = debounce(searchHandler, 600);

  function disabledDate(current) {
    return current && current < moment().add(-1, "days");
  }
  return (
    <Container>
      <div>
        <Header>
          <Column lg={6} md={12}>
            <Heading to="/event">イベント管理 /&nbsp;</Heading>
            <Heading2>
              {isCreate ? "イベントの作成" : "イベントの更新"}
            </Heading2>
          </Column>
        </Header>
      </div>
      <div>
        <Wrapper>
          <RequiredWrapper>
            必要な情報を記入して下さい。
            <span style={{ color: "red" }}>(*)は必須の項目です。</span>
          </RequiredWrapper>
          <EventFormWrapper>
            <Form id="parent-form" onSubmit={handleSubmit(handleCheck)}>
              <InfoWrapper>
                <GroupFieldWrapper>
                  <FormFields>
                    <FormLabel>
                      イベント名&nbsp;<span style={{ color: "red" }}>*</span>
                    </FormLabel>
                    <Input
                      name="name"
                      inputRef={(e: any) => {
                        register(e, { required: true, maxLength: 100 });
                        userRef.current = e; // you can still assign to ref
                      }}
                    />
                    {errors.name && errors.name.type === "required" && (
                      <Error>(*)は必須の項目です。</Error>
                    )}
                    {errors.name && errors.name.type === "maxLength" && (
                      <Error>イベント名はは100文字以下設定してください。</Error>
                    )}
                  </FormFields>
                  <DateFieldsWrapper>
                    <DateField>
                      <FormFields>
                        <FormLabel>
                          開始日&nbsp;<span style={{ color: "red" }}>*</span>
                        </FormLabel>
                        <ContainerDateTime>
                          <DatePickerContainer
                            format="YYYY年MM月DD日 - HH:mm"
                            value={moment(dateStart)}
                            allowClear={false}
                            disabledDate={disabledDate}
                            showNow={false}
                            showTime={{
                              defaultValue: moment(dateStart, "HH:mm"),
                            }}
                            onOk={(date) => {
                              const dateParse = Array.isArray(date)
                                ? date[0]
                                : date;
                              if (dayjs(dateParse).isValid()) {
                                setValue("startdate", Date.parse(dateParse));
                                setDateStart(date);
                              }
                              if (errors.checkDateStartandEnd?.type) {
                                errors.checkDateStartandEnd.type = "changenew";
                              }
                            }}
                          />
                          {errors.startdate &&
                            errors.startdate.type === "required" && (
                              <Error>(*)は必須の項目です。</Error>
                            )}
                          {errors.checkDateStartandEnd &&
                            errors.checkDateStartandEnd.type === "change" && (
                              <Error>
                                開始日は終了日より前の日付にしてください。
                              </Error>
                            )}
                        </ContainerDateTime>
                      </FormFields>
                    </DateField>
                    <DateField>
                      <FormFields>
                        <FormLabel>
                          終了日&nbsp;<span style={{ color: "red" }}>*</span>
                        </FormLabel>
                        <ContainerDateTime>
                          <DatePickerContainer
                            format="YYYY年MM月DD日 - HH:mm"
                            value={moment(dateEnd)}
                            allowClear={false}
                            disabledDate={disabledDate}
                            showNow={false}
                            showTime={{
                              defaultValue: moment(dateEnd, "HH:mm"),
                            }}
                            onOk={(date) => {
                              const dateParse = Array.isArray(date)
                                ? date[0]
                                : date;
                              if (dayjs(dateParse).isValid()) {
                                setValue("enddate", Date.parse(dateParse));
                                setDateEnd(date);
                              }
                              if (errors.checkDateStartandEnd?.type) {
                                errors.checkDateStartandEnd.type = "changenew";
                              }
                            }}
                          />

                          {errors.enddate &&
                            errors.enddate.type === "required" && (
                              <Error>(*)は必須の項目です。</Error>
                            )}
                          {errors.checkDateStartandEnd &&
                            errors.checkDateStartandEnd.type === "change" && (
                              <Error>
                                終了日は開始日より後の日付にしてください。
                              </Error>
                            )}
                        </ContainerDateTime>
                      </FormFields>
                    </DateField>
                  </DateFieldsWrapper>
                </GroupFieldWrapper>
                {/* Win price video */}
                <GroupFieldWrapper>
                  <FormFields>
                    <FormLabel>
                      当選時演出動画&nbsp;
                      <span style={{ color: "red" }}>*</span>
                    </FormLabel>
                    <Select
                      labelKey="name"
                      valueKey="id"
                      value={videoLink}
                      placeholder={
                        videoLink?.length > 0 ? "" : "当選時演出動画を選択する"
                      }
                      options={!loading && data?.Videos.videos}
                      onInputChange={(event: any) =>
                        debounceAjax({ search: event.target.value, input: 0 })
                      }
                      onChange={({ value }) => {
                        setValue("videoid", value[0]?.id);
                        setVideoLink(value);
                      }}
                      controlRef={(e: any) => {
                        register(e, { required: true });
                        selectVideoRef.current = e;
                      }}
                      overrides={OverideSelect}
                    />
                    {errors.videoid && errors.videoid.type === "required" && (
                      <Error>(*)は必須の項目です。</Error>
                    )}
                  </FormFields>

                  <FormFields>
                    <FormLabel>
                      ハズレ時演出動画 &nbsp;
                      {/* <span style={{ color: 'red' }}>*</span> */}
                    </FormLabel>
                    <Select
                      labelKey="name"
                      valueKey="id"
                      value={noPrizeVideoLink}
                      placeholder={
                        noPrizeVideoLink?.length > 0
                          ? ""
                          : "ハズレ時演出動画を選択する"
                      }
                      options={!loading && data?.Videos.videos}
                      onInputChange={(event: any) =>
                        debounceAjax({ search: event.target.value, input: 1 })
                      }
                      onChange={({ value }) => {
                        setValue(
                          "noPrizeVideoId",
                          value[0]?.id === undefined ? null : value[0]?.id
                        );
                        setNoPrizeVideoLink(value);
                      }}
                      controlRef={(e: any) => {
                        register(e);
                        selectVideoRef.current = e;
                      }}
                      overrides={OverideSelect}
                    />
                    {errors.noPrizeVideoId &&
                      errors.noPrizeVideoId.type === "required" && (
                        <Error>(*)は必須の項目です。</Error>
                      )}
                  </FormFields>
                  <FormFields>
                    <FormLabel>
                      背景画像&nbsp;
                      <span style={{ color: "red" }}>*</span>
                    </FormLabel>
                    <BannerUploader
                      disabled={isUploading4}
                      onChange={(files: any) =>
                        handleUploader(files, "videoBackground")
                      }
                      onDelete={() => {
                        setUrlVideoBackground(null);
                        setValue("videoBackground", null);
                      }}
                      imageURL={urlVideoBackground}
                      progress={loadData4}
                      isEdit={isEdit}
                    />
                    {errors.videoBackground &&
                      errors.videoBackground.type === "required" && (
                        <Error>(*)は必須の項目です。</Error>
                      )}
                  </FormFields>
                </GroupFieldWrapper>

                <GroupFieldWrapper>
                  <FormFields>
                    <div style={{ marginBottom: "10px" }}>
                      <span style={{ marginRight: "1rem" }}>
                        <FormLabel>アンケート</FormLabel>
                      </span>
                    </div>
                    <Select
                      labelKey="question"
                      valueKey="id"
                      value={question}
                      placeholder={
                        question?.length > 0 ? "" : "アンケートを選択する"
                      }
                      options={!loadingQuestion && listQuestion}
                      onInputChange={(event: any) =>
                        debounceAjax({ value: event.target.value, input: 2 })
                      }
                      onChange={({ value }) => {
                        setValue("questionnaireId", value[0]?.id);
                        setQuestion(value);
                      }}
                      controlRef={(e: any) => {
                        register(e);
                        selectVideoRef.current = e;
                      }}
                      overrides={OverideSelect}
                    />
                  </FormFields>
                </GroupFieldWrapper>

                <GroupFieldWrapper>
                  <FormFields>
                    <FormLabel>テンプレート</FormLabel>
                    <Select
                      labelKey="name"
                      valueKey="id"
                      value={template}
                      placeholder={
                        template?.length > 0 ? "" : "テンプレートを選択する"
                      }
                      options={!loadingTemplates && listTemplate}
                      onChange={({ value }) => {
                        setValue("templateId", value[0]?.id);
                        setTemplate(value);
                      }}
                      overrides={OverideSelect}
                    />
                  </FormFields>
                </GroupFieldWrapper>
                <GroupFieldWrapper>
                  <FormFields>
                    <FormLabel>
                      トップーバナー&nbsp;
                      <span style={{ color: "red" }}>*</span>
                    </FormLabel>
                    <BannerUploader
                      disabled={isUploading}
                      onChange={(files: any) => handleUploader(files, "banner")}
                      onDelete={() => {
                        setUrlImages(null);
                        setValue("bannerUrl", null);
                      }}
                      imageURL={urlImages}
                      progress={loadData}
                      isEdit={isEdit}
                    />
                    {errors.bannerUrl &&
                      errors.bannerUrl.type === "required" && (
                        <Error>(*)は必須の項目です。</Error>
                      )}
                  </FormFields>

                  <FormFields>
                    <FormLabel>URL</FormLabel>
                    <Input
                      name="topBannerUrl"
                      inputRef={register({
                        maxLength: 500,
                      })}
                    />
                    {errors.topBannerUrl &&
                      errors.topBannerUrl.type === "maxLength" && (
                        <Error>URLは500文字以下設定してください。</Error>
                      )}
                  </FormFields>
                </GroupFieldWrapper>

                <GroupFieldWrapper>
                  <FormFields>
                    <FormLabel>メモ</FormLabel>
                    <Textarea
                      rows={4}
                      inputRef={(e: any) => {
                        register(e, { maxLength: 200 });
                        memoRef.current = e; // you can still assign to ref
                      }}
                      name="memo"
                      clearOnEscape
                      value={memoText}
                      onChange={(e) => {
                        let value = e.target.value;
                        setValue("memo", value);
                        setMemoText(value);
                      }}
                    />
                    {errors.memo && errors.memo.type === "maxLength" && (
                      <Error>メモは200文字以下入力してください。</Error>
                    )}
                  </FormFields>
                </GroupFieldWrapper>

                <ButtonWrapper>
                  <ButtonGroup>
                    <ButtonCustom
                      type="button"
                      kind={KIND.minimal}
                      onClick={() => history.push("/event")}
                      overrides={OverideCancel}
                    >
                      キャンセル
                    </ButtonCustom>
                    <ButtonCustom
                      isLoading={createLoading || editLoading}
                      id="button-submit"
                      type="submit"
                      overrides={OverideSave}
                    >
                      {isCreate ? "追加する" : "更新する"}
                    </ButtonCustom>
                  </ButtonGroup>
                </ButtonWrapper>
              </InfoWrapper>

              <PrizeWrapper>
                <GroupFieldWrapper>
                  <FormLabel>賞品</FormLabel>
                  <PrizeForm
                    prizeList={prizes ?? []}
                    setPrizes={setPrizes}
                    control={control}
                    setCustomerLost={setCustomerLostState}
                    customerLost={customerLostState}
                  />
                </GroupFieldWrapper>
                <GroupFieldWrapper>
                  <FormFields>
                    <FormLabel>当選時ボタン文字</FormLabel>
                    <Input
                      name="txtBtnWin"
                      inputRef={register({
                        maxLength: 255,
                      })}
                    />
                    {errors.txtBtnWin &&
                      errors.txtBtnWin.type === "maxLength" && (
                        <Error>
                          当選時ボタン文字は255文字以下設定してください。
                        </Error>
                      )}
                  </FormFields>
                  <FormFields>
                    <FormLabel>ハズレ時ボタン文字</FormLabel>
                    <Input
                      name="txtBtnLost"
                      inputRef={register({
                        maxLength: 255,
                      })}
                    />
                    {errors.txtBtnLost &&
                      errors.txtBtnLost.type === "maxLength" && (
                        <Error>
                          ハズレ時ボタン文字は255文字以下設定してください。
                        </Error>
                      )}
                  </FormFields>
                </GroupFieldWrapper>

                <GroupFieldWrapper>
                  <FormFields>
                    <FormLabel>当選者情報入力制限時間（分）</FormLabel>
                    <Input
                      name="timeOut"
                      type="number"
                      min={1}
                      max={999999}
                      inputRef={register({})}
                    />
                  </FormFields>
                  <>
                    ＊この制限時間は当選時点から当選者情報(住所やアンケート等)を受け付けるまでの制限時間となります。
                  </>
                  <br></br>
                  <>
                    ＊極端に短い時間を入力されますと当選しても情報入力までの時間で制限時間となり、当選者が当選者情報を入力できなくなりますのでご注意ください。
                  </>
                  <br></br>
                  <>
                    ＊未入力の場合はイベント終了後でも当選者情報が入力できる無制限状態となります。
                  </>
                </GroupFieldWrapper>
                <GroupFieldWrapper>
                  <FormFields>
                    <FormLabel>一日抽選回数制限</FormLabel>
                    <Input
                      name="spinLimit"
                      type="number"
                      min={1}
                      max={999999}
                      inputRef={register({})}
                    />
                  </FormFields>
                  <>＊未入力の場合は無制限となります。</>
                  <br></br>
                </GroupFieldWrapper>
                <div>
                  <GroupFieldWrapper
                    style={{ marginBottom: isIE ? "80px" : "0px" }}
                  >
                    <FormLabel>概要&nbsp;</FormLabel>

                    <QuillEditorWrapper>
                      <CustomQuill>
                        <QuillToolbar />
                        <ReactQuill
                          theme="snow"
                          modules={modules}
                          formats={formats}
                          value={dataEditor ? dataEditor : ""}
                          onChange={ChangeContent}
                        />
                      </CustomQuill>
                    </QuillEditorWrapper>

                    <FormFields>
                      <FormLabel>テンプレートの呼出し&nbsp;</FormLabel>
                      <Row style={{ alignItems: "center" }}>
                        <Col xs={18}>
                          <Select
                            labelKey="name"
                            valueKey="id"
                            value={selectOption}
                            options={!loadingCampaign && listCampaigns}
                            placeholder={"テンプレートを選択する"}
                            onInputChange={(event: any) =>
                              debounceAjax({
                                search: event.target.value,
                                input: 3,
                              })
                            }
                            onFocus={() => refetchCampaigns()}
                            onChange={({ value }) => {
                              setSelectOption(value);
                              setSelectValue(value);
                            }}
                            overrides={OverideSelect}
                          />
                        </Col>
                        <Col xs={6}>
                          <ButtonCustom
                            onClick={handleConfirmCamp}
                            disabled={selectValue.length > 0 ? false : true}
                            type="button"
                            style={{
                              width: "100%",
                              height: "45px",
                              backgroundColor:
                                selectValue.length > 0 ? "#00bd87" : "gray",
                              color: "white",
                            }}
                          >
                            呼出し
                          </ButtonCustom>
                        </Col>
                      </Row>
                    </FormFields>
                  </GroupFieldWrapper>
                </div>
              </PrizeWrapper>
            </Form>
          </EventFormWrapper>
        </Wrapper>
      </div>
    </Container>
  );
}
