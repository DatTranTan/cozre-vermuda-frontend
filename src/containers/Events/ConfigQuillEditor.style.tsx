import styled from "styled-components";

export const QuillEditorWrapper = styled.div`
    .ql-container.ql-snow {
        height: 400px;
        overflow-y: auto;
    }
`