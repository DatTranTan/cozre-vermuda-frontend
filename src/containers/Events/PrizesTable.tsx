/* eslint-disable */
import React, { useState } from 'react'
import { useDeviceType } from '../../settings/useDeviceType'
import { Table, Tooltip } from 'antd'
import { TableWrapper } from './Events.style'
import confirm from 'antd/lib/modal/confirm'
import { ActionWrapper } from './EventForm.style'
import {
  DeleteOutlined,
  EditOutlined,
  ExclamationCircleOutlined
} from '@ant-design/icons'
import { useEventDispatch } from '../../context/EventContext'
import { FullPrizeColumns } from './Columns'

type Props = {
  handleEdit: any
  prizeList: any
  setPrizes: any
}

const PrizesTable: React.FC<Props> = ({ handleEdit, setPrizes, prizeList }) => {
  const dispatch = useEventDispatch()
  const [selectValue, setSelectValue] = useState(null)
  const [disableEdit, setDisableEdit] = useState(true)
  const [disableDelete, setDisableDelete] = useState(true)
  const [selectedRows, setSelectedRows] = useState([])
  const [pageIndex, setPageIndex] = useState(0)
  const { tablet } = useDeviceType(window.navigator.userAgent)
  const pageSize = tablet ? 10 : 5
  const prizeData = prizeList.map((item, idx) => ({
    ...item,
    idx: idx + 1,
    key: item.id,
    videoId: item.video?.id,
    videoName: item.video?.name,
    videoURL: item.video?.url
  }))
  const total = prizeList?.length ?? 0

  const rowSelection = {
    selectedRowKeys: selectedRows.map((item) => item.key),
    onChange: (selectedRowKeys, selectedRows) => {
      console.log(selectedRows)
      setSelectedRows(selectedRows)

      if (selectedRowKeys.length > 1) {
        setDisableEdit(true)
        setDisableDelete(false)
      } else if (selectedRowKeys.length > 0) {
        setDisableEdit(false)
        setDisableDelete(false)
      } else {
        setDisableEdit(true)
        setDisableDelete(true)
      }
    }
  }

  const deleteHandler = () => {
    confirm({
      type: 'warning',
      title: '確認',
      content: `賞品を削除してもよろしいですか？`,
      icon: <ExclamationCircleOutlined />,
      cancelText: 'キャンセル',
      okText: 'OK',
      onOk: () => {
        dispatch({
          type: 'DELETE_PRIZE',
          prizeIndex: selectedRows[0].idx
        })
        let clonePrize = [...prizeList]
        clonePrize.splice(selectedRows[0].idx, 1)
        setPrizes(clonePrize)
        setSelectedRows([])
        setDisableDelete(true)
        setDisableEdit(true)
      }
    })
  }

  return (
    <>
      <TableWrapper>
        <ActionWrapper>
          <EditOutlined
            style={{
              fontSize: '1.4rem',
              color: disableEdit && '#ccc',
              margin: '0rem 0rem 0rem 0.5rem'
            }}
            title={'賞品編集'}
            onClick={!disableEdit ? () => handleEdit() : undefined}
          />
          <DeleteOutlined
            style={{
              fontSize: '1.4rem',
              color: disableDelete ? '#ccc' : '#f00',
              margin: '0rem 0rem 0rem 1rem'
            }}
            title={'賞品削除'}
            onClick={!disableDelete ? () => deleteHandler() : undefined}
          />
          
        </ActionWrapper>
        <Table
          bordered
          scroll={{ x: 1270 }}
          style={{
            overflow: 'auto',
            background: 'white',
            border: '1px solid #ddd',
            minHeight: '40vh'
          }}
          rowSelection={{
            type: 'checkbox',
            ...rowSelection
          }}
          columns={FullPrizeColumns}
          dataSource={prizeData}
          pagination={{
            total: total,
            pageSize: pageSize,
            // simple: true,
            current: pageIndex + 1,
            onChange: (value) => {
              setPageIndex(value - 1)
            },
            position: ['bottomCenter']
          }}
        />{' '}
      </TableWrapper>
    </>
  )
}
export default PrizesTable
