import style from "styled-components";
import { styled } from 'baseui'
import { Col } from '../../components/FlexBox/FlexBox'
import Button from '../../components/Button/Button'
import { DatePicker } from "antd";

export const ContainerDateTime = style.div`
  display: "flex";
  .ant-btn-primary {
    background-color: rgb(0, 197, 141),
  };
  .ant-picker-focused {
    border: 2px rgb(0, 197, 141) solid;
    border-style: solid;
    border-width: 2px !important;
    box-shadow: none;
  }
`;

export const DatePickerContainer = style(DatePicker)`
  border: 0;
  border-radius: 0;
  font-weight: 900;
  background-color: rgb(238, 238, 238);
  height: 41px;
  width: 100%;
  :hover {
    border: 0;
    transition: none;
  }
  :active {
    border: 2px rgb(0, 197, 141) solid;
    transition: none;
  }
  .ant-picker-input > input {
    font-weight: bold;
    color: rgb(22, 31, 106);
  }
`;

export const Container = styled('div', () => ({
  display: 'flex',
  height: 'calc(100% - 70px)',
  flexDirection: 'column'
  // paddingBottom: '100px'
}))

export const Header = styled('header', () => ({
  display: 'flex',
  alignItems: 'center',
  flex: '0 1 auto',
  flexDirection: 'row',
  flexWrap: 'wrap',
  backgroundColor: '#ffffff',
  marginBottom: '1.5rem',
  height: '70px',
  boxShadow: '0 0 8px rgba(0, 0 ,0, 0.1)',

  '@media only screen and (max-width: 1024px)': {
    padding: '0 0.5rem'
  },

  '@media only screen and (max-width: 990px)': {
    marginBottom: '1rem'
  },

  '@media only screen and (max-width: 767px)': {
    marginBottom: '0.5rem'
  }
}))

export const Heading = styled('a', ({ $theme }) => ({
  ...$theme.typography.fontBold18,
  color: $theme.colors.textDark,
  marginLeft: '16px'
}))

export const Heading2 = styled('span', ({ $theme }) => ({
  ...$theme.typography.font18,
  color: $theme.colors.primary,
  margin: 0
}))

export const Form = styled('form', ({ $theme }) => ({
  backgroundColor: '#FFF',
  width: '100%',
  display: 'flex',
  '@media only screen and (max-width: 1280px)': {
    width: '100%',
    flexDirection: 'column',
    height: 'auto',
    minHeight: 'unset'
  }
}))

export const PrizeFormCustom = styled('form', ({ $theme }) => ({
  display: 'flex',
  flexDirection: 'column',
  backgroundColor: $theme.colors.backgroundF7,
  // padding: '20px',

  '@media only screen and (max-width: 1280px)': {
    minHeight: '20vh'
  },

  '@media only screen and (max-width: 1024px) and (max-height: 768px)': {
    minHeight: '28vh'
  },
  '@media only screen and (max-width: 1024px)': {
    minHeight: '20vh'
  },
  '@media only screen and (max-width: 768px)': {
    minHeight: '30vh'
  }
}))

export const FormPrizeWrapper = styled('div', () => ({
  display: 'flex',
  flexDirection: 'row'
}))

export const EventFormWrapper = styled('div', () => ({
  // paddingTop: '5px',
  display: 'flex',
  flexDirection: 'row',
  width: '100%',
  borderRadius: '3px',
  backgroundColor: '#FFF',
  paddingBottom: '70px',

  '@media only screen and (max-width: 1280px)': {
    flexDirection: 'column',
    paddingBottom: '60px'
  }
}))

export const ContainerPrize = styled('div', () => ({
  // overflowX: 'hidden',
  // height: 'calc(100% - 100px)',
  display: 'flex',
  flexDirection: 'column'
}))

export const ActionWrapper = styled('div', () => ({
  display: 'flex',
  width: '100%',
  alignSelf: 'flex-end',
  margin: '1.25rem 0',
  justifyContent: 'flex-start',
  span: {
    cursor: 'pointer',
    marginLeft: '1rem'
  },
  'span:hover': {
    color: '#40a9ff'
  }
}))

export const Wrapper = styled('div', () => ({
  display: 'flex',
  flexDirection: 'column',
  height: '100%',
  paddingBottom: '1rem',
  width: '100%',
  backgroundColor: '#ffffff',
  '@media only screen and (max-width: 1024px)': {
    width: '100%'
  }
}))

export const ButtonWrapper = styled('div', () => ({
  position: 'fixed',
  bottom: 0,
  right: 0,
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
  width: 'calc(100% - 271px)',
  zIndex: 1,
  height: '5rem',

  '@media only screen and (max-width: 1440px)': {
    width: 'calc(100% - 271px)',
    height: '4rem'
  },

  '@media only screen and (max-width: 1280px)': {
    width: 'calc(100% - 231px)',
    height: '3rem'
  },

  '@media only screen and (max-width: 1024px)': {
    bottom: 0,
    left: 0,
    width: '100%',
    height: '3rem'
  },
  '@media only screen and (max-width: 1280px) and (max-height: 768px)': {
    width: '100%'
  }
}))

export const ButtonGroup = styled('div', ({ $theme }) => ({
  padding: '15px 50px',
  display: 'flex',
  alignItems: 'center',
  width: '100%',
  height: '100%',
  backgroundColor: '#ffffff',
  boxShadow: '0 0 3px rgba(0, 0, 0, 0.1)',

  '@media only screen and (max-width: 1024px)': {
    padding: '10px 50px'
  }
}))

export const ButtonCustom = styled(Button, () => ({
  height: '47px',

  '@media only screen and (max-width: 1280px)': {
    height: '40px'
  },

  '@media only screen and (max-width: 1024px)': {
    height: '35px'
  },

  '@media only screen and (max-width: 768px)': {
    height: '30px'
  }
}))

export const InfoWrapper = styled('div', () => ({
  width: '50%',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  borderRadius: '3px',
  backgroundColor: '#ffffff',
  padding: '10px 10px 10px 20px',

  '@media only screen and (max-width: 1280px)': {
    width: '100%',
    height: 'auto',
    paddingRight: '20px'
  }
}))

export const GroupFieldWrapper = styled('div', () => ({
  backgroundColor: 'rgb(247, 247, 247);',
  padding: '20px 20px 20px 20px',
  marginBottom: '20px',
  width: '100%',
 }))

export const PrizeWrapper = styled('div', () => ({
  // display: 'flex',
  // flexDirection: 'column',
  backgroundColor: '#FFF',
  width: '50%',
  padding: '10px 20px 10px 10px',

  '@media only screen and (max-width: 1280px)': {
    width: '100%',
    minHeight: 'unset',
    padding: '0 20px'
  }

  // '@media only screen and (max-width: 1024px)': {}
}))

export const InfoFieldsWrapper = styled('div', () => ({
  display: 'flex',
  flexDirection: 'column',
  width: '50%',
  // paddingRight: theme.sizing.scale700 //10px
  paddingRight: '10px'
}))

export const ValueFieldsWrapper = styled('div', () => ({
  display: 'flex',
  flexDirection: 'column',
  width: '50%',
  paddingLeft: '10px'
}))

export const FieldWrapper = styled('div', () => ({
  display: 'flex',
  flexDirection: 'row',
  // justifyContent: 'space-between',
  width: '100%',
  padding: '10px 0',

  ':first-child': {
    paddingTop: 0
  },
  ':last-child': {
    marginBottom: 0
  },
  ':only-child': {
    padding: 0
  }
}))

export const DateFieldsWrapper = styled('div', () => ({
  display: 'flex',
  flexDirection: 'row',
  width: '100%',
  margin: '10px 0',
  marginBottom: '0',
}))

export const DateField = styled('div', () => ({
  display: 'flex',
  width: '50%',

  ':first-child': {
    marginRight: '0.5rem'
  },

  ':last-child': {
    marginLeft: '0.5rem'
  },
  '@media only screen and (max-width: 1420px) ': {
    width: '48.5%'
  }
}))

export const ButtonPrizeWrapper = styled('div', () => ({
  // padding: '5px',
  display: 'flex',
  alignItems: 'center',
  width: '100%',
  marginTop: '15px'
}))

export const Column = styled(Col, () => ({
  display: 'flex',
  alignItems: 'center',
  flexDirection: 'row',
  '@media only screen and (max-width: 767px)': {
    marginBottom: '10px',
    ':last-child': {
      marginBottom: 0
    }
  },
  height: '100%'
}))

export const RequiredWrapper = styled('div', () => ({
  display: 'flex',
  width: '100%',
  margin: '10px 1.25rem 0.1rem',
  flexDirection: 'row',
  justifyContent: 'flex-start',
  '@media only screen and (max-width: 1024px)': {
    lineHeight: '3.5rem'
  }
}))

export const SelectWrapper = styled('div', () => ({
  display: 'flex',
  width: '100%',
  flexDirection: 'row',
  alignItems: 'flex-end',
  justifyContent: 'space-between'
}))

export const ButtonSelect = styled('div', ({ $theme }) => ({
  height: '48px',
  minWidth: '100px',
  borderRadius: '0',
  backgroundColor: 'rgb(221,221,221)',
  justifyContent: 'center',
  alignItems: 'center',
  display: 'flex'
}))
