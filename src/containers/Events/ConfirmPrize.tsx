import { AreaChartOutlined } from '@ant-design/icons'
import { useQuery } from '@apollo/react-hooks'
import React, { memo } from 'react'
import { QUERY_EVENT } from '../../graphql/query/event'
import { PrizeConfirmWrapper, PrizeItem, PrizeName } from './Events.style'
// import Scrollbars from 'react-custom-scrollbars'
// import { ContainerPrize } from './EventForm.style'

type Props = any
const ConfirmPrize: React.FC<Props> = (props) => {
  const { eventId } = props
  const { data } = useQuery(QUERY_EVENT, {
    variables: {
      id: eventId
    },
    fetchPolicy: 'network-only'
  })
  console.log('111111',data?.event?.customerLost)
  const prizes = data?.event.prizes
    .map((item, idx) => ({
      ...item,
      idx: idx + 1,
      key: item.id
    }))
    .filter((x) => data?.event?.customerLost === 'Lost'? x.rank !== 0: true)
    .map((item) => (
      <PrizeItem>
        <PrizeName>
          <div
            style={{
              color: 'rgb(22, 31, 106)'
            }}
          >
            <p>賞品名: </p>
            <p>数量: </p>
            {/* <p>残りの贈り物: </p> */}
          </div>
          <div
            style={{
              marginLeft: '3rem',
              fontWeight: 'normal',
              color: 'rgb(4 160 116)',
              width: '80%',
              overflow: 'hidden',
              textOverflow: 'ellipsis',
              whiteSpace: 'nowrap'
            }}
          >
            <p
              title={item.name}
              style={{
                overflow: 'hidden',
                textOverflow: 'ellipsis',
                whiteSpace: 'nowrap'
              }}
            >
              {item.name}{' '}
            </p>
            <p>
              {item.rank !== 0 ?`${item.quantity - item.numberOfWinner} / ${item.quantity}`: '∞'}{' '}
            </p>
            {/* <p>
              {(
                ((item.quantity - item.numberOfWinner) / item.quantity) *
                100
              ).toFixed(0)}
              %{' '}
            </p> */}
          </div>
          {/* <span style={{ color: 'rgb(0, 197, 141)' }}>イベント名: </span>
          <span style={{ color: 'rgb(22, 31, 106)' }}>{item.name}</span>
          <span style={{ color: 'rgb(0, 197, 141)' }}>| 数量: </span>
          <span style={{ color: 'rgb(22, 31, 106)' }}>
            {item.quantity - item.numberOfWinner}
          </span>
          | 残りの贈り物:{' '}
          {(
            ((item.quantity - item.numberOfWinner) / item.quantity) *
            100
          ).toFixed(0)}
          % */}
        </PrizeName>
        {/* <ButtonConfirm
          disabled={item.quantity - item.numberOfWinner === 0}
          onClick={() =>
            Modal.confirm({
              title: '確認',
              content: 'この操作を続行してもよろしいですか？',
              cancelText: 'キャンセル',
              okText: 'OK',
              icon: <ExclamationCircleOutlined />,
              onOk: () =>
                handleConfirm({ variables: { eventId, prizeId: item.id } })
                  .then((rs) => {
                    Notification({
                      type: 'success',
                      message: '成功',
                      description: '操作が成功しました。'
                    })

                    refetch()
                  })
                  .catch(({ graphQLErrors }) => {
                    const mess = graphQLErrors.map((item) => item.message)
                    if (mess.includes('賞品はもう終わりました')) {
                      Notification({
                        type: 'error',
                        message: ' エラー',
                        description: '賞品の数量が0になっています。'
                      })
                    } else {
                      Notification({
                        type: 'error',
                        message: ' エラー',
                        description: graphQLErrors[0]?.message
                      })
                    }
                  })
            })
          }
        >
          賞品を減らす
        </ButtonConfirm> */}
      </PrizeItem>
    ))

  if (prizes?.length < 1) {
    return (
      <div
        style={{
          color: '#ddd',
          fontSize: '1rem',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          flexDirection: 'column'
        }}
      >
        <AreaChartOutlined style={{ fontSize: '3rem', color: '#ddd' }} />
        賞品がありません。
      </div>
    )
  }
  return (
    <>
      {prizes?.length <= 2 ? (
        <div>{prizes}</div>
      ) : (
        prizes?.length > 2 && (
          <PrizeConfirmWrapper>
            {/* <Scrollbars
              renderView={(props) => (
                <ContainerPrize
                  // {...props}
                  style={
                    {
                      // ...props.style
                    }
                  }
                />
              )}
              // renderTrackHorizontal={(props) => (
              //   <div
              //     {...props}
              //     style={{ display: 'none' }}
              //     className='track-horizontal'
              //   />
              // )}
            > */}
            {prizes}
            {/* </Scrollbars> */}
          </PrizeConfirmWrapper>
        )
      )}
    </>
  )
}

export default memo(ConfirmPrize)
