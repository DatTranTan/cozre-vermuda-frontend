/* eslint-disable */
import { ExclamationCircleOutlined } from "@ant-design/icons";
import { Modal } from "antd";
import * as qs from "query-string";
import React, { memo, useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useLocation } from "react-router-dom";
import { v4 as uuidv4 } from "uuid";
import { DeleteIcon, EditIcon } from "../../components/ActionIcon/ActionIcon";
import Button, { KIND, SIZE } from "../../components/Button/Button";
import { FormPrizeFields } from "../../components/FormFields/FormFields";
import Input from "../../components/Input/Input";
import { Notification } from "../../components/Notification/NotificationCustom";
import Uploader from "../../components/Uploader/PrizeUploader";
import { useEventDispatch } from "../../context/EventContext";
import { useDeviceType } from "../../settings/useDeviceType";
import addPhoto from "../../utils/S3/upload";
import { PrizeColumns } from "./Columns";
import {
  ActionWrapper,
  ButtonPrizeWrapper,
  FieldWrapper,
  PrizeFormCustom,
  GroupFieldWrapper,
} from "./EventForm.style";
import { TableCustom, TableWrapper } from "./Events.style";
import {
  OverideNameInput,
  OverideNumberInput,
  OverideReset,
  OverideSave,
} from "./Override";
import PrizesTable from "./PrizesTable";
import { Decimal } from "decimal.js";
import { Checkbox } from "antd";
import {
  Error,
  FormFields,
  FormLabel,
} from "../../components/FormFields/FormFields";
import { Controller } from "react-hook-form";

const { confirm } = Modal;

type Props = {
  prizeList: any;
  setPrizes: any;
  control: any;
  customerLost: any;
  setCustomerLost: any;
};
const PrizeForm: React.FC<Props> = ({
  prizeList,
  setPrizes,
  control,
  customerLost,
  setCustomerLost,
}) => {
  const location = useLocation();
  let search = qs.parse(location.search);
  const dispatch = useEventDispatch();
  const UniqueID = Date.now() + "_" + Math.random().toString(36).substr(2, 34);
  const [albumName, setAlbumName] = useState(UniqueID);
  const [loadData, setLoadData] = useState({ loaded: 0, total: 0, unit: "" });
  const [urlImages, setUrlImages] = useState(null);
  const [isUploading, setIsUploading] = useState(false);
  const [isChecked, setIsChecked] = useState(false);
  const [disableEdit, setDisableEdit] = useState(true);
  const [disableDelete, setDisableDelete] = useState(true);
  const [selectedTableRows, setSelectedTableRows] = useState([]);
  const [actionType, setActionType] = useState("add");
  const [pageIndex, setPageIndex] = useState(0);
  const [totalRate, setTotalRate] = useState(0) as any;
  const [visibleModal, setVisibleModal] = useState(false);
  const { register, errors, setValue, handleSubmit } = useForm();
  const { tablet } = useDeviceType(window.navigator.userAgent);
  const pageSize = tablet ? 10 : 5;

  const callback = {
    onSuccess: (urls) => {
      setUrlImages(urls);
      setIsUploading(false);
    },
    onError: (urls) => {
      setIsUploading(false);
    },
    onLoad: (loaded: number, total: number, unit: any) => {
      setLoadData({ loaded: loaded, total: total, unit: unit });
    },
  };

  const handleUploader = (files) => {
    setIsUploading(true);
    addPhoto(albumName, "images/", files, callback);
  };

  const columnsNew = PrizeColumns.slice(0, PrizeColumns.length);
  const prizesData = prizeList?.map((item, idx) => ({
    ...item,
    idx: idx + 1,
    key: item.id,
  }));

  useEffect(() => {
    const cal = prizeList
      ?.filter((item) => item.rank !== 0)
      .reduce((sum, item) => Number(new Decimal(sum).plus(item.rate)), 0);
    setTotalRate(cal ?? 0);
  }, [search.type, prizeList.length, selectedTableRows.length]);

  useEffect(() => {
    setPrizes([
      {
        id: uuidv4(),
        name: "ハズレ",
        rank: 0,
        rate: 100,
        quantity: 0,
        url: "",
      },
    ]);
  }, []);

  const rowSelection = {
    selectedRowKeys: selectedTableRows.map((item) => item.key),
    onChange: (selectedRowKeys, selectedRows) => {
      setSelectedTableRows(selectedRows);
      const isRank0 = selectedRows.map((item) => item.rank).includes(0);
      if (selectedRowKeys.length > 1) {
        setDisableEdit(true);
        !isRank0 && setDisableDelete(false);
        isRank0 && setDisableDelete(true);
      } else if (selectedRowKeys.length > 0) {
        setDisableEdit(false);
        !isRank0 && setDisableDelete(false);
        isRank0 && setDisableDelete(true);
      } else {
        setDisableEdit(true);
        setDisableDelete(true);
        resetFields();
      }
    },
  };

  const deleteHandler = () => {
    confirm({
      type: "warning",
      title: "確認",
      content: `賞品を削除してもよろしいですか？`,
      icon: <ExclamationCircleOutlined />,
      cancelText: "キャンセル",
      okText: "OK",
      onOk: () => {
        const mapRows = selectedTableRows.map((x) => x.id);
        const newPrizeList = prizeList.filter(
          (item) => !mapRows.includes(item.id) && item.rank !== 0
        );

        const rank0 = prizeList.find((item) => item.rank === 0);
        const deleteRate = selectedTableRows.reduce(
          (sum, row) => sum + row.rate,
          0
        );

        setPrizes([
          ...newPrizeList,
          {
            ...rank0,
            rate: Number(new Decimal(100).minus(totalRate).plus(deleteRate)),
          },
        ]);
        setTotalRate(Number(new Decimal(totalRate).minus(deleteRate)));

        setSelectedTableRows([]);
        setDisableDelete(true);
        setDisableEdit(true);
        Notification({
          type: "success",
          message: "成功",
          description: "削除に成功しました。",
        });
      },
    });
  };

  const onSubmitPrize = async (data: any) => {
    const checkRank = !prizeList
      .filter((x) => !(x.id === selectedTableRows[0]?.id))
      .map((item) => item.rank)
      .includes(parseFloat(data.rank));

    const newList = prizeList.filter((item) => item.rank !== 0);
    const rank0 = prizeList.find((item) => item.rank === 0);
    if (checkRank) {
      if (actionType === "add") {
        const newId = uuidv4();
        let dataNew = {
          id: newId,
          name: data.prname,
          rank: parseFloat(data.rank),
          rate: parseFloat(data.rate),
          quantity: parseFloat(data.quantity),
          imageUrl: urlImages,
          url: data.url,
          createdAt: Date.now(),
        };
        let rate = Number(new Decimal(totalRate).plus(parseFloat(data.rate)));
        setPrizes([
          dataNew,
          ...newList,
          {
            ...rank0,
            rate: Number(
              new Decimal(100).minus(totalRate).minus(parseFloat(data.rate))
            ),
          },
        ]);
        setTotalRate(rate);
        Notification({
          type: "success",
          message: "成功",
          description: "賞品追加に成功しました。",
        });
      }
      if (actionType === "edit" && selectedTableRows.length > 0) {
        let dataNew = {
          id: selectedTableRows[0].id,
          name: data.prname,
          rank: parseFloat(data.rank),
          rate: parseFloat(data.rate),
          quantity: parseFloat(data.quantity),
          imageUrl: urlImages,
          url: data.url,
          createdAt: Date.now(),
        };
        const idx = newList.findIndex(
          (item) => item.id === selectedTableRows[0].id
        );

        const newPrizes =
          dataNew.rank !== 0
            ? [
                ...newList.slice(0, idx),
                dataNew,
                ...newList.slice(idx + 1),
                {
                  ...rank0,
                  rate: Number(
                    new Decimal(100)
                      .minus(totalRate)
                      .minus(parseFloat(data.rate))
                  ),
                },
              ]
            : [
                ...newList.slice(0),
                {
                  ...dataNew,
                  rate: Number(
                    new Decimal(100)
                      .minus(totalRate)
                      .minus(parseFloat(data.rate))
                  ),
                },
              ];
        setPrizes(newPrizes);
        setTotalRate(
          Number(new Decimal(totalRate).plus(parseFloat(data.rate)))
        );
        Notification({
          type: "success",
          message: "成功",
          description: "賞品更新に成功しました。",
        });
      }
      resetFields();
      setDisableEdit(true);
      setDisableDelete(true);
    } else {
      Notification({
        type: "error",
        message: " エラー",
        description: "ランクは既に存在しました",
      });
    }
  };

  const handleEdit = () => {
    setActionType("edit");
    setVisibleModal(false);
    setValue("prname", selectedTableRows[0]?.name);
    setValue("rank", selectedTableRows[0]?.rank);
    setValue("quantity", selectedTableRows[0]?.quantity);
    setValue("rate", selectedTableRows[0]?.rate);
    setValue("url", selectedTableRows[0]?.url);
    setUrlImages(selectedTableRows[0]?.imageUrl);
    setTotalRate(
      Number(new Decimal(totalRate).minus(selectedTableRows[0].rate)) ?? 0
    );
  };
  const resetFields = () => {
    setValue("prname", "");
    setValue("rank", null);
    setValue("quantity", null);
    setValue("rate", null);
    setValue("url", null);
    setUrlImages(null);
    setIsChecked(false);
    dispatch({
      type: "RESET_PRIZE_FIELDS",
      isReset: true,
    });
    setSelectedTableRows([]);
    setActionType("add");
    setDisableEdit(true);
    setDisableDelete(true);
  };
  const requiredLabel = (value: string) => (
    <div>
      {value}&nbsp;
      <span style={{ color: "red" }}>*</span>
    </div>
  );

  return (
    <>
      <PrizeFormCustom id="prize-form" onSubmit={handleSubmit(onSubmitPrize)}>
        <FieldWrapper>
          <FormPrizeFields>
            <Input
              inputRef={register({ required: true, maxLength: 255 })}
              name="prname"
              startEnhancer={requiredLabel("賞品名")}
              overrides={OverideNameInput}
            />
            {isChecked &&
              errors.prname &&
              errors.prname.type === "required" && (
                <Error>(*)は必須の項目です。</Error>
              )}
            {isChecked &&
              errors.prname &&
              errors.prname.type === "maxLength" && (
                <Error>賞品名は255文字以下設定してください。</Error>
              )}
          </FormPrizeFields>
          <FormPrizeFields>
            <Input
              type="number"
              inputRef={register({ required: true })}
              disabled={
                actionType === "edit" && selectedTableRows[0]?.rank === 0
              }
              name="rank"
              startEnhancer={requiredLabel("ランク")}
              overrides={OverideNumberInput}
              min={0}
              max={100}
            />
            {isChecked && errors.rank && errors.rank.type === "required" && (
              <Error>(*)は必須の項目です。</Error>
            )}
          </FormPrizeFields>{" "}
        </FieldWrapper>
        <FieldWrapper>
          <FormPrizeFields>
            <Input
              type="number"
              inputRef={register({ required: true })}
              name="quantity"
              disabled={
                actionType === "edit" && selectedTableRows[0]?.rank === 0
              }
              startEnhancer={requiredLabel("数量")}
              overrides={OverideNumberInput}
              min={0}
            />
            {isChecked &&
              errors.quantity &&
              errors.quantity.type === "required" && (
                <Error>(*)は必須の項目です。</Error>
              )}
          </FormPrizeFields>
          <FormPrizeFields>
            <Input
              type="number"
              inputRef={register({ required: true })}
              name="rate"
              step="0.000001"
              startEnhancer={requiredLabel("割合(%)")}
              min={0}
              max={100 - totalRate}
              disabled={
                actionType === "edit" && selectedTableRows[0]?.rank === 0
              }
              overrides={OverideNumberInput}
            />
            {isChecked && errors.rate && errors.rate.type === "required" && (
              <Error>(*)は必須の項目です。</Error>
            )}
          </FormPrizeFields>
        </FieldWrapper>
        <FieldWrapper>
          <FormFields>
            <Input
              inputRef={register({ required: true, maxLength: 2000 })}
              name="url"
              startEnhancer={requiredLabel("URL")}
              overrides={OverideNameInput}
            />
            {isChecked && errors.url && errors.url.type === "required" && (
              <Error>(*)は必須の項目です。</Error>
            )}
            {isChecked && errors.url && errors.url.type === "maxLength" && (
              <Error>URLは2000文字以下設定してください。</Error>
            )}
          </FormFields>
        </FieldWrapper>
        <FieldWrapper>
          <FormFields>
            <Uploader
              disabled={isUploading}
              onChange={handleUploader}
              onDelete={() => setUrlImages(null)}
              imageURL={urlImages}
              progress={loadData}
            />
          </FormFields>
        </FieldWrapper>
        <ButtonPrizeWrapper>
          <Button
            size={SIZE.compact}
            kind={KIND.secondary}
            overrides={OverideReset}
            onClick={resetFields}
            type="button"
          >
            {disableEdit ? "リセット" : "キャンセル"}
          </Button>
          <Button
            id="button-childe"
            onClick={(e) => setIsChecked(true)}
            size={SIZE.compact}
            type="submit"
            overrides={OverideSave}
            disabled={
              (actionType === "edit" && selectedTableRows.length < 1) ||
              isUploading
            }
          >
            {actionType === "edit" ? "更新する" : "追加する"}
          </Button>
        </ButtonPrizeWrapper>
      </PrizeFormCustom>

      <GroupFieldWrapper
        // style={{ marginTop: "20px", paddingTop: "1px", paddingBottom: "1px" }}
        style={{ padding: "0" }}
      >
        <TableWrapper>
          <ActionWrapper>
            <EditIcon
              disabled={disableEdit}
              color={disableEdit && "#ccc"}
              title={"賞品編集"}
              onClick={!disableEdit ? () => handleEdit() : undefined}
            />
            <DeleteIcon
              disabled={disableDelete}
              color={disableDelete ? "#ccc" : "#f00"}
              title={"賞品削除"}
              onClick={!disableDelete ? () => deleteHandler() : undefined}
              margin={"0 0.5rem"}
            />
            <Checkbox
              style={{ paddingLeft: "10px" }}
              checked={customerLost}
              onChange={() => setCustomerLost(!customerLost)}
            >
              <FormLabel>参加賞機能</FormLabel>{" "}
            </Checkbox>
            {/* <Controller
                      as={
                        <Checkbox>
                          <FormLabel>参加賞機能</FormLabel>{" "}
                        </Checkbox>
                      }
                      name="customerLost"
                      control={control}
                      onChange={([e]) => {
                        return e.target.checked;
                      }}
                    /> */}
            {/* <FullScreenIcon
            color={'#0df'}
            title={'賞品'}
            onClick={() => setVisibleModal(true)}
          /> */}
          </ActionWrapper>
          <TableCustom
            bordered
            rowSelection={{
              type: "checkbox",
              ...rowSelection,
            }}
            columns={search.type === "edit" ? PrizeColumns : columnsNew}
            dataSource={prizesData}
            pagination={{
              total: prizesData.length,
              pageSize: pageSize,
              simple: true,
              current: pageIndex + 1,
              onChange: (value) => {
                setPageIndex(value - 1);
              },
            }}
          />
        </TableWrapper>
      </GroupFieldWrapper>
      <Modal
        width={tablet ? "80vw" : "90vw"}
        destroyOnClose={true}
        title={
          <h2>
            <span style={{ color: "rgb(0, 197, 141)" }}>賞品</span>
          </h2>
        }
        visible={visibleModal}
        footer={null}
        onCancel={() => setVisibleModal(false)}
      >
        <PrizesTable
          handleEdit={handleEdit}
          prizeList={prizeList}
          setPrizes={setPrizes}
        />
      </Modal>
    </>
  );
};
export default memo(PrizeForm);
