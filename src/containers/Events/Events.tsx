/* eslint-disable */
import { ExclamationCircleOutlined } from '@ant-design/icons'
import { useMutation, useQuery } from '@apollo/react-hooks'
import { Modal, Table } from 'antd'
import confirm from 'antd/lib/modal/confirm'
import * as qs from 'query-string'
import React, { useState } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import {
  AddIcon,
  DeleteIcon,
  EditIcon,
  ExportIcon
} from '../../components/ActionIcon/ActionIcon'
import { Notification } from '../../components/Notification/NotificationCustom'
import SeachText from '../../components/SeachText/BaseSeachText'
import Select from '../../components/Select/HeaderSelect'
import { MUTATION_DELETE_EVENT } from '../../graphql/mutation/delete-event'
import { QUERY_EVENTS } from '../../graphql/query/events'
import { LANDING_PAGE, PDF_EXPORT } from '../../settings/constants'
import { useDeviceType } from '../../settings/useDeviceType'
import { EventColumns } from './Columns'
import ConfirmPrize from './ConfirmPrize'
import EditableCell from './EditableCell'
import { ActionCol, ActionWrapper, Header, NameCol, SearchWrapper, SelectWrapper, TableWrapper } from './Events.style'
import { IsExpiredOptions, SortOptions } from './Interface'

export default function Events() {
  const history = useHistory()
  const location = useLocation()
  let search = qs.parse(location.search)
  const [searchText, setSearchText] = useState('')
  const [disableEdit, setDisableEdit] = useState(true)
  const [disableDelete, setDisableDelete] = useState(true)
  const [selectedRows, setSelectedRows] = useState([])
  const [pageIndex, setPageIndex] = useState(0)
  const { tablet } = useDeviceType(window.navigator.userAgent)
  const [isModalVisible, setIsModalVisible] = useState(false)
  const [isExpired, setisExpired] = useState([])
  const [sortValue, setSortValue] = useState([])
  const pageSize = tablet ? 12 : 10
  const { data, loading, error, refetch } = useQuery(QUERY_EVENTS, {
    variables: {
      searchText: searchText,
      offset: pageSize * pageIndex,
      limit: pageSize,
      isExpired: isExpired[0]?.value,
      orderBy: {
        field: sortValue[0]?.field,
        direction: sortValue[0]?.direction
      }
    },
    fetchPolicy: 'network-only'
  })
  // console.log(isExpired)
  const [deleteEvents] = useMutation(MUTATION_DELETE_EVENT, {
    fetchPolicy: 'no-cache'
  })
  const eventsData =
    data?.events?.events
      .map((item: any, idx: number) => ({
        ...item,
        idx: pageSize * pageIndex + idx + 1,
        key: item.id,
        owner: item.owner.nameKanji,
        link: item?.template?.id ? `/${item?.template?.id }?eventId=${item.id}` :`${LANDING_PAGE}?eventId=${item.id}`
      })) || []

  const total = data?.events.count ?? 0

  const onIsExpiredChange = (value: any) => {
    setisExpired(value)
    // refetch()
    if (pageIndex && pageIndex !== 0) setPageIndex(0)
  }

  const onSortTypeChange = (value: any) => {
    setSortValue(value)
    // refetch()
    if (pageIndex && pageIndex !== 0) setPageIndex(0)
  }
  const rowSelection = {
    selectedRowKeys: selectedRows.map((item) => item.key),
    onChange: (selectedRowKeys, selectedRows) => {
      setSelectedRows(selectedRows)

      if (selectedRowKeys.length > 1) {
        setDisableEdit(true)
        setDisableDelete(false)
      } else if (selectedRowKeys.length > 0) {
        setDisableEdit(false)
        setDisableDelete(false)
      } else {
        setDisableEdit(true)
        setDisableDelete(true)
      }
    }
  }

  const deleteHandler = () => {
    confirm({
      type: 'warning',
      title: '確認',
      content: '選択したアイテムを削除してもよろしいですか？',
      cancelText: 'キャンセル',
      icon: <ExclamationCircleOutlined />,
      okText: 'OK',
      onOk: () =>
        deleteEvents({
          variables: {
            ids: selectedRows.map((item) => item.id)
          }
        })
          .then(() => {
            setSelectedRows([])
            setDisableDelete(true)
            setDisableEdit(true)
            refetch()
            Notification({
              type: 'success',
              message: '成功',
              description: '削除に成功しました。'
            })
          })
          .catch(({ graphQLErrors }) =>
            Notification({
              type: 'error',
              message: ' エラー',
              description: graphQLErrors[0]?.message
            })
          )
    })
  }
  const [eventId, setEventId] = useState<any>()
  const handleOpenForm = (type) => {
    const newSearch =
      type === 'create'
        ? { type: type }
        : {
            eventId: selectedRows[0].id,
            type: type
          }
          if(type === 'create'){
            window.open("/event?type=create","_blank")
          } else {
            history.push({
              search: qs.stringify(newSearch)
            })
          }

  }
  const handleExportqr = () => {
    const win = window.open(
      `${PDF_EXPORT}?eventId=${selectedRows[0].id}`,
      '_blank'
    )
    win.focus()
  }

  const columns = EventColumns.map((col) => {
    if (!col.editable) {
      return col
    }
    return {
      ...col,
      onCell: (record: any) => ({
        record,
        editable: col.editable,
        dataIndex: col.dataIndex,
        title: col.title,
        setEventId: setEventId,
        setIsVisible: setIsModalVisible
      })
    }
  })
  const components = {
    body: {
      cell: EditableCell
    }
  }

  return (
    <>
      {eventId && (
        <Modal
          width={'600px'}
          destroyOnClose={true}
          title={
            <h2>
              <span style={{ color: 'rgb(0, 197, 141)' }}>賞品</span>
            </h2>
          }
          visible={isModalVisible}
          footer={null}
          onCancel={() => setIsModalVisible(false)}
        >
          <ConfirmPrize eventId={eventId} />
        </Modal>
      )}
      <Header>
        <NameCol lg={3} md={3} sm={3}>
          {/* <Heading>イベント管理</Heading> */}
          <ActionWrapper>
            {localStorage.role !== 'agency' &&
            <>
            <AddIcon
              color={'#00C58D'}
              title={'イベント追加'}
              onClick={() => handleOpenForm('create')}
            />
            <EditIcon
            disabled={disableEdit}
              color={disableEdit && '#ccc'}
              title={'イベント編集'}
              onClick={
                !disableEdit
                  ? () => {
                      handleOpenForm('edit')
                    }
                  : undefined
              }
            />
            <DeleteIcon
              disabled={disableDelete}
              color={disableDelete ? '#ccc' : '#f00'}
              onClick={!disableDelete ? () => deleteHandler() : undefined}
              title={'イベント削除'}
              margin={'0 0.5rem'}
              />
              </>
            }
            <ExportIcon
              disabled={disableEdit}
              color={disableEdit ? '#ccc' : '#11a4df'}
              onClick={!disableEdit ? () => handleExportqr() : undefined}
              title={'QRリストのエクスポート'}
            />
          </ActionWrapper>
        </NameCol>
        <ActionCol lg={9} md={9} sm={9}>
          <SearchWrapper>
            <SeachText
              setText={setSearchText}
              pageIndex={pageIndex}
              setPageIndex={setPageIndex}
            />
          </SearchWrapper>
          <SelectWrapper>
            {' '}
            <Select
              name='filterExpired'
              labelKey='label'
              valueKey='value'
              size='compact'
              options={IsExpiredOptions}
              clearable={true}
              searchable={false}
              value={isExpired}
              placeholder={'イベントステータス'}
              onChange={({ value }) => onIsExpiredChange(value)}
            />
          </SelectWrapper>
          <SelectWrapper>
            <Select
              name='sortType'
              labelKey='label'
              valueKey='value'
              size='compact'
              options={SortOptions}
              clearable={true}
              searchable={false}
              value={sortValue}
              placeholder={'並び順'}
              onChange={({ value }) => onSortTypeChange(value)}
            />
          </SelectWrapper>
        </ActionCol>
      </Header>
      <TableWrapper>
        <Table
          bordered
          loading={loading}
          components={components}
          scroll={{ x: 1270 }}
          style={{
            overflow: 'auto',
            background: 'white',
            border: '1px solid #ddd'
          }}
          rowSelection={{
            type: 'checkbox',
            ...rowSelection
          }}
          columns={columns}
          dataSource={eventsData}
          // bordered
          pagination={{
            total: total,
            pageSize: pageSize,
            current: pageIndex + 1,
            onChange: (value) => setPageIndex(value - 1),
            showSizeChanger: false,
            position: ['bottomCenter']
          }}
        />{' '}
      </TableWrapper>
      {/* <ConfirmPrize /> */}
    </>
  )
}
