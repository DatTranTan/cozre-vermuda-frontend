export interface PrizeInput {
  id: string;
  name: string;
  rank: number;
  rate: number;
  quantity: number;
  message: string;
  imageUrl: string;
  url: string;
  createdAt: any;
}

export interface EventFormInput {
  name: string;
  startdate: number;
  enddate: number;
  videoid: string;
  noPrizeVideoId: string;
  videoBackground?: string;
  memo: string;
  bannerUrl: string;
  footerUrl1?: any;
  footerUrl2?: any;
  prizes: [PrizeInput];
  checkDateStartandEnd: boolean;
  topBannerUrl: string;
  leftFooterBannerUrl?: string;
  rightFooterBannerUrl?: string;
  questionnaireId: string;
  customerLost: string;
  timeOut: number;
  spinLimit: number;
  campaignId: string;
  txtBtnWin: string;
  txtBtnLost: string;
}

enum Options {
  OP_0 = "numberOfLotteryPeople",
  OP_1 = "createdAt",
}

enum Directions {
  ASC = "ASC",
  DESC = "DESC",
}

export const IsExpiredOptions: any = [
  { value: true, label: "有効期限切れ" },
  { value: false, label: "有効期限内" },
];
export const SortOptions = [
  {
    value: 0,
    label: "追加日新しい順",
    field: Options.OP_1,
    direction: Directions.DESC,
  },
  {
    value: 1,
    label: "追加日古い順",
    field: Options.OP_1,
    direction: Directions.ASC,
  },
  {
    value: 2,
    label: "抽選回数少ない順",
    field: Options.OP_0,
    direction: Directions.ASC,
  },
  {
    value: 3,
    label: "抽選回数多い順",
    field: Options.OP_0,
    direction: Directions.DESC,
  },
];
