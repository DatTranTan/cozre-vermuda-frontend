import React, { useCallback } from 'react'
import { useForm } from 'react-hook-form'
import { useMutation } from '@apollo/react-hooks'
import { Scrollbars } from 'react-custom-scrollbars'
// import { AuthContext, Role } from '../../context/auth'
import { useDrawerDispatch, useDrawerState } from '../../context/DrawerContext'
import Input from '../../components/Input/Input'
import Button, { KIND } from '../../components/Button/Button'
import DrawerBox from '../../components/DrawerBox/DrawerBox'
import { Row, Col } from '../../components/FlexBox/FlexBox'
import {
  Form,
  DrawerTitleWrapper,
  DrawerTitle,
  FieldDetails,
  ButtonGroup
} from '../DrawerItems/DrawerItems.style'
import {
  FormFields,
  FormLabel,
  Error
} from '../../components/FormFields/FormFields'
import Select from '../../components/Select/Select'
import { OverideCancel, OverideSave, OverideSelect } from './Overrides'
import { Notification } from '../../components/Notification/NotificationCustom'
import { MUTATION_CREATE_CUSTOMER } from '../../graphql/mutation/create-customer'

type Props = any
const gender = [
  { value: 'male', label: 'Male' },
  { value: 'female', label: 'Female' },
  { value: 'other', label: 'Other' }
]

const PrizeWinnerForm: React.FC<Props> = (props) => {
  const dispatch = useDrawerDispatch()
  const actionType = useDrawerState('actionType')
  const fillData = useDrawerState('data')
  const genderRef = React.useRef(null)
  const closeDrawer = useCallback(() => {
    dispatch({ type: 'CLOSE_DRAWER' })
    dispatch({ type: 'SUCCESS' })
  }, [dispatch])
  const { register, handleSubmit, setValue, errors } = useForm()
  const [genderSelect, setGenderSelect] = React.useState(gender[0])

  const [createCustomer] = useMutation(MUTATION_CREATE_CUSTOMER, {
    fetchPolicy: 'no-cache'
  })
  const [updateCustomer] = useMutation(MUTATION_CREATE_CUSTOMER, {
    fetchPolicy: 'no-cache'
  })

  const onSubmit = (data) => {
    console.log(data)
    if (actionType === 'create') {
      createCustomer({
        variables: {
          createCustomerInput: {
            name: data.name,
            prizeId: data.prizeId ?? '2dcde700-d2db-436e-8584-b62c2a22ecc6',
            tel: data.phone,
            email: data.email,
            address: data.address,
            postalCode: data.postalcode,
            age: parseFloat(data.age),
            gender: data.gender
          }
        }
      })
        .then(() => {
          Notification({
            type: 'success',
            message: '成に成功しました。'
          })
          closeDrawer()
        })
        .catch((err) =>
          Notification({ type: 'error', message: err.toString() })
        )
    } else {
      updateCustomer({
        variables: {
          updateCustomerInput: {
            name: data.name,
            // eventId: data.eventname,
            // prizeId: data.prizeId ?? '2dcde700-d2db-436e-8584-b62c2a22ecc6',
            tel: data.phone,
            email: data.email,
            postalCode: data.postalcode,
            address: data.address,
            age: parseFloat(data.age),
            gender: data.gender
          }
        }
      })
        .then(() => {
          Notification({
            type: 'success',
            message: '新に成功しました。'
          })
          closeDrawer()
        })
        .catch((err) =>
          Notification({ type: 'error', message: err.toString() })
        )
    }
  }

  const handleChangeGender = ({ value, option }) => {
    setValue('gender', option.value)
    setGenderSelect(value)
  }

  React.useEffect(() => {
    console.log(fillData)

    setValue('id', fillData?.id)
    setValue('name', fillData?.name)
    setValue('eventname', fillData?.eventname)
    setValue('prizename', fillData?.prizename)
    setValue('email', fillData?.email)
    setValue('phone', fillData?.phone)
    setValue('postalcode', fillData?.postalCode)
    setValue('address', fillData?.address)
    setValue('age', fillData?.age)
    setValue('gender', fillData?.gender?.value)
    setGenderSelect({ label: fillData?.gender?.label, value: fillData?.id })
  }, [actionType, fillData, setValue])

  React.useEffect(() => {
    register({ name: 'name', required: true, minLength: 4, maxLength: 50 })
    register({ name: 'eventname', required: true })
    register({ name: 'prizename', required: true })
    register({ name: 'email', required: true })
    register({ name: 'postalcode', required: true, maxLength: 200 })
    register({ name: 'phone', required: true })
    register({ name: 'address', required: true })
    register({ name: 'age' })
    register({ name: 'gender' })
  }, [register])

  return (
    <>
      <DrawerTitleWrapper>
        <DrawerTitle>{actionType === 'create' ? '作成' : '編集'}</DrawerTitle>
      </DrawerTitleWrapper>

      <Form onSubmit={handleSubmit(onSubmit)} style={{ height: '100%' }}>
        <Scrollbars
          autoHide
          renderView={(props) => (
            <div {...props} style={{ ...props.style, overflowX: 'hidden' }} />
          )}
          renderTrackHorizontal={(props) => (
            <div
              {...props}
              style={{ display: 'none' }}
              className='track-horizontal'
            />
          )}
        >
          <Row>
            <Col lg={3}>
              <FieldDetails>
                必要な情報を記入して下さい。
                <br />
                <span style={{ color: 'red' }}>(*)は必須の項目です。</span>
              </FieldDetails>
            </Col>

            <Col lg={9}>
              <DrawerBox>
                <FormFields>
                  <FormLabel>
                    氏名&nbsp;<span style={{ color: 'red' }}>*</span>
                  </FormLabel>
                  <Input
                    disabled={actionType === 'edit' && true}
                    inputRef={register({
                      required: true,
                      maxLength: 50,
                      minLength: 4
                    })}
                    name='name'
                  />
                  {errors.name && errors.name.type === 'required' && (
                    <Error>これは必須の項目です。</Error>
                  )}
                  {errors.name && errors.name.type === 'minLength' && (
                    <Error>氏名は4文字以上設定してください。</Error>
                  )}
                  {errors.name && errors.name.type === 'maxLength' && (
                    <Error>氏名は50文字以下設定してください。</Error>
                  )}
                </FormFields>

                <FormFields>
                  <FormLabel>
                    イベント名&nbsp;<span style={{ color: 'red' }}>*</span>
                  </FormLabel>
                  <Input
                    inputRef={register({
                      required: true
                    })}
                    name='eventname'
                  />
                  {errors.eventname && errors.eventname.type === 'required' && (
                    <Error>これは必須の項目です。</Error>
                  )}
                </FormFields>
                <FormFields>
                  <FormLabel>
                    賞品名&nbsp;<span style={{ color: 'red' }}>*</span>
                  </FormLabel>
                  <Input
                    inputRef={register({ required: true })}
                    name='prizename'
                  />
                  {errors.prizename && errors.prizename.type === 'required' && (
                    <Error>これは必須の項目です。</Error>
                  )}
                </FormFields>
                <FormFields>
                  <FormLabel>メールアドレス</FormLabel>
                  <Input
                    inputRef={register({
                      required: true,
                      pattern: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                    })}
                    name='email'
                  />
                  {errors.email && errors.email.type === 'required' && (
                    <Error>これは必須の項目です。</Error>
                  )}
                  {errors.email && errors.email.type === 'pattern' && (
                    <Error>メールアドレスの形式は正しくありません。</Error>
                  )}
                </FormFields>
                <FormFields>
                  <FormLabel>電話番号</FormLabel>
                  <Input
                    name='phone'
                    inputRef={register({ required: true, maxLength: 20 })}
                  />
                  {errors.phone && errors.phone.type === 'required' && (
                    <Error>これは必須の項目です。</Error>
                  )}
                  {errors.phone && errors.phone.type === 'maxLength' && (
                    <Error>電話番号は20文字以下設定してください。</Error>
                  )}
                </FormFields>
                <FormFields>
                  <FormLabel>POSTAL CODE</FormLabel>
                  <Input
                    name='postalcode'
                    type='number'
                    inputRef={register({ required: true, maxLength: 200 })}
                  />
                  {errors.postalcode &&
                    errors.postalcode.type === 'required' && (
                      <Error>これは必須の項目です。</Error>
                    )}
                  {errors.postalcode &&
                    errors.postalcode.type === 'maxLength' && (
                      <Error>電話番号は200文字以下です。</Error>
                    )}
                </FormFields>
                <FormFields>
                  <FormLabel>住所</FormLabel>
                  <Input
                    inputRef={register({ required: true })}
                    name='address'
                  />
                  {errors.address && errors.address.type === 'required' && (
                    <Error>これは必須の項目です。</Error>
                  )}
                </FormFields>
                <FormFields>
                  <FormLabel>年齢</FormLabel>
                  <Input
                    type='number'
                    inputRef={register({})}
                    name='age'
                    min={0}
                    max={200}
                  />

                </FormFields>
                <FormFields>
                  <FormLabel>種別</FormLabel>
                  <Select
                    controlRef={genderRef}
                    options={gender}
                    clearable={false}
                    name='gender'
                    labelKey='label'
                    valueKey='value'
                    value={genderSelect}
                    placeholder={
                      actionType === 'create' ? '種別を選択してください' : ''
                    }
                    onChange={handleChangeGender}
                    overrides={OverideSelect}
                  />
                  {/* {errors.usertype && errors.usertype.type === 'required' && (
                    <Error>これは必須の項目です。</Error>
                  )} */}
                </FormFields>
              </DrawerBox>
            </Col>
          </Row>
        </Scrollbars>

        <ButtonGroup>
          <Button
            kind={KIND.minimal}
            onClick={closeDrawer}
            overrides={OverideCancel}
          >
            キャンセル
          </Button>
          <Button type='submit' overrides={OverideSave}>
            {actionType === 'create' ? '追加' : '更新する'}
          </Button>
        </ButtonGroup>
      </Form>
    </>
  )
}
export default PrizeWinnerForm
