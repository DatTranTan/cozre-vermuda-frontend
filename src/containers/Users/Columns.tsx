/* eslint-disable */
import {
  colTitleRender,
  renderCreateDate,
  renderRowCenter,
  renderRowNumber,
  renderRowText,
  renderPass,
  renderMaximum
} from '../../components/TableCell/TableCellRender'

export const columnsAdmin: any = [
  {
    title: colTitleRender('項番'),
    dataIndex: 'idx',
    width: 60,
    ellipsis: true,
    align: 'center',
    render: (value: any) => renderRowCenter(value)
  },
  {
    title: colTitleRender('ユーザーID'),
    dataIndex: 'id',
    width: '10%',
    ellipsis: true,
    render: (value: any) => renderRowText(value)
  },
  // {
  //   title: colTitleRender('パスワード'),
  //   dataIndex: 'pwd',
  //   width: '15%',
  //   cursor: 'pointer',
  //   ellipsis: true,
  //   render: (value: any) => renderPass(value)
  // },
  {
    title: colTitleRender('氏名'),
    dataIndex: 'nameKanji',
    width: '10%',
    ellipsis: true,
    render: (value: any) => renderRowText(value)
  },
  {
    title: colTitleRender('種別'),
    dataIndex: 'role',
    width: '10%',
    ellipsis: true,
    render: (value: any) => renderRowCenter(value)
  },
  {
    title: colTitleRender('企業作成上限'),
    dataIndex: 'maxClient',
    width: '10%',
    ellipsis: true,
    render: (value: any) => renderMaximum(value)
  },
  {
    title: colTitleRender('作成日'),
    dataIndex: 'createdAt',
    width: '10%',
    ellipsis: true,
    align: 'center',
    render: (value: any) => renderCreateDate(value)
  },
  {
    title: colTitleRender('会社名'),
    dataIndex: 'companyName',
    width: '10%',
    ellipsis: true,
    render: (value: any) => renderRowText(value)
  },
  {
    title: colTitleRender('電話番号'),
    dataIndex: 'tel',
    width: '10%',
    ellipsis: true,
    render: (value: any) => renderRowNumber(value)
  },
  {
    title: colTitleRender('メールアドレス'),
    dataIndex: 'email',
    width: '10%',
    ellipsis: true,
    render: (value: any) => renderRowText(value)
  },
  {
    title: colTitleRender('住所'),
    dataIndex: 'address',
    width: '10%',
    ellipsis: true,
    render: (value: any) => renderRowText(value)
  }
]

export const columns: any = [
  {
    title: colTitleRender('項番'),
    dataIndex: 'idx',
    width: 60,
    ellipsis: true,
    align: 'center',
    render: (value: any) => renderRowCenter(value)
  },
  {
    title: colTitleRender('ユーザーID'),
    dataIndex: 'id',
    width: '10%',
    ellipsis: true,
    render: (value: any) => renderRowText(value)
  },
  // {
  //   title: colTitleRender('パスワード'),
  //   dataIndex: 'pwd',
  //   width: '15%',
  //   cursor: 'pointer',
  //   ellipsis: true,
  //   render: (value: any) => renderPass(value)
  // },
  {
    title: colTitleRender('氏名'),
    dataIndex: 'nameKanji',
    width: '10%',
    ellipsis: true,
    render: (value: any) => renderRowText(value)
  },
  {
    title: colTitleRender('種別'),
    dataIndex: 'role',
    width: '10%',
    ellipsis: true,
    render: (value: any) => renderRowCenter(value)
  },
  {
    title: colTitleRender('作成日'),
    dataIndex: 'createdAt',
    width: '10%',
    ellipsis: true,
    align: 'center',
    render: (value: any) => renderCreateDate(value)
  },
  {
    title: colTitleRender('会社名'),
    dataIndex: 'companyName',
    width: '10%',
    ellipsis: true,
    render: (value: any) => renderRowText(value)
  },
  {
    title: colTitleRender('電話番号'),
    dataIndex: 'tel',
    width: '10%',
    ellipsis: true,
    render: (value: any) => renderRowNumber(value)
  },
  {
    title: colTitleRender('メールアドレス'),
    dataIndex: 'email',
    width: '10%',
    ellipsis: true,
    render: (value: any) => renderRowText(value)
  },
  {
    title: colTitleRender('住所'),
    dataIndex: 'address',
    width: '10%',
    ellipsis: true,
    render: (value: any) => renderRowText(value)
  }
]
