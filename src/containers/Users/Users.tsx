/* eslint-disable */
import {
  ExclamationCircleOutlined
} from '@ant-design/icons'
import { useMutation, useQuery } from '@apollo/react-hooks'
import { Table } from 'antd'
import confirm from 'antd/lib/modal/confirm'
import React, { useCallback, useState } from 'react'
import {
  AddIcon, DeleteIcon, EditIcon
} from '../../components/ActionIcon/ActionIcon'
import { Notification } from '../../components/Notification/NotificationCustom'
import SeachText from '../../components/SeachText/BaseSeachText'
import Select from '../../components/Select/HeaderSelect'
import { useDrawerDispatch, useDrawerState } from '../../context/DrawerContext'
import { MUTATION_DELETE_USER } from '../../graphql/mutation/delete-user'
import { QUERY_MAXCLIENT } from '../../graphql/query/user'
import { QUERY_USERS } from '../../graphql/query/users'
import { useDeviceType } from '../../settings/useDeviceType'
import { ActionCol, ActionWrapper, Header, SearchWrapper, TableWrapper, TitleCol } from '../Users/Users.style'
import { columns, columnsAdmin } from './Columns'

const SelectOptions = [
  { key: 0, value: '2', label: '代理店' },
  { key: 1, value: '3', label: '企業' }
]
export default function Users() {
  const [searchText, setSearchText] = useState('')
  const isFinished = useDrawerState('isFinished')
  const [selectValue, setSelectValue] = useState([])
  const [disableEdit, setDisableEdit] = useState(true)
  const [disableDelete, setDisableDelete] = useState(true)
  const [selectedRows, setSelectedRows] = useState([])
  const [pageIndex, setPageIndex] = useState(0)

  const { tablet } = useDeviceType(window.navigator.userAgent)
  const pageSize = tablet ? 12 : 10
  const { data, loading, error, refetch } = useQuery(QUERY_USERS, {
    variables: {
      searchText,
      skip: pageSize * pageIndex,
      take: pageSize,
      userType: selectValue[0]?.value || ''
    },
    fetchPolicy: 'network-only'
  })

  const userDetail = useQuery(QUERY_MAXCLIENT, {
    variables: {
      id: localStorage.getItem('userId')
    },
    fetchPolicy: 'network-only'
  })

  const [deleteUser] = useMutation(MUTATION_DELETE_USER, {
    fetchPolicy: 'no-cache'
  })
  const userData =
    data?.users?.users
      .map((item, idx) => ({
        ...item,
        maxClient: item.userType.id === '3' ? '--/--' : item.maxClient,
        idx: pageSize * pageIndex + idx + 1,
        key: item.id,
        role: item.userType.name
      }))
      .filter((item) => {
        return item.id !== localStorage.getItem('userId')
      }) ?? []

  const checkCreateUser = () => {
    if (localStorage.role === 'agency') {
      console.log('userDetail: ', userDetail)
      let X = userDetail?.data?.user.maxClient
      let Y = data?.users?.count
      if (X < 0) {
        return false
      }
      if (X <= Y) {
        return true
      }
      return false
    }
    return false
  }
  const total = data?.users.count ?? 0

  const dispatch = useDrawerDispatch()

  const openEditUserForm = useCallback(
    (data) => {
      dispatch({
        type: 'OPEN_DRAWER',
        drawerComponent: 'USER_FORM',
        data: data,
        actionType: 'edit'
      })
    },
    [dispatch]
  )

  const openAddUserForm = useCallback(
    () =>
      dispatch({
        type: 'OPEN_DRAWER',
        drawerComponent: 'USER_FORM',
        data: {},
        actionType: 'create'
      }),
    [dispatch]
  )

  const onSelectChange = (value: any) => {
    setSelectValue(value)
    if (pageIndex && pageIndex !== 0) setPageIndex(0)
  }

  const rowSelection = {
    selectedRowKeys: selectedRows.map((item) => item.key),
    onChange: (selectedRowKeys, selectedRows) => {
      setSelectedRows(selectedRows)

      if (selectedRowKeys.length > 1) {
        setDisableEdit(true)
        setDisableDelete(false)
      } else if (selectedRowKeys.length > 0) {
        setDisableEdit(false)
        setDisableDelete(false)
      } else {
        setDisableEdit(true)
        setDisableDelete(true)
      }
    }
  }

  const deleteHandler = () => {
    confirm({
      type: 'warning',
      title: '確認',
      content:
        'ユーザーを削除すると関連イベントも削除されます。よろしいですか？',
      icon: <ExclamationCircleOutlined />,
      cancelText: 'キャンセル',
      okText: 'OK',
      onOk: () =>
        deleteUser({
          variables: {
            ids: selectedRows.map((item) => item.id)
          }
        })
          .then(() => {
            setSelectedRows([])
            setDisableDelete(true)
            setDisableEdit(true)
            refetch()
            Notification({
              type: 'success',
              message: '成功',
              description: '削除に成功しました。'
            })
          })
          .catch(({ graphQLErrors }) =>
            Notification({
              type: 'error',
              message: ' エラー',
              description: graphQLErrors[0]?.message
            })
          )
    })
  }

  React.useEffect(() => {
    refetch()
    setSelectedRows([])
    setDisableEdit(true)
    setDisableDelete(true)
  }, [isFinished, refetch])

  return (
    <>
      <Header>
        <TitleCol xl={6} lg={6} md={6}>
          {/* <Heading>アカウント管理</Heading> */}
          <ActionWrapper>
            <AddIcon
              color={
                checkCreateUser() || localStorage.role === 'client'
                  ? '#ccc'
                  : '#00C58D'
              }
              title={'ユーザー追加'}
              onClick={
                checkCreateUser() || localStorage.role === 'client'
                  ? undefined
                  : () => openAddUserForm()
              }
            />
            <EditIcon
              disabled={disableEdit}
              color={disableEdit && '#ccc'}
              title={'ユーザー編集'}
              onClick={
                !disableEdit
                  ? () => openEditUserForm(selectedRows[0])
                  : undefined
              }
            />
            <DeleteIcon
              disabled={disableDelete}
              color={disableDelete ? '#ccc' : '#f00'}
              onClick={!disableDelete ? () => deleteHandler() : undefined}
              title={'ユーザー削除'}
            />
            {localStorage.role === 'agency' && (
              // <div style={{width: '100%', marginLeft:'35px'}}>
              <ActionCol lg={24} md={24}>
                <div>
                  <span
                    style={{
                      fontSize: '15px',
                      fontFamily: 'Lato, sans-serif'
                    }}
                  >
                    現在の企業数:{' '}
                    <span style={{ fontWeight: 700 }}>
                      {data?.users?.count}
                    </span>
                  </span>
                </div>
                <div style={{ marginLeft: '10px' }}>
                  <span
                    style={{
                      fontSize: '15px',
                      fontFamily: 'Lato, sans-serif'
                    }}
                  >
                    企業作成上限:{' '}
                    <span style={{ fontWeight: 700 }}>
                      {userDetail?.data?.user.maxClient < 0
                        ? 'なし'
                        : userDetail?.data?.user.maxClient}
                    </span>
                  </span>
                </div>
              </ActionCol>
              // </div>
            )}
          </ActionWrapper>
        </TitleCol>
        <ActionCol xl={6} lg={6} md={6}>
          <SearchWrapper style={{ width: '50%' }}>
            <SeachText
              setText={setSearchText}
              pageIndex={pageIndex}
              setPageIndex={setPageIndex}
            />
          </SearchWrapper>
          <div style={{ width: '50%' }}>
            <Select
              name='filterUser'
              labelKey='label'
              valueKey='value'
              size='compact'
              options={SelectOptions}
              clearable={true}
              searchable={false}
              value={selectValue}
              placeholder={'ユーザー種別の絞り込み'}
              onChange={({ value }) => onSelectChange(value)}
            />
          </div>
        </ActionCol>
      </Header>
      <TableWrapper>
        <Table
          bordered
          loading={loading}
          scroll={{ x: 1270 }}
          style={{
            overflow: 'auto',
            background: 'white',
            border: '1px solid #ddd'
          }}
          rowSelection={{
            type: 'checkbox',
            ...rowSelection
          }}
          columns={localStorage.role === 'admin' ? columnsAdmin : columns}
          dataSource={userData}
          // bordered
          pagination={{
            total: total,
            pageSize: pageSize,
            current: pageIndex + 1,
            onChange: (value) => {
              refetch({ skip: pageSize * value - 1, take: pageSize }).then(
                () => {
                  setPageIndex(value - 1)
                }
              )
            },
            showSizeChanger: false,
            // onShowSizeChange: (current, size) => {
            //   setPageSize(size)
            //   console.log(current, size)
            // },
            position: ['bottomCenter']
          }}
        />{' '}
      </TableWrapper>
    </>
  )
}
