import { PLACEMENT } from 'baseui/popover'

const getInputFontStyle = ({ $theme }) => {
  return {
    color: $theme.colors.textDark,
    ...$theme.typography.fontBold14
  }
}

export const OverideDatePicker = {
  Popover: {
    props: {
      overrides: {
        Body: {
          style: { zIndex: 10000 }
        }
      }
    }
  },
  Input: {
    props: {
      overrides: {
        Input: {
          style: ({ $theme }) => {
            return {
              ...getInputFontStyle({ $theme })
            }
          }
        }
      }
    }
  }
}
