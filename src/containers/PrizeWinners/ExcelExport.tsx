/* eslint-disable */
import React, { useState } from "react";
import Button from "../../components/Button/Button";
import { useStyletron } from "baseui";
import XlsxPopulate from "xlsx-populate";
import { ExcelExportIcon } from "../../components/ActionIcon/ActionIcon";
import { QUERY_CUSTOMERS } from "../../graphql/query/customers";
import { useQuery } from "@apollo/react-hooks";
import { Spin } from "antd";
import { LoadingOutlined } from "@ant-design/icons";
import { Notification } from "../../components/Notification/NotificationCustom";
var isJSON = require("is-json");

const StatusOptions = [
  { key: 0, value: "won", label: "当選者" },
  { key: 1, value: "preparing", label: "賞品準備中" },
  { key: 2, value: "sent", label: "賞品送付済み" },
];
const question = [
  { type: 0, question: "won", anwser: "当選者" },
  { type: 1, question: "preparing", anwser: "賞品準備中" },
  { type: 2, question: "sent", anwser: "賞品送付済み" },
];

const questionColums = [
  "L",
  "M",
  "N",
  "O",
  "P",
  "Q",
  "R",
  "S",
  "T",
  "U",
  "V",
  "W",
  "X",
  "Y",
  "Z",
];
var Promise = XlsxPopulate.Promise;
export default function PrizeWinners({
  statusValue,
  eventIdValue,
  ownerIdValue,
  searchText,
}) {
  const { data, loading, error, refetch } = useQuery(QUERY_CUSTOMERS, {
    variables: {
      searchText: searchText,
      offset: 0,
      limit: 0,
      prizeStatus: statusValue[0]?.value,
      eventId: eventIdValue[0]?.value,
      ownerId: ownerIdValue[0]?.value,
    },
    fetchPolicy: "network-only",
  });
  const [loadingDownload, setLoadingDownload] = useState(false);

  const getWorkbook = () => {
    return new Promise(function (resolve, reject) {
      var req = new XMLHttpRequest();
      var url = "template.xlsx";
      req.open("GET", url, true);
      req.responseType = "arraybuffer";
      req.onreadystatechange = function () {
        if (req.readyState === 4) {
          if (req.status === 200) {
            resolve(XlsxPopulate.fromDataAsync(req.response));
          } else {
            reject("Received a " + req.status + " HTTP code.");
          }
        }
      };

      req.send();
    });
  };
  const generate = (data) => {
    return getWorkbook().then(function (workbook) {
      // export excel data here
      data.map((item, index) => {
        workbook
          .sheet(0)
          .cell(`A${2 + index}`)
          .value(index + 1);
        workbook
          .sheet(0)
          .cell(`B${2 + index}`)
          .value(item.name);
        workbook
          .sheet(0)
          .cell(`C${2 + index}`)
          .value(item.event.name);
        workbook
          .sheet(0)
          .cell(`D${2 + index}`)
          .value(item.prizename);
        workbook
          .sheet(0)
          .cell(`E${2 + index}`)
          .value(item.email);
        workbook
          .sheet(0)
          .cell(`F${2 + index}`)
          .value(item.tel);
        workbook
          .sheet(0)
          .cell(`G${2 + index}`)
          .value(item.postalCode);
        workbook
          .sheet(0)
          .cell(`H${2 + index}`)
          .value(item.address);
        workbook
          .sheet(0)
          .cell(`I${2 + index}`)
          .value(
            StatusOptions.filter((i) => i.value === item.prizeStatus)[0].label
          );
        workbook
          .sheet(0)
          .cell(`J${2 + index}`)
          .value(item.age);
        workbook
          .sheet(0)
          .cell(`K${2 + index}`)
          .value(item.gender);
        const List = [];
        const colum = [...questionColums];
        if (item.questionnaire) {
          if (isJSON.strict(item.questionnaire)) {
            let newItem: any = JSON.parse(item.questionnaire);
            if (newItem.length > 0) {
              newItem.map((item) => List.push(item));
            }
          } else {
            for (let i = 0; i < item.questionnaire.length; i = i + 2) {
              List.push({
                question: item.questionnaire[i],
                answer: item.questionnaire[i + 1],
              });
            }
          }
          List.map((itemQ, indexQ) => {
            workbook
              .sheet(0)
              .cell(`${colum[indexQ] + 1}`)
              .value("アンケート項目" + (indexQ + 1));
            workbook
              .sheet(0)
              .cell(`${colum[indexQ] + (2 + index)}`)
              .value(itemQ.question);
            colum.splice(indexQ, 1);
            workbook
              .sheet(0)
              .cell(`${colum[indexQ] + 1}`)
              .value("回答");
            workbook
              .sheet(0)
              .cell(`${colum[indexQ] + (2 + index)}`)
              .value(
                Array.isArray(itemQ?.answer)
                  ? itemQ?.answer?.length > 0
                    ? itemQ?.answer?.join()
                    : itemQ?.answer
                  : itemQ?.answer || itemQ?.content || ""
              );
          });
        }
      });
      return workbook.outputAsync();
    });
  };
  const generateBlob = () => {
    setLoadingDownload(true);
    refetch()
      .then(({ data }) => {
        const customersData =
          data?.customers.customers.map((item: any, idx: number) => ({
            ...item,
            idx: idx + 1,
            key: item.id,
            prizename: item.prize?.name,
          })) || [];

        return generate(customersData)
          .then(function (blob) {
            const nerWindow = window.navigator as any;
            if (nerWindow && nerWindow.msSaveOrOpenBlob) {
              nerWindow.msSaveOrOpenBlob(blob, "当選者.xlsx");
            } else {
              var url = window.URL.createObjectURL(blob);
              var a = document.createElement("a");
              document.body.appendChild(a);
              a.href = url;
              a.download = "当選者.xlsx";
              a.click();
              window.URL.revokeObjectURL(url);
              document.body.removeChild(a);
            }
            setLoadingDownload(false);
          })
          .catch(function (err) {
            alert(err.message || err);
            throw err;
          });
      })
      .catch(function (err) {
        setLoadingDownload(false);
        Notification({
          type: "error",
          message: " エラー",
          description:
            "ファイルをダウンロードできません。インターネット接続を確認してやり直してください。",
        });
        throw err;
      });
  };

  return (
    <>
      {loadingDownload ? (
        <Spin
          style={{
            color: "rgb(0, 197, 141)",
          }}
          spinning={loadingDownload}
          indicator={
            <LoadingOutlined
              style={{
                fontSize: 21,
              }}
              spin
            />
          }
        >
          <ExcelExportIcon
            margin={"0"}
            color={"rgb(0, 197, 141)"}
            title={"エクスポート"}
          />
        </Spin>
      ) : (
        <ExcelExportIcon
          color={"rgb(0, 197, 141)"}
          onClick={() => generateBlob()}
          title={"エクスポート"}
        />
      )}
    </>
  );
}
