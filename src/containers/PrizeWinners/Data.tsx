const dataTable = {
  customers: [
    {
      id: '1',
      name: '抽選者一覧01',
      eventname: 'イベント01',
      prizename: '賞品名1',
      email: 'abc@123.com',
      phone: '01023456789',
      address: 'HCM-VN',
      age: '20',
      gender: { id: 0, label: 'Male' }
    },
    {
      id: '2',
      name: '抽選者一覧02',
      eventname: 'イベント02',
      prizename: '賞品名2',
      email: 'abc@123.com',
      phone: '01023456789',
      address: 'HCM-VN',
      age: '20',
      gender: { id: 0, label: 'Male' }
    },
    {
      id: '3',
      name: '抽選者一覧03',
      eventname: 'イベント03',
      prizename: '賞品名3',
      email: 'abc@123.com',
      phone: '01023456789',
      address: 'HCM-VN',
      age: '20',
      gender: { id: 0, label: 'Male' }
    },
    {
      id: '4',
      name: '抽選者一覧04',
      eventname: 'イベント04',
      prizename: '賞品名4',
      email: 'abc@123.com',
      phone: '01023456789',
      address: 'HCM-VN',
      age: '20',
      gender: { id: 0, label: 'Male' }
    },
    {
      id: '5',
      name: '抽選者一覧05',
      eventname: 'イベント05',
      prizename: '賞品名5',
      email: 'abc@123.com',
      phone: '01023456789',
      address: 'HCM-VN',
      age: '20',
      gender: { id: 1, label: 'Female' }
    },
    {
      id: '6',
      name: 'Customer1',
      eventname: 'イベント',
      prizename: '賞品名',
      email: 'abc@123.com',
      phone: '01023456789',
      address: 'HCM-VN',
      age: '20',
      gender: { id: 2, label: 'Other' }
    },
    {
      id: '7',
      name: 'Customer1',
      eventname: 'イベント',
      prizename: '賞品名',
      email: 'abc@123.com',
      phone: '01023456789',
      address: 'HCM-VN',
      age: '20',
      gender: { id: 1, label: 'Female' }
    },
    {
      id: '8',
      name: 'Customer1',
      eventname: 'イベント',
      prizename: '賞品名',
      email: 'abc@123.com',
      phone: '01023456789',
      address: 'HCM-VN',
      age: '20',
      gender: { id: 1, label: 'Female' }
    },
    {
      id: '9',
      name: 'Customer1',
      eventname: 'イベント',
      prizename: '賞品名',
      email: 'abc@123.com',
      phone: '01023456789',
      address: 'HCM-VN',
      age: '20',
      gender: { id: 1, label: 'Female' }
    },
    {
      id: '10',
      name: 'Customer1',
      eventname: 'イベント',
      prizename: '賞品名',
      email: 'abc@123.com',
      phone: '01023456789',
      address: 'HCM-VN',
      age: '20',
      gender: { id: 1, label: 'Female' }
    },
    {
      id: '11',
      name: 'Customer1',
      eventname: 'イベント',
      prizename: '賞品名',
      email: 'abc@123.com',
      phone: '01023456789',
      address: 'HCM-VN',
      age: '20',
      gender: { id: 0, label: 'Male' }
    },
    {
      id: '12',
      name: 'Customer1',
      eventname: 'イベント',
      prizename: '賞品名',
      email: 'abc@123.com',
      phone: '01023456789',
      address: 'HCM-VN',
      age: '20',
      gender: { id: 0, label: 'Male' }
    },
    {
      id: '13',
      name: 'Customer1',
      eventname: 'イベント',
      prizename: '賞品名',
      email: 'abc@123.com',
      phone: '01023456789',
      address: 'HCM-VN',
      age: '20',
      gender: { id: 0, label: 'Male' }
    },
    {
      id: '14',
      name: 'Customer1',
      eventname: 'イベント',
      prizename: '賞品名',
      email: 'abc@123.com',
      phone: '01023456789',
      address: 'HCM-VN',
      age: '20',
      gender: { id: 1, label: 'Female' }
    },
    {
      id: '15',
      name: 'Customer1',
      eventname: 'イベント',
      prizename: '賞品名',
      email: 'abc@123.com',
      phone: '01023456789',
      address: 'HCM-VN',
      age: '20',
      gender: { id: 0, label: 'Male' }
    },
    {
      id: '16',
      name: 'Customer1',
      eventname: 'イベント',
      prizename: '賞品名',
      email: 'abc@123.com',
      phone: '01023456789',
      address: 'HCM-VN',
      age: '20',
      gender: { id: 0, label: 'Male' }
    },
    {
      id: '17',
      name: 'Customer3',
      eventname: 'イベント',
      prizename: '賞品名',
      email: 'abc@123.com',
      phone: '01023456789',
      address: 'HCM-VN',
      age: '20',
      gender: { id: 1, label: 'Female' }
    },
    {
      id: '18',
      name: 'Customer1',
      eventname: 'イベント',
      prizename: '賞品名',
      email: 'abc@123.com',
      phone: '01023456789',
      address: 'HCM-VN',
      age: '20',
      gender: { id: 2, label: 'Other' }
    },
    {
      id: '19',
      name: 'Customer1',
      eventname: 'イベント',
      prizename: '賞品名',
      email: 'abc@123.com',
      phone: '01023456789',
      address: 'HCM-VN',
      age: '20',
      gender: { id: 0, label: 'Male' }
    },
    {
      id: '20',
      name: 'Customer1',
      eventname: 'イベント',
      prizename: '賞品名',
      email: 'abc@123.com',
      phone: '01023456789',
      address: 'HCM-VN',
      age: '20',
      gender: { id: 1, label: 'Female' }
    },
    {
      id: '21',
      name: 'Customer1',
      eventname: 'イベント',
      prizename: '賞品名',
      email: 'abc@123.com',
      phone: '01023456789',
      address: 'HCM-VN',
      age: '20',
      gender: { id: 1, label: 'Female' }
    },
    {
      id: '22',
      name: 'Customer1',
      eventname: 'イベント',
      prizename: '賞品名',
      email: 'abc@123.com',
      phone: '01023456789',
      address: 'HCM-VN',
      age: '20',
      gender: { id: 2, label: 'Other' }
    },
    {
      id: '23',
      name: 'Customer1',
      eventname: 'イベント',
      prizename: '賞品名',
      email: 'abc@123.com',
      phone: '01023456789',
      address: 'HCM-VN',
      age: '20',
      gender: { id: 0, label: 'Male' }
    },
    {
      id: '24',
      name: 'Customer1',
      eventname: 'イベント',
      prizename: '賞品名',
      email: 'abc@123.com',
      phone: '01023456789',
      address: 'HCM-VN',
      age: '20',
      gender: { id: 2, label: 'Other' }
    },
    {
      id: '25',
      name: 'Customer1',
      eventname: 'イベント',
      prizename: '賞品名',
      email: 'abc@123.com',
      phone: '01023456789',
      address: 'HCM-VN',
      age: '20',
      gender: { id: 1, label: 'Female' }
    },
    {
      id: '26',
      name: 'Customer1',
      eventname: 'イベント',
      prizename: '賞品名',
      email: 'abc@123.com',
      phone: '01023456789',
      address: 'HCM-VN',
      age: '20',
      gender: { id: 2, label: 'Other' }
    },
    {
      id: '27',
      name: 'Customer1',
      eventname: 'イベント',
      prizename: '賞品名',
      email: 'abc@123.com',
      phone: '01023456789',
      address: 'HCM-VN',
      age: '20',
      gender: { id: 0, label: 'Male' }
    },
    {
      id: '28',
      name: 'Customer1',
      eventname: 'イベント',
      prizename: '賞品名',
      email: 'abc@123.com',
      phone: '01023456789',
      address: 'HCM-VN',
      age: '20',
      gender: { id: 0, label: 'Male' }
    }
  ],
  categories: []
}

const SelectOptions = [
  { value: 'all', label: 'すべて' },
  { value: 'eventname', label: 'イベント名' },
  { value: 'prizename', label: '賞品名' }
]
export { dataTable, SelectOptions }
