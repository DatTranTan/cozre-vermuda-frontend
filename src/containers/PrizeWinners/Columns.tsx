import {
  colTitleRender,
  renderRowCenter,
  renderRowNumber,
  renderRowText,
  renderRowPopoverCustomer,
} from "../../components/TableCell/TableCellRender";

export const Columns: any = [
  {
    title: colTitleRender("項番"),
    dataIndex: "idx",
    width: 60,
    ellipsis: true,
    render: (value: any) => renderRowCenter(value),
  },
  {
    title: colTitleRender("氏名"),
    dataIndex: "name",
    width: "15%",
    ellipsis: true,
    render: (value: any) => renderRowText(value),
  },
  {
    title: colTitleRender("イベント名"),
    dataIndex: "event",
    width: "10%",
    ellipsis: true,
    render: (value: any) => renderRowText(value.name),
  },
  {
    title: colTitleRender("賞品名"),
    dataIndex: "prizename",
    width: "10%",
    ellipsis: true,
    render: (value: any) => renderRowText(value),
  },
  {
    title: colTitleRender("メールアドレス"),
    dataIndex: "email",
    width: "15%",
    ellipsis: true,
    render: (value: any) => renderRowText(value),
  },
  {
    title: colTitleRender("電話番号"),
    dataIndex: "tel",
    width: "10%",
    ellipsis: true,
    render: (value: any) => renderRowText(value),
  },
  {
    title: colTitleRender("郵便番号"),
    dataIndex: "postalCode",
    width: "15%",
    ellipsis: true,
    render: (value: any) => renderRowText(value),
  },
  {
    title: colTitleRender("住所"),
    dataIndex: "address",
    width: "15%",
    ellipsis: true,
    render: (value: any) => renderRowText(value),
  },
  {
    title: colTitleRender("状態"),
    dataIndex: "prizeStatus",
    width: "15%",
    editable: true,
  },
  {
    title: colTitleRender("年齢"),
    dataIndex: "age",
    width: "10%",
    ellipsis: true,
    render: (value: any) => renderRowNumber(value),
  },
  {
    title: colTitleRender("性別"),
    dataIndex: "gender",
    width: "10%",
    ellipsis: true,
    render: (value: any) => renderRowText(value),
  },
  {
    title: colTitleRender("アンケート"),
    dataIndex: "questionnaire",
    width: 105,
    ellipsis: false,
    render: (value: any) =>
      value === null || value.length === 0 || value.length === 1
        ? renderRowText("なし")
        : renderRowPopoverCustomer(value),
  },
];
