/* eslint-disable */
import React, { memo, useState } from 'react'
import { Grid, Col, Wrapper, Row, Label } from './Stastistic.style'
import StickerCard from '../StickerCard/StickerCard'
import accessIcon from '../../../assets/images/access.png'
import winnersIcon from '../../../assets/images/winners.jpg'
import submitIcon from '../../../assets/images/submit.png'
import spinIcon from '../../../assets/images/spin.jpg'
import { useLazyQuery, useQuery } from '@apollo/react-hooks'
import { QUERY_STATISTICS } from '../../../graphql/query/statistics'
import { QUERY_EVENTS, QUERY_AGENCYS } from '../../../graphql/query/customers'
import { StatHeader, StatSelectWrapper } from './Stastistic.style'
import Select from '../../../components/Select/ModalSelect'
import ja from 'date-fns/locale/ja'
import { DatePicker } from '../../../components/DatePicker/DatePicker'
import dayjs from 'dayjs'
import { Result, Spin } from 'antd'

interface Props {}
const Statistic: React.FC<Props> = () => {
  const [eventIdValue, setEventIdValue] = useState([])
  const [ownerIdValue, setOwnerIdValue] = useState([])
  const [dateFrom, setDateFrom] = useState(null)
  const [dateTo, setDateTo] = useState(null)
  const [ownersData, setOwnersData] = React.useState<any>('')
  const [searchEventText, setSearchEventText] = useState('')
  const [searchOwnerText, setSearchOwnerText] = React.useState<any>('')
  const { data, loading, error } = useQuery(QUERY_STATISTICS, {
    variables: {
      eventId: eventIdValue[0]?.value,
      ownerId: ownerIdValue[0]?.value,
      timeFrom: Date.parse(dateFrom?.toDateString()),
      timeTo: Date.parse(dateTo?.toDateString())
    },
    fetchPolicy: 'network-only'
  })

  const { data: eventData, loading: loadingEvent } = useQuery(QUERY_EVENTS, {
    variables: {
      limit: 0,
      offset: 0,
      ownerId: ownerIdValue[0]?.value
    },
    fetchPolicy: 'network-only'
  })

  const events =
    eventData?.events?.events
      .map((item: any, index: number) => ({
        key: index,
        value: item.id,
        label: item.name
      }))
      .filter((item: any) => {
        const text = item.label.toLowerCase()
        return (
          text.includes(searchEventText.toLowerCase()) || searchEventText === ''
        )
      }) || []

  const [queryAgencys, { loading: loadingUser }] = useLazyQuery(QUERY_AGENCYS, {
    fetchPolicy: 'network-only',
    onCompleted: (data) => {
      let agencys = data?.agencys?.users.map((item: any, index: any) => ({
        key: index,
        value: item.id,
        label: `${item.id} (${item.nameKanji})`
      }))
      setOwnersData(agencys)
    }
  })

  const onEventIdChange = (value: any) => {
    // console.log('event', value)
    setEventIdValue(value)
  }
  const onOwnerChange = (value: any) => {
    // console.log('owner', value)
    setOwnerIdValue(value)
    setEventIdValue([])
  }

  React.useEffect(() => {
    queryAgencys({
      variables: {
        searchText: searchOwnerText,
        skip: 0,
        take: 0
      }
    })
  }, [searchOwnerText])

  const { numberOfLotteryPeople, totalVisit, totalForm, totalPeopleWin } =
    data?.statistics ?? {}

  const debounce = (fn: any, delay: number) => {
    return (args: any) => {
      clearTimeout(fn.id)

      fn.id = setTimeout(() => {
        fn.call(this, args)
      }, delay)
    }
  }
  const searchHandler = (search: any) => {
    if (search?.input === 0) {
      setSearchEventText(search?.value)
    }
    if (search?.input === 1) {
      setSearchOwnerText(search?.value)
    }
  }
  const debounceAjax = debounce(searchHandler, 600)

  const distance = dateTo ? ((dateTo - dateFrom) / 86400000).toFixed(0) : 0

  return (
    <Wrapper>
      <StatHeader>
        {localStorage.role !== 'client' && (
          <StatSelectWrapper>
            <Label>ユーザーID</Label>
            <Select
              name='ownerSelect'
              labelKey='label'
              valueKey='value'
              size='compact'
              disabled={loadingEvent}
              isLoading={loadingEvent}
              options={ownersData}
              clearable={true}
              searchable={true}
              value={ownerIdValue}
              zIndex={10000}
              placeholder={'ユーザーID検索'}
              onChange={({ value }) => onOwnerChange(value)}
              onInputChange={(e: any) =>
                debounceAjax({ value: e.target.value, input: 1 })
              }
            />
          </StatSelectWrapper>
        )}
        <StatSelectWrapper>
          <Label>イベント名</Label>
          <Select
            name='eventSelect'
            labelKey='label'
            valueKey='value'
            size='compact'
            options={events}
            disabled={loadingUser}
            isLoading={loadingUser}
            clearable={true}
            searchable={true}
            value={eventIdValue}
            zIndex={10000}
            placeholder={'イベント名検索'}
            onChange={({ value }) => onEventIdChange(value)}
            onInputChange={(e: any) =>
              debounceAjax({ value: e.target.value, input: 0 })
            }
          />
        </StatSelectWrapper>
        <StatSelectWrapper>
          <Label>日付から</Label>
          <DatePicker
            locale={ja}
            value={dateFrom}
            size='compact'
            zIndex={10000}
            clearable
            formatString='yyyy.MM.dd'
            onChange={({ date }) => {
              const dateParse = Array.isArray(date) ? date[0] : date
              if (dayjs(dateParse).isValid() || dateParse === null) {
                setDateFrom(dateParse)
                // if (dateTo === null) {
                //   setDateTo(new Date())
                // }
              }
            }}
            placeholder='年.月.日'
          />
        </StatSelectWrapper>
        <StatSelectWrapper>
          <Label>日付まで</Label>
          <DatePicker
            locale={ja}
            value={dateTo}
            size='compact'
            zIndex={10000}
            clearable
            formatString='yyyy.MM.dd'
            placeholder='年.月.日'
            onChange={({ date }) => {
              const dateParse = Array.isArray(date) ? date[0] : date
              if (dayjs(dateParse).isValid() || dateParse === null) {
                setDateTo(dateParse)
              }
            }}
          />
        </StatSelectWrapper>
      </StatHeader>
      {!error ? (
        <Spin size={'large'} spinning={loading}>
          <Grid>
            <Row>
              <Col>
                <StickerCard
                  title='アクセス数'
                  subtitle={
                    distance > 0 ? `(${distance} 日で)` : '(日付すべて)'
                  }
                  icon={<img src={accessIcon} height={45} width={45} />}
                  price={`${totalVisit || 0}人`}
                  // indicator='up'
                  // indicatorText='Revenue up'

                  // link='#'
                  // linkText='Full Details'
                />
              </Col>
              <Col>
                <StickerCard
                  title='抽選応募者'
                  subtitle={
                    distance > 0 ? `(${distance} 日で)` : '(日付すべて)'
                  }
                  icon={<img src={spinIcon} height={45} width={45} />}
                  price={`${numberOfLotteryPeople || 0}人`}
                  // indicator='down'
                  // indicatorText='Order down'
                  // note='(previous 30 days)'
                  // link='#'
                  // linkText='Full Details'
                />
              </Col>
            </Row>
            <Row>
              <Col>
                <StickerCard
                  title='当選者'
                  subtitle={
                    distance > 0 ? `(${distance} 日で)` : '(日付すべて)'
                  }
                  icon={<img src={winnersIcon} height={45} width={45} />}
                  price={`${totalPeopleWin || 0}人`}
                  // indicator='up'
                  // indicatorText='Customer up'
                  // note='(previous 30 days)'
                  // link='#'
                  // linkText='Full Details'
                />
              </Col>
              <Col>
                <StickerCard
                  title='情報を入力した人数'
                  subtitle={
                    distance > 0 ? `(${distance} 日で)` : '(日付すべて)'
                  }
                  icon={<img src={submitIcon} height={45} width={45} />}
                  price={`${totalForm || 0}人`}
                  //  indicator='up'
                  //   indicatorText='Delivery up'
                  //   note='(previous 30 days)'
                  //   link='#'
                  //   linkText='Full Details'
                />
              </Col>
            </Row>
          </Grid>
        </Spin>
      ) : (
        <Result status='404' title='情報はありません。' />
      )}
    </Wrapper>
  )
}
export default memo(Statistic)
