/* eslint-disable */
import React, { useState, useEffect } from "react";
import { useDeviceType } from "../../settings/useDeviceType";
import { Modal, Table, Tooltip } from "antd";
import { useLazyQuery, useQuery } from "@apollo/react-hooks";
import {
  ActionCol,
  ActionWrapper,
  ButtonWrapper,
  CustomModal,
  Header,
  ModalTitle,
  SearchWrapper,
  SelectWrapper,
  TableWrapper,
  TitleCol,
} from "../PrizeWinners/PrizeWinners.style";
import {
  QUERY_AGENCYS,
  QUERY_CUSTOMERS,
  QUERY_EVENTS,
} from "../../graphql/query/customers";
import SeachText from "../../components/SeachText/BaseSeachText";
import EditableCell, { EditableCellProps } from "./EditableCell";
import { StaticticIcon } from "../../components/ActionIcon/ActionIcon";
import Select from "../../components/Select/HeaderSelect";
import Statistic from "./Stastistics/Statistic";
import { Columns } from "./Columns";
import Button from "../../components/Button/Button";
import ExcelExport from "./ExcelExport";
import { useStyletron } from "baseui";
import { AnalyticIcon } from "../../components/ActionIcon/ActionIcon";

const StatusOptions = [
  { key: 0, value: "won", label: "当選者" },
  { key: 1, value: "preparing", label: "賞品準備中" },
  { key: 2, value: "sent", label: "賞品送付済み" },
  // { key: 3, value: 'lost', label: 'ハズレ' }
];
export default function PrizeWinners() {
  const [css] = useStyletron();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [eventIdValue, setEventIdValue] = useState([]);
  const [ownerIdValue, setOwnerIdValue] = useState([]);
  const [statusValue, setStatusValue] = useState([]);
  // const [eventsData, setEventsData] = React.useState<any>('')
  const [ownersData, setOwnersData] = React.useState<any>("");
  const [pageIndex, setPageIndex] = useState(0);
  const [searchText, setSearchText] = React.useState<any>("");
  const [searchEventText, setSearchEventText] = useState("");
  const [customersData, setCustomersData] = useState([]);
  const [searchOwnerText, setSearchOwnerText] = React.useState<any>("");
  const { tablet } = useDeviceType(window.navigator.userAgent);
  const [total, setTotal] = useState(0);
  const pageSize = tablet ? 12 : 10;
  const { data, loading, error, refetch } = useQuery(QUERY_CUSTOMERS, {
    variables: {
      searchText: searchText,
      offset: pageSize * pageIndex,
      limit: pageSize,
      prizeStatus: statusValue[0]?.value,
      eventId: eventIdValue[0]?.value,
      ownerId: ownerIdValue[0]?.value,
    },
    fetchPolicy: "network-only",
  });

  const { data: eventData, refetch: refetchEvent } = useQuery(QUERY_EVENTS, {
    variables: {
      limit: 0,
      offset: 0,
      ownerId: ownerIdValue[0]?.value,
    },
    fetchPolicy: "network-only",
  });

  useEffect(() => {
    if (data) {
      const list =
        data?.customers.customers.map((item: any, idx: number) => ({
          ...item,
          idx: pageSize * pageIndex + idx + 1,
          key: item.id,
          prizename: item.prize?.name,
        })) || [];
      // .filter((item: any) => {
      //   const text = item.name.toLowerCase()
      //   return text.includes(searchText.toLowerCase()) || searchText === ''
      // }) || []
      setCustomersData(list);
      setTotal(data?.customers.count);
    }
  }, [data]);

  const events =
    eventData?.events?.events
      .map((item: any, index: number) => ({
        key: index,
        value: item.id,
        label: item.name,
      }))
      .filter((item: any) => {
        const text = item.label.toLowerCase();
        return (
          text.includes(searchEventText.toLowerCase()) || searchEventText === ""
        );
      }) || [];

  const [queryAgencys] = useLazyQuery(QUERY_AGENCYS, {
    fetchPolicy: "network-only",
    onCompleted: (data) => {
      let agencys = data?.agencys?.users.map((item: any, index: any) => ({
        key: index,
        value: item.id,
        label: `${item.id} (${item.nameKanji})`,
      }));
      setOwnersData(agencys);
    },
  });

  const onEventIdChange = (value: any) => {
    // console.log('event', value)
    setEventIdValue(value);
    if (pageIndex && pageIndex !== 0) setPageIndex(0);
  };
  const onOwnerChange = (value: any) => {
    // console.log('owner', value)
    setOwnerIdValue(value);
    setEventIdValue([]);
    if (pageIndex && pageIndex !== 0) setPageIndex(0);
  };
  const onStatusChange = (value: any) => {
    // console.log('status', value)
    setStatusValue(value);
    if (pageIndex && pageIndex !== 0) setPageIndex(0);
  };

  React.useEffect(() => {
    queryAgencys({
      variables: {
        searchText: searchOwnerText,
        skip: 0,
        take: 0,
      },
    });
  }, [searchOwnerText]);

  const columns = React.useMemo(
    () =>
      Columns.map((col) => {
        if (col.dataIndex === "prizeStatus") {
          return {
            ...col,
            onCell: (record: any) => {
              const editable = col.editable;

              const cellProps: EditableCellProps = {
                record,
                editable,
                dataIndex: col.dataIndex,
                title: col.title,
              };
              return cellProps;
            },
          };
        }

        if (!col.editable) {
          return col;
        }

        return {
          ...col,
          onCell: (record: any) => ({
            record,
            editable: col.editable,
            dataIndex: col.dataIndex,
            title: col.title,
          }),
        };
      }),
    []
  );

  const components = {
    body: {
      cell: (cellProps: EditableCellProps) => {
        let newCellProps = cellProps;

        // if (cellProps.dataIndex === 'prizeStatus' && !cellProps.editable) {
        //   newCellProps = {
        //     ...cellProps
        //     children: [
        //       <Tooltip placement='topLeft' title='ハズレ'>
        //         ハズレ
        //       </Tooltip>
        //     ]
        //   }
        // }

        return <EditableCell {...newCellProps} />;
      },
    },
  };

  const debounce = (fn: any, delay: number) => {
    return (args: any) => {
      clearTimeout(fn.id);

      fn.id = setTimeout(() => {
        fn.call(this, args);
      }, delay);
    };
  };
  const searchHandler = (search: any) => {
    if (search?.input === 0) {
      setSearchEventText(search?.value);
    }
    if (search?.input === 1) {
      setSearchOwnerText(search?.value);
    }
  };
  const debounceAjax = debounce(searchHandler, 600);

  return (
    <>
      <CustomModal
        title={
          <ModalTitle>
            イベントを絞り込んだ状態での統計情報を表示すること
          </ModalTitle>
        }
        visible={isModalVisible}
        footer={null}
        onCancel={() => setIsModalVisible(false)}
      >
        <Statistic />
      </CustomModal>
      <Header>
        <ButtonWrapper>
          <AnalyticIcon
            color={"rgb(0, 197, 141)"}
            onClick={() => setIsModalVisible(true)}
            title={"統計"}
          />
          <ExcelExport
            searchText={searchText}
            ownerIdValue={ownerIdValue}
            eventIdValue={eventIdValue}
            statusValue={statusValue}
          />
        </ButtonWrapper>
        <ActionWrapper>
          <SearchWrapper>
            <SeachText
              setText={setSearchText}
              pageIndex={pageIndex}
              setPageIndex={setPageIndex}
            />
          </SearchWrapper>
          {/* {localStorage.role !== 'client' && (
            <SelectWrapper>
              <Select
                name='ownerSelect'
                labelKey='label'
                valueKey='value'
                size='compact'
                options={ownersData}
                clearable={true}
                searchable={true}
                value={ownerIdValue}
                placeholder={'ユーザーID検索'}
                onChange={({ value }) => onOwnerChange(value)}
                onInputChange={(e: any) =>
                  debounceAjax({ value: e.target.value, input: 1 })
                }
              />
            </SelectWrapper>
          )} */}
          <SelectWrapper>
            <Select
              name="eventSelect"
              labelKey="label"
              valueKey="value"
              size="compact"
              options={events}
              clearable={true}
              searchable={true}
              value={eventIdValue}
              placeholder={"イベント名検索"}
              onChange={({ value }) => onEventIdChange(value)}
              onInputChange={(e: any) =>
                debounceAjax({ value: e.target.value, input: 0 })
              }
            />
          </SelectWrapper>
          <SelectWrapper>
            <Select
              name="statusSelect"
              labelKey="label"
              valueKey="value"
              size="compact"
              options={StatusOptions}
              clearable={true}
              searchable={false}
              value={statusValue}
              placeholder={"賞品配達状態"}
              onChange={({ value }) => onStatusChange(value)}
            />
          </SelectWrapper>
        </ActionWrapper>
      </Header>
      <TableWrapper>
        <Table
          bordered
          components={components}
          loading={loading}
          scroll={{ x: 1270 }}
          style={{
            overflow: "auto",
            background: "white",
            border: "1px solid #ddd",
          }}
          columns={columns}
          dataSource={customersData}
          pagination={{
            total: total,
            pageSize: pageSize,
            current: pageIndex + 1,
            onChange: (value) => setPageIndex(value - 1),
            showSizeChanger: false,
            position: ["bottomCenter"],
          }}
        />{" "}
      </TableWrapper>
    </>
  );
}
