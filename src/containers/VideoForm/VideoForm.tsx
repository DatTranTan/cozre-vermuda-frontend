/* eslint-disable */
import React, { useCallback, useContext, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useMutation } from '@apollo/react-hooks'
import { Scrollbars } from 'react-custom-scrollbars'
import { AuthContext, Role } from '../../context/auth'
import { useDrawerDispatch, useDrawerState } from '../../context/DrawerContext'
import Input from '../../components/Input/Input'
import Button, { KIND } from '../../components/Button/Button'
import DrawerBox from '../../components/DrawerBox/DrawerBox'
import { Row, Col } from '../../components/FlexBox/FlexBox'
import VideoUploader from '../../components/Uploader/VideoUploader'
import addPhoto from '../../utils/S3/upload'
import {
  Form,
  DrawerTitleWrapper,
  DrawerTitle,
  FieldDetails,
  ButtonGroup,
  RequiredCol
} from '../DrawerItems/DrawerItems.style'
import {
  FormFields,
  FormLabel,
  Error
} from '../../components/FormFields/FormFields'
import { MUTATION_CREATE_VIDEOS } from '../../graphql/mutation/videos'
import { MUTATION_UPDATE_VIDEO } from '../../graphql/mutation/update-video'
import { Notification } from '../../components/Notification/NotificationCustom'

type Props = any

const VideosForm: React.FC<Props> = (props) => {
  const dispatch = useDrawerDispatch()
  const actionType = useDrawerState('actionType')
  const fillData = useDrawerState('data')
  const UniqueID = Date.now() + '_' + Math.random().toString(36).substr(2, 34)
  const [albumName, setAlbumName] = useState(UniqueID)
  const [urlVideos, setUrlVideos] = useState(null)
  const [isUploading, setIsUploading] = useState(false)
  const [isChecked, setIsChecked] = useState(false)
  const [loadData, setLoadData] = useState({ loaded: 0, total: 0, unit: '' })
  const closeDrawer = useCallback(() => {
    dispatch({ type: 'CLOSE_DRAWER' })
    dispatch({ type: 'SUCCESS' })
  }, [dispatch])
  const { register, handleSubmit, setValue, errors } = useForm()

  const [createVideos] = useMutation(MUTATION_CREATE_VIDEOS, {
    fetchPolicy: 'no-cache'
  })
  const [updateVideos] = useMutation(MUTATION_UPDATE_VIDEO, {
    fetchPolicy: 'no-cache'
  })

  const callback = {
    onSuccess: (urls: any) => {
      setUrlVideos(urls)
      setIsUploading(false)
    },
    onError: (urls) => {
      setIsUploading(false)
    },
    onLoad: (loaded: number, total: number, unit: string) => {
      setLoadData({ loaded: loaded, total: total, unit: unit })
    }
  }

  const handleUploader = (files: any) => {
    setIsUploading(true)
    addPhoto(albumName, 'videos/', files, callback)
  }

  function onDeleteVideo() {
    setUrlVideos(null)
  }

  const onSubmit = (data: any) => {
    if (actionType === 'create' && urlVideos) {
      createVideos({
        variables: {
          createVideoInput: {
            name: data.name,
            url: urlVideos
          }
        }
      })
        .then(() => {
          Notification({
            type: 'success',
            message: '成功',
            description: '動画アップロードに成功しました。'
          })
          closeDrawer()
        })
        .catch(({ graphQLErrors }) => {
          Notification({
            type: 'error',
            message: ' エラー',
            description: graphQLErrors[0].message
          })
        })
    }
    if (actionType === 'edit' && urlVideos) {
      updateVideos({
        variables: {
          updateVideoInput: {
            id: data.id,
            name: data.name,
            url: urlVideos
          }
        }
      })
        .then(() => {
          Notification({
            type: 'success',
            message: '成功',
            description: '動画更新に成功しました。'
          })
          closeDrawer()
        })
        .catch(({ graphQLErrors }) => {
          Notification({
            type: 'error',
            message: ' エラー',
            description: graphQLErrors[0].message
          })
        })
    }
  }
  React.useEffect(() => {
    if (actionType === 'edit') {
      setValue('id', fillData?.id)
      setValue('name', fillData?.name)
      setValue('url', fillData?.name)
      setUrlVideos(fillData?.url)
    }
  }, [actionType])
  React.useEffect(() => {
    register({ name: 'name' }, { required: true, maxLength: 100 })
  }, [register])
  return (
    <>
      <DrawerTitleWrapper>
        <DrawerTitle>
          {actionType === 'create' ? '動画アップロード' : '動画変更'}
        </DrawerTitle>
      </DrawerTitleWrapper>

      <Form onSubmit={handleSubmit(onSubmit)} style={{ height: '100%' }}>
        <Scrollbars
          autoHide
          renderView={(props) => (
            <div {...props} style={{ ...props.style, overflowX: 'hidden' }} />
          )}
          renderTrackHorizontal={(props) => (
            <div
              {...props}
              style={{ display: 'none' }}
              className='track-horizontal'
            />
          )}
        >
          <Row>
            <RequiredCol lg={3}>
              <FieldDetails>
                必要な情報を記入して下さい。
                <br />
                <span style={{ color: 'red' }}>(*)は必須の項目です。</span>
              </FieldDetails>
            </RequiredCol>

            <Col lg={9}>
              <DrawerBox>
                {actionType === 'edit' && (
                  <FormFields>
                    <FormLabel>
                      動画ID&nbsp;<span style={{ color: 'red' }}>*</span>
                    </FormLabel>
                    <Input
                      inputRef={register({ required: true })}
                      name='id'
                      disabled
                      overrides={{
                        Input: {
                          style: ({ $theme }) => {
                            return {
                              color: 'gray'
                            }
                          }
                        }
                      }}
                    />
                  </FormFields>
                )}
                <FormFields>
                  <FormLabel>
                    動画名&nbsp;<span style={{ color: 'red' }}>*</span>
                  </FormLabel>
                  <Input
                    inputRef={register({ required: true, maxLength: 100 })}
                    name='name'
                  />
                  {errors.name && errors.name.type === 'required' && (
                    <Error>(*)は必須の項目です。</Error>
                  )}
                  {errors.name && errors.name.type === 'maxLength' && (
                    <Error>動画名は100文字以下設定してください。</Error>
                  )}
                </FormFields>
                <FormFields>
                  <FormLabel>
                    動画アップロード&nbsp;
                    <span style={{ color: 'red' }}>*</span>
                  </FormLabel>
                  <VideoUploader
                    disabled={isUploading}
                    onChange={handleUploader}
                    onDelete={onDeleteVideo}
                    videoURL={urlVideos}
                    progress={loadData}
                  />
                  {isChecked && !urlVideos && (
                    <Error>(*)は必須の項目です。</Error>
                  )}
                </FormFields>
              </DrawerBox>
            </Col>
          </Row>
        </Scrollbars>

        <ButtonGroup>
          <Button
            kind={KIND.minimal}
            onClick={closeDrawer}
            overrides={{
              BaseButton: {
                style: ({ $theme }) => ({
                  width: '50%',
                  borderTopLeftRadius: '3px',
                  borderTopRightRadius: '3px',
                  borderBottomRightRadius: '3px',
                  borderBottomLeftRadius: '3px',
                  borderColor: '#f5f5f5',
                  marginRight: '15px',
                  backgroundColor: '#ddd',
                  color: $theme.colors.red400
                })
              }
            }}
          >
            キャンセル
          </Button>

          <Button
            type='submit'
            disabled={isUploading}
            onClick={() => setIsChecked(true)}
            overrides={{
              BaseButton: {
                style: ({ $theme }) => ({
                  width: '50%',
                  borderTopLeftRadius: '3px',
                  borderTopRightRadius: '3px',
                  borderBottomRightRadius: '3px',
                  borderBottomLeftRadius: '3px'
                })
              }
            }}
          >
            {actionType === 'create' ? '追加する' : '更新する'}
          </Button>
        </ButtonGroup>
      </Form>
    </>
  )
}

export default VideosForm
