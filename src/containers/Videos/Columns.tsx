import React from 'react'
import {
  colTitleRender,
  renderCreateDate,
  // renderRowCenter,
  // renderRowNumber,
  renderRowText,
  renderURL
} from '../../components/TableCell/TableCellRender'

export const columns: any = [
  {
    title: colTitleRender('項番'),
    dataIndex: 'idx',
    width: 60,
    ellipsis: true,
    align: 'center',
    render: function Render(text) {
      return <div style={{ cursor: 'pointer' }}>{text}</div>
    }
  },
  {
    title: colTitleRender('動画名'),
    dataIndex: 'name',
    width: '25%',
    ellipsis: {
      showTitle: false
    },
    render: (value: any) => renderRowText(value)
  },
  {
    title: colTitleRender('作成日'),
    dataIndex: 'createdAt',
    width: '20%',
    ellipsis: true,
    render: (value: any) => renderCreateDate(value)
  },
  {
    title: colTitleRender('動画リンク'),
    dataIndex: 'url',
    width: '55%',
    ellipsis: {
      showTitle: false
    },
    render: (value: any) => renderURL(value)
  }
]
