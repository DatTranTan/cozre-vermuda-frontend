import { styled } from 'baseui'
import { Table } from 'antd'
import { Col } from '../../components/FlexBox/FlexBox'

export const Header = styled('header', () => ({
  display: 'flex',
  alignItems: 'center',
  flex: '0 1 auto',
  flexDirection: 'row',
  flexWrap: 'wrap',
  backgroundColor: '#ffffff',
  padding: '0 1rem',
  marginBottom: '1.5rem',
  minHeight: '70px',
  boxShadow: '0 0 8px rgba(0, 0 ,0, 0.1)',

  '@media only screen and (max-width: 1024px)': {
    padding: '0 0.5rem'
  },

  '@media only screen and (max-width: 990px)': {
    marginBottom: '1rem'
  },

  '@media only screen and (max-width: 767px)': {
    marginBottom: '0.5rem'
  }
}))

export const Heading = styled('a', ({ $theme }) => ({
  ...$theme.typography.fontBold18,
  color: $theme.colors.textDark,
  marginLeft: '16px',
  '@media only screen and (max-width: 768px)': {
    marginLeft: '0'
  }
}))
export const ImageWrapper = styled('div', ({ $theme }) => ({
  width: '40px',
  height: '40px',
  overflow: 'hidden',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: '20px',
  backgroundColor: $theme.colors.backgroundF7,
  boxShadow: '0 0 5px rgba(0, 0 , 0, 0.05)'
}))

export const Icon = styled('span', () => ({
  width: '100%',
  height: 'auto'
}))

export const TableWrapper = styled('div', () => ({
  width: '100%'
}))

export const CustomTable = styled(Table, () => ({
  overflow: 'auto',
  background: 'white',
  border: '1px solid #ddd'
}))

export const ToolWrapper = styled('div', () => ({
  justifyContent: 'flex-end',
  display: 'flex',
  flexDirection: 'row',
  marginBottom: '1rem',
  overflow: 'hidden'
}))

export const ActionWrapper = styled('div', () => ({
  display: 'flex',
  alignItems: 'center',

  span: {
    cursor: 'pointer',
    marginLeft: '1rem'
  },

  'span:hover': {
    color: '#40a9ff'
  }
}))

export const TitleCol = styled(Col, () => ({
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',

  '@media only screen and (max-width: 767px)': {
    marginBottom: '10px',

    ':last-child': {
      marginBottom: 0
    }
  },
  height: '100%'
}))

export const ActionCol = styled(Col, () => ({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'flex-end',
  alignItems: 'center',

  '@media only screen and (max-width: 767px)': {
    marginBottom: '10px',

    ':last-child': {
      marginBottom: 0
    }
  },

  height: '100%'
}))
export const SearchWrapper = styled('div', () => ({
  width: '70%',

  '@media only screen and (max-width: 800px)': {
    width: '100%'
  }
}))
export const VideoURL = styled('a', () => ({
  '@media only screen and (max-width: 1024px)': {
    display: 'flex',
    width: '100%',
    overflow: 'auto hidden',
    textOverflow: 'unset'
  }
}))
