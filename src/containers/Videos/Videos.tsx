import React, { useState, useCallback } from 'react'
import { useDrawerDispatch, useDrawerState } from '../../context/DrawerContext'
import { useDeviceType } from '../../settings/useDeviceType'
import { ExclamationCircleOutlined } from '@ant-design/icons'
import { useMutation, useQuery } from '@apollo/react-hooks'
import { QUERY_VIDEO } from '../../graphql/query/video'
import { DELETE_VIDEO } from '../../graphql/mutation/delete-videos'
import confirm from 'antd/lib/modal/confirm'
import { CustomTable, Header } from './Videos.style'
import { TitleCol, ActionCol, TableWrapper } from './Videos.style'
import { SearchWrapper, ActionWrapper } from './Videos.style'
import SeachText from '../../components/SeachText/BaseSeachText'
import { Notification } from '../../components/Notification/NotificationCustom'
import {
  AddIcon,
  DeleteIcon,
  EditIcon
} from '../../components/ActionIcon/ActionIcon'
import { columns } from './Columns'

export default function Users() {
  const [name, setSearchText] = useState('')
  const isFinished = useDrawerState('isFinished')
  const [disableEdit, setDisableEdit] = useState(true)
  const [disableDelete, setDisableDelete] = useState(true)
  const [selectedRows, setSelectedRows] = useState([])
  const [pageIndex, setPageIndex] = useState(0)
  const { tablet } = useDeviceType(window.navigator.userAgent)
  const pageSize = tablet ? 12 : 10
  const { data, loading, refetch } = useQuery(QUERY_VIDEO, {
    variables: {
      name,
      offset: pageSize * pageIndex,
      limit: pageSize
    },
    fetchPolicy: 'network-only'
  })
  const [deleteVideo] = useMutation(DELETE_VIDEO, {
    fetchPolicy: 'no-cache'
  })
  const videosData =
    data?.Videos?.videos.map((item, idx) => ({
      ...item,
      idx: pageSize * pageIndex + idx + 1,
      key: item.id
    })) ?? []

  const total = data?.Videos.count ?? 0

  const dispatch = useDrawerDispatch()

  const openAddVideorForm = useCallback(
    (data) =>
      dispatch({
        type: 'OPEN_DRAWER',
        drawerComponent: 'VIDEOS_FORM',
        data: data,
        actionType: 'create'
      }),
    [dispatch]
  )
  const openEditVideoForm = useCallback(
    (data) => {
      dispatch({
        type: 'OPEN_DRAWER',
        drawerComponent: 'VIDEOS_FORM',
        data: data[0],
        actionType: 'edit'
      })
    },
    [dispatch]
  )

  const rowSelection = {
    selectedRowKeys: selectedRows.map((item) => item.key),
    onChange: (selectedRowKeys, selectedRows) => {
      setSelectedRows(selectedRows)

      if (selectedRowKeys.length > 1) {
        setDisableEdit(true)
        setDisableDelete(false)
      } else if (selectedRowKeys.length > 0) {
        setDisableEdit(false)
        setDisableDelete(false)
      } else {
        setDisableEdit(true)
        setDisableDelete(true)
      }
    }
  }

  const deleteHandler = () => {
    confirm({
      type: 'warning',
      title: '確認',
      content: '選択したアイテムを削除してもよろしいですか？',
      okText: 'OK',
      cancelText: 'キャンセル',
      icon: <ExclamationCircleOutlined />,
      onOk: () =>
        deleteVideo({
          variables: {
            ids: selectedRows.map((item) => item.id)
          }
        })
          .then(async () => {
            await refetch()
            setSelectedRows([])
            setDisableDelete(true)
            setDisableEdit(true)
            Notification({
              type: 'success',
              message: '成功',
              description: '動画削除に成功しました。'
            })
          })
          .catch(({ graphQLErrors }) => {
            Notification({
              type: 'error',
              message: ' エラー',
              description: graphQLErrors[0]?.message
            })
          })
    })
  }

  React.useEffect(() => {
    refetch()
    setSelectedRows([])
    setDisableDelete(true)
    setDisableEdit(true)
  }, [isFinished, refetch])
  return (
    <>
      <Header>
        <TitleCol lg={6} md={4}>
          {/* <Heading>アニメーション動画管理</Heading> */}
          <ActionWrapper>
            {/* <video src='https://vermuda-images.s3-ap-northeast-1.amazonaws.com/videos/1636337970843_gfkq1hz2pnag/SLOTMACHINE-no%20(1).mp4' /> */}
            <AddIcon
              color={'#00C58D'}
              title={'追加する'}
              onClick={() => openAddVideorForm('add')}
            />
            <EditIcon
              disabled={disableEdit}
              color={disableEdit && '#ccc'}
              title={'編集する'}
              onClick={
                !disableEdit ? () => openEditVideoForm(selectedRows) : undefined
              }
            />
            <DeleteIcon
              disabled={disableDelete}
              color={disableDelete ? '#ccc' : '#f00'}
              onClick={!disableDelete ? () => deleteHandler() : undefined}
              title={'動画削除'}
            />
          </ActionWrapper>
        </TitleCol>
        <ActionCol lg={6} md={8}>
          <SearchWrapper>
            <SeachText
              setText={setSearchText}
              pageIndex={pageIndex}
              setPageIndex={setPageIndex}
            />
          </SearchWrapper>
        </ActionCol>
      </Header>
      <TableWrapper>
        <CustomTable
          bordered
          loading={loading}
          rowSelection={{
            type: 'checkbox',
            ...rowSelection
          }}
          columns={columns}
          dataSource={videosData}
          // bordered
          pagination={{
            total: total,
            pageSize: pageSize,
            current: pageIndex + 1,
            onChange: (value) => {
              refetch({ offset: pageSize * value - 1, limit: pageSize }).then(
                () => {
                  setPageIndex(value - 1)
                }
              )
            },
            position: ['bottomCenter']
          }}
        />{' '}
      </TableWrapper>
    </>
  )
}
