import { styled } from 'baseui'
import { NavLink as NavLinks } from 'react-router-dom'

export const SidebarWrapper = styled('div', ({ $theme }) => ({
  width: '270px',
  height: '100%',
  display: 'flex',
  flexShrink: '0',
  backgroundColor: '#FFF',
  flexDirection: 'column',
  alignContent: 'space-between',

  '@media only screen and (max-width: 1280px)': {
    width: '230px',
    padding: '0'
  }
}))

export const MenuWrapper = styled('div', ({ $theme }) => ({
  width: '100%',
  height: '90%',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'flex-end',
  padding: '30px 0',
  overflowY: 'auto',

  '@media only screen and (max-width: 1024px)': {
    alignContent: 'center',
    padding: '30px 0 20px 0'
  }
}))

export const NavLink = styled(NavLinks, ({ $theme }) => ({
  ...$theme.typography.fontBold18,
  width: 'calc(100% - 25px)',
  outline: '0',
  color: $theme.colors.textDark,
  display: 'flex',
  alignItems: 'center',
  padding: '30px',
  textDecoration: 'none',
  transition: '0.15s ease-in-out',

  '@media only screen and (max-width: 1440px)': {
    width: 'calc(100% - 30px)',
    padding: '30px 20px'
  },

  '@media only screen and (max-width: 1280px)': {
    width: 'calc(100% - 20px)',
    padding: '30px 15px'
  },

  '@media only screen and (max-width: 1024px)': {
    padding: '30px 15px'
  },

  '&.active': {
    color: $theme.colors.primary,
    backgroundColor: $theme.colors.backgroundF7,
    borderRadius: '50px 0 0 50px'
  }
}))

export const Svg = styled('span', ({ $theme }) => ({
  width: '16px',
  marginRight: '15px',
  display: 'flex',
  alignItems: 'center'
}))

export const LogoutWrapper = styled('div', ({ $theme }) => ({
  width: '100%',
  height: '10%',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center'
}))

export const LogoutBtn = styled('button', ({ $theme }) => ({
  ...$theme.typography.fontBold18,
  color: $theme.colors.textDark,
  width: '100%',
  outline: '0',
  backgroundColor: 'transparent',
  border: '0',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  textDecoration: 'none',
  transition: '0.15s ease-in-out',
  cursor: 'pointer'
}))
