/* eslint-disable */
import React, { useContext, useState } from 'react'
import { withRouter } from 'react-router-dom'
import {
  SidebarWrapper,
  NavLink,
  MenuWrapper,
  Svg,
  LogoutBtn,
  LogoutWrapper
} from './Sidebar.style'
import {
  EVENT,
  // CATEGORY,
  // COUPONS,
  // DASHBOARD,
  // ORDERS,
  // DASHBOARD,
  // PRODUCTS,
  // CATEGORY,
  // ORDERS,
  // COUPONS,
  // CUSTOMERS,
  // SETTINGS
  // PRODUCTS,
  PRIZE_WINNERS,
  USERS,
  VIDEO,
  PDF_EXPORT,
  QUESTIONNAIRE,
  OVERVIEWTEMPLATE
} from '../../../settings/constants'
import { AuthContext } from '../../../context/auth'
import {
  SettingIcon,
  LogoutIcon,
  SidebarCategoryIcon,
  OrderIcon,
  CouponIcon,
  CustomerIcon,
  DashboardIcon
} from '../../../components/AllSvgIcon'
import { useSelectDispatch } from '../../../context/SelectContext'
import { ExclamationCircleOutlined, QrcodeOutlined,QuestionCircleOutlined, BuildOutlined } from '@ant-design/icons'
import { useDrawerDispatch } from '../../../context/DrawerContext'
import confirm from 'antd/lib/modal/confirm'
const sidebarMenus = [
  // {
  //   name: 'Dashboard',
  //   path: DASHBOARD,
  //   exact: true,
  //   icon: <DashboardIcon />
  // },
  // {
  //   name: 'Products',
  //   path: PRODUCTS,
  //   exact: false,
  //   icon: <ProductIcon />
  // },
  // {
  //   name: 'Category',
  //   path: CATEGORY,
  //   exact: false,
  //   icon: <SidebarCategoryIcon />
  // },
  // {
  //   name: 'Orders',
  //   path: ORDERS,
  //   exact: false,
  //   icon: <OrderIcon />
  // },
  // {
  //   name: 'Coupons',
  //   path: COUPONS,
  //   exact: false,
  //   icon: <CouponIcon />
  // },
  // {
  //   name: 'Customers',
  //   path: CUSTOMERS,
  //   exact: false,
  //   icon: <CustomerIcon />
  // },
  {
    name: 'アカウント管理',
    path: USERS,
    exact: false,
    icon: <CustomerIcon />
  },
  // {
  //   name: '当選者管理',
  //   path: PRIZE_WINNERS,
  //   exact: false,
  //   icon: <SettingIcon />
  // },
  {
    name: 'イベント管理',
    path: EVENT,
    exact: false,
    icon: <CouponIcon />
  },
  {
    name: '動画管理',
    path: VIDEO,
    exact: false,
    icon: <SettingIcon />
  },
  {
    name: 'アンケート管理',
    path: QUESTIONNAIRE,
    exact: false,
    icon: <QuestionCircleOutlined />
  },
  {
    name: '概要テンプレート',
    path: OVERVIEWTEMPLATE,
    exact: false,
    icon: <BuildOutlined />
  },
  {
    name: '当選者一覧',
    path: PRIZE_WINNERS,
    exact: false,
    icon: <DashboardIcon />
  }
]
export default withRouter(function Sidebar({
  refs,
  style,
  onMenuItemClick
}: any) {
  const { signout } = useContext(AuthContext)
  const selectDispatch = useSelectDispatch()
  const drawer = useDrawerDispatch()
  const SideBar =
    localStorage.role !== 'client'
      ? sidebarMenus
      : sidebarMenus.filter((item) => item.path !== USERS)


  return (
    <SidebarWrapper ref={refs} style={style}>
      <MenuWrapper>
        {SideBar.map((menu: any, index: number) => (
          <NavLink
            to={menu.path}
            key={index}
            exact={menu.exact}
            activeStyle={{
              color: '#00C58D',
              backgroundColor: '#f7f7f7',
              borderRadius: '50px 0 0 50px'
            }}
            onClick={() => {
              // dont hide menu when click on navlink:
              // selectDispatch({ type: 'OPEN_SELECT' })
              //hide menu when click on navlink:
              selectDispatch({ type: 'CLOSE_SELECT' })
            }}
          >
            {menu.icon ? <Svg>{menu.icon}</Svg> : ''}
            {menu.name}
          </NavLink>
        ))}
      </MenuWrapper>
      <LogoutWrapper>
        <LogoutBtn
          onClick={() => {
            confirm({
              type: 'warning',
              title: '確認',
              content: 'ログアウトしてもよろしいですか？',
              icon: <ExclamationCircleOutlined />,
              cancelText: 'キャンセル',
              okText: 'OK',
              onOk: () => signout()
            })
          }}
        >
          <Svg>
            <LogoutIcon />
          </Svg>
          ログアウト
        </LogoutBtn>
      </LogoutWrapper>
    </SidebarWrapper>
  )
})
