/* eslint-disable */
import React, { useCallback, useState } from 'react'
import { Link } from 'react-router-dom'
import Button from '../../../components/Button/Button'
import Popover, { PLACEMENT } from '../../../components/Popover/Popover'
import { Notification } from '../../../components/Notification/NotificationCustom'
import { AuthContext } from '../../../context/auth'
import { STAFF_MEMBERS, SETTINGS } from '../../../settings/constants'
import {
  ArrowLeftRound,
  LogoutIcon,
  MenuIcon
} from '../../../components/AllSvgIcon'
import {
  TopbarWrapper,
  Logo,
  TopbarRightSide,
  ProfileImg,
  Image,
  UserDropdowItem,
  NavLink,
  LogoutBtn,
  DrawerIcon,
  CloseButton,
  DrawerWrapper,
  LogoHeaderText,
  PrimaryHeaderText,
  Heading
} from './Topbar.style'
import UserImage from '../../../assets/images/icon_user.png'
import Drawer, { ANCHOR } from '../../../components/Drawer/Drawer'
import Sidebar from '../Sidebar/Sidebar'
import {
  useSelectDispatch,
  useSelectState
} from '../../../context/SelectContext'
import { Modal } from 'antd'
import { ExclamationCircleOutlined } from '@ant-design/icons'
import { Svg } from '../Sidebar/Sidebar.style'
const { confirm } = Modal
const Topbar = ({ refs }: any) => {
  const dispatch = useSelectDispatch()
  const isOpen = useSelectState('isOpen')
  const { signout } = React.useContext(AuthContext)
  const [isDrawerOpen, setIsDrawerOpen] = useState(false)
  const openDrawer = useCallback(() => dispatch({ type: 'OPEN_SELECT' }), [
    dispatch
  ])
  const closeDrawer = useCallback(() => dispatch({ type: 'CLOSE_SELECT' }), [
    dispatch
  ])
  const displayName =
    typeof window !== undefined && localStorage.getItem('display_name')

  const userRole = typeof window !== undefined && localStorage.getItem('role')
  return (
    <TopbarWrapper ref={refs}>
      <DrawerWrapper>
        <DrawerIcon onClick={openDrawer}>
          <MenuIcon />
        </DrawerIcon>
        <Drawer
          isOpen={isOpen}
          anchor={ANCHOR.left}
          onClose={closeDrawer}
          overrides={{
            Root: {
              style: {
                zIndex: '1'
              }
            },
            DrawerBody: {
              style: {
                marginRight: '0',
                marginLeft: '0',
                '@media only screen and (max-width: 767px)': {
                  marginLeft: '30px'
                }
              }
            },
            DrawerContainer: {
              style: {
                width: '270px',
                '@media only screen and (max-width: 767px)': {
                  width: '80%'
                }
              }
            },
            Close: {
              component: () => (
                <CloseButton onClick={closeDrawer}>
                  <ArrowLeftRound />
                </CloseButton>
              )
            }
          }}
        >
          <Sidebar />
        </Drawer>
      </DrawerWrapper>
      <Logo>
        {/* <Link to='/' style={{ textDecoration: 'none' }}> */}
        {/* <LogoImage src={Logoimage} alt="pickbazar-admin" /> */}
        <LogoHeaderText>
        コズレ<PrimaryHeaderText>抽選管理者ページ</PrimaryHeaderText>
        </LogoHeaderText>
        {/* </Link> */}
      </Logo>

      <TopbarRightSide>
        {/* <Popover
          content={({ close }) => <Notification data={data} onClear={close} />}
          accessibilityType={'tooltip'}
          placement={PLACEMENT.bottomRight}
          overrides={{
            Body: {
              style: {
                width: '330px',
                zIndex: 2
              }
            },
            Inner: {
              style: {
                backgroundColor: '#ffffff'
              }
            }
          }}
        >
          <NotificationIconWrapper>
            <NotificationIcon />
            <AlertDot>
              <AlertDotIcon />
            </AlertDot>
          </NotificationIconWrapper>
        </Popover> */}

        {userRole !== 'admin' && (
          <Popover
            content={({ close }) => (
              <UserDropdowItem>
                <LogoutBtn
                  onClick={() => {
                    confirm({
                      type: 'warning',
                      title: '確認',
                      content: 'ログアウトしてもよろしいですか？',
                      icon: <ExclamationCircleOutlined />,
                      cancelText: 'キャンセル',
                      okText: 'OK',
                      onOk: () => signout()
                    })
                  }}
                >
                  <Svg>
                    <LogoutIcon />
                  </Svg>
                  ログアウト
                </LogoutBtn>
              </UserDropdowItem>
            )}
            accessibilityType={'tooltip'}
            placement={PLACEMENT.bottomRight}
            overrides={{
              Body: {
                style: () => ({
                  width: '220px',
                  zIndex: 2
                })
              },
              Inner: {
                style: {
                  backgroundColor: '#ffffff'
                }
              }
            }}
          >
            <ProfileImg>
              <Heading>{displayName}</Heading>
              <Image src={UserImage} />
            </ProfileImg>
          </Popover>
        )}
      </TopbarRightSide>
    </TopbarWrapper>
  )
}

export default Topbar
