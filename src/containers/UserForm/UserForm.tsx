/* eslint-disable */
import React, { useCallback, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useLazyQuery, useMutation, useQuery } from '@apollo/react-hooks'
import { Scrollbars } from 'react-custom-scrollbars'
// import { AuthContext, Role } from '../../context/auth'
import { useDrawerDispatch, useDrawerState } from '../../context/DrawerContext'
import Input from '../../components/Input/Input'
import Button, { KIND } from '../../components/Button/Button'
import DrawerBox from '../../components/DrawerBox/DrawerBox'
import { Row, Col } from '../../components/FlexBox/FlexBox'
import {
  Form,
  DrawerTitleWrapper,
  DrawerTitle,
  FieldDetails,
  ButtonGroup,
  RequiredCol
} from '../DrawerItems/DrawerItems.style'
import {
  FormFields,
  FormLabel,
  Error
} from '../../components/FormFields/FormFields'
import { MUTATION_CREATE_USER } from '../../graphql/mutation/create-user'
import { MUTATION_UPDATE_USER } from '../../graphql/mutation/update-user'
import Select from '../../components/Select/Select'
import { OverideCancel, OverideSave, OverideSelect } from './Overrides'
import { Notification } from '../../components/Notification/NotificationCustom'
import { User, UserFormInput, UserType } from './Interface'
import { Value } from 'baseui/select'
import { QUERY_AGENCYS } from '../../graphql/query/users'

type Props = any
// const roleOption = [
//   { id: '1', role: 'admin', name: 'ADMIN' },
//   { id: '2', role: 'agency', name: '代理店' },
//   { id: '3', role: 'client', name: '企業' }
// ]
const roleAdmin = [
  { id: '2', role: 'agency', name: '代理店' },
  { id: '3', role: 'client', name: '企業' }
]
const roleAgency = [{ id: '3', role: 'client', name: '企業' }]

const UserForm: React.FC<Props> = (props) => {
  const dispatch = useDrawerDispatch()
  const isCreate = useDrawerState('actionType') === 'create'
  const isEdit = useDrawerState('actionType') === 'edit'
  const {
    id,
    pwd,
    nameKanji,
    companyName,
    userType,
    tel,
    email,
    address,
    agency,
    role,
    maxClient
  } = useDrawerState('data') ?? []

  const userTypeRef = React.useRef(null)
  const agencyRef = React.useRef(null)
  const [userTypeSelect, setUserTypeSelect] = React.useState<UserType>(userType)
  const [agencySelected, setAgencySelected] = React.useState<Value>()
  const [searchText, setSearchText] = React.useState<any>('')
  const [agencyData, setAgencyData] = React.useState<any>('')
  const [visibleAgency, setVisibleAgency] = React.useState(false)
  const [isCharacter, setIsCharacter] = useState(false)
  const [textMaxClient, setTextMaxClient] = useState('')

  const { register, handleSubmit, setValue, errors } = useForm<UserFormInput>({
    defaultValues: {
      usertype: isEdit
        ? userType.id
        : isCreate && localStorage.role === 'agency'
        ? roleAgency[0].id
        : '',
      agency: null,
      maxClient: -1
    }
  })

  const validateMaxClient = () => {
    if (textMaxClient.match(/^[-]?[0-9]+$/) || textMaxClient.length === 0) {
      return false
    }
    return true
  }

  const closeDrawer = useCallback(() => {
    dispatch({ type: 'CLOSE_DRAWER' })
    dispatch({ type: 'SUCCESS' })
  }, [dispatch])
  const [queryAgencys] = useLazyQuery(QUERY_AGENCYS, {
    fetchPolicy: 'network-only',
    onCompleted: (data) => {
      let agencys = data?.agencys?.users.map((item) => ({
        ...item,
        agencyName: `${item.id} (${item.nameKanji})`
      }))
      setAgencyData(agencys)
    }
  })

  const [createUser] = useMutation(MUTATION_CREATE_USER, {
    fetchPolicy: 'no-cache'
  })
  const [updateUser] = useMutation(MUTATION_UPDATE_USER, {
    fetchPolicy: 'no-cache'
  })

  const isShowMaxClient = (select) => {
    console.log('user select type:', select, typeof select)
    if (!select) return false
    else {
      if (select.id && select.id !== '2') return false
      else if (select[0].id !== '2') return false
      return true
    }
  }

  const checkValidate = (select) => {
    if (!select) return false
    else {
      if (Array.isArray(select)) {
        if (select[0].id !== '2') return false
        return true
      } else {
        if (select.id && select.id !== '2') return false
        return true
      }
    }
  }

  const onSubmit = (data: UserFormInput) => {
    // console.log(data)
    if (!userTypeSelect) {
      return
    }
    if (checkValidate(userTypeSelect)) {
      if (
        (!Number(data.maxClient) && Number(data.maxClient) !== 0) ||
        data.maxClient.toString().indexOf('.') !== -1 ||
        data.maxClient.toString().indexOf(' ') !== -1
      ) {
        return
      }
    }
    let values = {
      id: data.userid,
      pwd: data.password,
      nameKanji: data.namekanji,
      companyName: data.company,
      tel: data.contact_number,
      email: data.email,
      address: data.address,
      userTypeId: data.usertype,
      agencyId: data.agency ?? localStorage.getItem('userId'),
      maxClient: Number(data.maxClient) >= 0 ? Number(data.maxClient) : -1
    }
    if (isCreate) {
      createUser({
        variables: {
          createUserInput: values
        }
      })
        .then(() => {
          Notification({
            type: 'success',
            message: '成功',
            description: 'ユーザー作成に成功しました。'
          })
          closeDrawer()
        })
        .catch(({ graphQLErrors }) =>
          Notification({
            type: 'error',
            message: ' エラー',
            description: graphQLErrors[0]?.message
          })
        )
    } else if (isEdit) {
      console.log(values)

      updateUser({
        variables: {
          updateUserInput: values
        }
      })
        .then(() => {
          Notification({
            type: 'success',
            message: '成功',
            description: 'ユーザー更新に成功しました。'
          })
          closeDrawer()
        })
        .catch(({ graphQLErrors }) => {
          Notification({
            type: 'error',
            message: ' エラー',
            description: graphQLErrors[0]?.message
          })
        })
    }
  }

  React.useEffect(() => {
    setValue('userid', id)
    setValue('password', pwd)
    setValue('namekanji', nameKanji)
    setValue('company', companyName)
    setValue('contact_number', tel)
    setValue('email', email)
    setValue('address', address)
    setValue('usertype', isEdit && userType?.id)
    setValue('agency', agency?.id)
    setValue('maxClient', maxClient >= 0 ? maxClient : -1)
  }, [isEdit])

  React.useEffect(() => {
    if (isCreate && localStorage.role === 'agency') {
      setUserTypeSelect(roleAgency[0])
      setValue('usertype', roleAgency[0].id)
    }
  }, [isCreate])
  React.useEffect(() => {
    register(
      { name: 'userid' },
      { required: true, minLength: 4, maxLength: 10 }
    )
    register(
      { name: 'password' },
      { required: true, minLength: 4, maxLength: 20 }
    )
    register({ name: 'namekanji' }, { required: true, maxLength: 100 })
    register({ name: 'company' }, { maxLength: 500 })
    register({ name: 'contact_number' }, { maxLength: 20 })
    register({ name: 'email' }, { maxLength: 100 })
    register({ name: 'address' }, { maxLength: 500 })
    register({ name: 'usertype' }, { required: true })
    register({ name: 'agency' })
  }, [register])

  React.useEffect(() => {
    if (localStorage.role && localStorage.role === 'admin') {
      queryAgencys({
        variables: {
          searchText,
          skip: 0,
          take: 0,
          userType: '2'
        }
      })
      setAgencySelected([
        {
          ...agency,
          agencyName: `${agency?.id}(${agency?.nameKanji})`
        }
      ])
    }
  }, [])
  React.useEffect(() => {
    if (userType?.role === 'client') {
      setVisibleAgency(true)
    }
  }, [userType])

  return (
    <>
      <DrawerTitleWrapper>
        <DrawerTitle>{isCreate ? 'ユーザー作成' : 'ユーザー編集'}</DrawerTitle>
      </DrawerTitleWrapper>

      <Form onSubmit={handleSubmit(onSubmit)} style={{ height: '100%' }}>
        <Scrollbars
          autoHide
          renderView={(props) => (
            <div {...props} style={{ ...props.style, overflowX: 'hidden' }} />
          )}
          renderTrackHorizontal={(props) => (
            <div
              {...props}
              style={{ display: 'none' }}
              className='track-horizontal'
            />
          )}
        >
          <Row>
            <RequiredCol lg={3}>
              <FieldDetails>
                必要な情報を記入して下さい。
                <br />
                <span style={{ color: 'red' }}>(*)は必須の項目です。</span>
              </FieldDetails>
            </RequiredCol>

            <Col lg={9}>
              <DrawerBox>
                <FormFields>
                  <FormLabel>
                    ユーザーID&nbsp;<span style={{ color: 'red' }}>*</span>
                  </FormLabel>
                  <Input
                    name='userid'
                    disabled={isEdit}
                    inputRef={register({
                      required: true,
                      maxLength: 10,
                      minLength: 4
                    })}
                  />
                  {errors.userid && errors.userid.type === 'required' && (
                    <Error>(*)は必須の項目です。</Error>
                  )}
                  {errors.userid && errors.userid.type === 'minLength' && (
                    <Error>ユーザーIDは4文字以上設定してください。</Error>
                  )}
                  {errors.userid && errors.userid.type === 'maxLength' && (
                    <Error>ユーザーIDは10文字以下に設定してください。</Error>
                  )}
                </FormFields>
                <FormFields>
                  <FormLabel>
                    パスワード&nbsp;<span style={{ color: 'red' }}>*</span>
                  </FormLabel>
                  <Input
                    name='password'
                    type='password'
                    inputRef={register({
                      required: true,
                      maxLength: 20,
                      minLength: 4
                    })}
                  />
                  {errors.password && errors.password.type === 'required' && (
                    <Error>(*)は必須の項目です。</Error>
                  )}
                  {errors.password && errors.password.type === 'maxLength' && (
                    <Error>パスワードは20文字以下設定してください。</Error>
                  )}
                  {errors.password && errors.password.type === 'minLength' && (
                    <Error>パスワードは4文字以上設定してください。</Error>
                  )}
                </FormFields>
                <FormFields>
                  <FormLabel>
                    氏名&nbsp;<span style={{ color: 'red' }}>*</span>
                  </FormLabel>
                  <Input
                    inputRef={register({ required: true, maxLength: 100 })}
                    name='namekanji'
                  />
                  {errors.namekanji && errors.namekanji.type === 'required' && (
                    <Error>(*)は必須の項目です。</Error>
                  )}
                  {errors.namekanji &&
                    errors.namekanji.type === 'maxLength' && (
                      <Error>
                        氏名は100文字以下設定してください。
                      </Error>
                    )}
                </FormFields>
                <FormFields>
                  <FormLabel>
                    種別&nbsp;<span style={{ color: 'red' }}>*</span>
                  </FormLabel>
                  <Select
                    ref={register({ required: true })}
                    name='usertype'
                    labelKey='name'
                    valueKey='id'
                    controlRef={userTypeRef}
                    options={
                      localStorage.role === 'admin' ? roleAdmin : roleAgency
                    }
                    clearable={false}
                    searchable={false}
                    value={userTypeSelect}
                    disabled={
                      localStorage.role !== 'admin' ||
                      role === '企業' ||
                      role === '代理店'
                    }
                    placeholder={
                      isCreate && localStorage.role !== 'agency'
                        ? 'ユーザー種別を選択してください'
                        : ''
                    }
                    onChange={({ value }) => {
                      // console.log(value)
                      setValue('usertype', value[0]?.id)
                      setUserTypeSelect(value)
                      setVisibleAgency(value[0]?.role === 'client')
                      setAgencySelected([])
                    }}
                    overrides={OverideSelect}
                  />
                  {!userTypeSelect &&
                    errors.usertype &&
                    errors.usertype.type === 'required' && (
                      <Error>(*)は必須の項目です。</Error>
                    )}
                </FormFields>
                {((isCreate && isShowMaxClient(userTypeSelect)) ||
                  (isEdit && userType.id !== '3')) &&
                  localStorage.role === 'admin' && (
                    <FormFields>
                      <FormLabel>
                        企業作成上限&nbsp;
                        <span style={{ color: 'red' }}>*</span>
                      </FormLabel>
                      <Input
                        name='maxClient'
                        // disabled={ isCreate && checkUserTypeSelect(userTypeSelect)}
                        onChange={(e) => setTextMaxClient(e.target.value)}
                        onKeyDown={(e) => {
                          if (!/^[0-9]/.test(e.key)) {
                            setIsCharacter(true)
                          }
                        }}
                        inputRef={register({ required: true, minLength: 1 })}
                      />
                      {isCharacter && validateMaxClient() && (
                        <Error>番号を入力してください。</Error>
                      )}
                      {errors.maxClient &&
                        errors.maxClient.type === 'required' && (
                          <Error>(*)は必須の項目です。</Error>
                        )}
                    </FormFields>
                  )}
                {localStorage.role === 'admin' && visibleAgency && (
                  <FormFields>
                    <FormLabel>代理店</FormLabel>
                    <Select
                      name='agency'
                      labelKey='agencyName'
                      valueKey='id'
                      controlRef={agencyRef}
                      options={agencyData}
                      clearable={true}
                      value={agencySelected[0] ? agencySelected : null}
                      placeholder={isCreate ? '代理店を選択してください。' : ''}
                      onInputChange={(event: any) =>
                        setSearchText(event.target.value)
                      }
                      onChange={({ value }) => {
                        console.log(value)
                        setValue('agency', value[0]?.id)
                        setAgencySelected(value)
                      }}
                      overrides={OverideSelect}
                    />
                  </FormFields>
                )}
                <FormFields>
                  <FormLabel>会社名</FormLabel>
                  <Input
                    inputRef={register({ maxLength: 500 })}
                    name='company'
                  />
                  {errors.company && errors.company.type === 'maxLength' && (
                    <Error>会社名は500文字以下設定してください。</Error>
                  )}
                </FormFields>
                <FormFields>
                  <FormLabel>電話番号</FormLabel>
                  <Input
                    name='contact_number'
                    inputRef={register({ maxLength: 20 })}
                  />
                  {errors.contact_number &&
                    errors.contact_number.type === 'maxLength' && (
                      <Error>電話番号は20文字以下設定してください。</Error>
                    )}
                </FormFields>

                <FormFields>
                  <FormLabel>メールアドレス</FormLabel>
                  <Input
                    name='email'
                    inputRef={register({
                      pattern:
                        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                      maxLength: 100
                    })}
                  />
                  {errors.email && errors.email.type === 'pattern' && (
                    <Error>メールアドレスの形式は正しくありません。</Error>
                  )}
                  {errors.email && errors.email.type === 'maxLength' && (
                    <Error>メールアドレスは100文字以下設定してください。</Error>
                  )}
                </FormFields>
                <FormFields>
                  <FormLabel>住所</FormLabel>
                  <Input
                    inputRef={register({ maxLength: 500 })}
                    name='address'
                  />
                  {errors.address && errors.address.type === 'maxLength' && (
                    <Error>住所は500文字以下設定してください。</Error>
                  )}
                </FormFields>
              </DrawerBox>
            </Col>
          </Row>
        </Scrollbars>

        <ButtonGroup>
          <Button
            kind={KIND.minimal}
            onClick={closeDrawer}
            overrides={OverideCancel}
          >
            キャンセル
          </Button>
          <Button type='submit' overrides={OverideSave}>
            {isCreate ? '追加' : '更新する'}
          </Button>
        </ButtonGroup>
      </Form>
    </>
  )
}
export default UserForm
