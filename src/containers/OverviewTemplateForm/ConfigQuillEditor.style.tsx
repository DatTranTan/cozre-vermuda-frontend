import styled from "styled-components";

export const QuillEditorWrapper = styled.div`
  .ql-container.ql-snow {
    height: 65vh;
    overflow-y: auto;
    @media (min-width: 2000px) {
      height: 75vh;
    }
    @media (min-width: 1800px) {
      height: 60vh;
    }
    @media (min-width: 1600px) {
      height: 50vh;
    }
    @media (max-width: 1500px) {
      height: 45vh;
    }
    @media (max-width: 1400px) {
      height: 40vh;
    }
    @media (max-width: 985px) {
      height: 50vh;
    }
    @media (max-width: 600px) {
      height: 25vh;
    }
    @media (max-width: 450px) {
      height: 35vh;
    }
  }
`;
