import React, { useState, useCallback } from "react";
import { useForm } from "react-hook-form";
import { v4 as uuidv4 } from "uuid";
import gql from "graphql-tag";
import { useMutation } from "@apollo/react-hooks";
import { useDrawerDispatch, useDrawerState } from "../../context/DrawerContext";
import { Scrollbars } from "react-custom-scrollbars";
import Input from "../../components/Input/Input";
import Select from "../../components/Select/Select";
import Button, { KIND } from "../../components/Button/Button";
import DrawerBox from "../../components/DrawerBox/DrawerBox";
import { Row, Col } from "../../components/FlexBox/FlexBox";
import {
  Form,
  DrawerTitleWrapper,
  DrawerTitle,
  FieldDetails,
  ButtonGroup,
  RequiredCol,
} from "../DrawerItems/DrawerItems.style";
import {
  FormFields,
  FormLabel,
  Error,
} from "../../components/FormFields/FormFields";
import { QuillEditorWrapper } from "./ConfigQuillEditor.style";
import { CustomQuill } from "../../components/ToolbarQuill/CustomQuill";
import QuillToolbar, {
  modules,
  formats,
} from "../../components/ToolbarQuill/ToolbarQuill";
import ReactQuill from "react-quill";
import { MUTATION_CREATE_CAMP } from "../../graphql/mutation/create-camp";
import { MUTATION_UPDATE_CAMP } from "../../graphql/mutation/create-camp";
import { Notification } from "../../components/Notification/NotificationCustom";

const GET_COUPONS = gql`
  query getCoupons($status: String, $searchBy: String) {
    coupons(status: $status, searchBy: $searchBy) {
      id
      title
      code
      number_of_used_coupon
      number_of_coupon
      expiration_date
      creation_date
      status
    }
  }
`;
const CREATE_COUPON = gql`
  mutation createCoupon($coupon: AddCouponInput!) {
    createCoupon(coupon: $coupon) {
      id
      title
      code
      number_of_used_coupon
      number_of_coupon
      expiration_date
      creation_date
      status
    }
  }
`;

const options = [
  { value: "grocery", name: "Grocery", id: "1" },
  { value: "women-cloths", name: "Women Cloths", id: "2" },
  { value: "bags", name: "Bags", id: "3" },
  { value: "makeup", name: "Makeup", id: "4" },
];
type Props = any;

const OverviewTemplateForm: React.FC<Props> = (props) => {
  const dispatch = useDrawerDispatch();
  const closeDrawer = useCallback(() => {
    dispatch({ type: "CLOSE_DRAWER" });
    dispatch({ type: "SUCCESS" });
  }, [dispatch]);
  const { register, handleSubmit, setValue, errors } = useForm();
  const [category, setCategory] = useState([]);
  const [dataEditor, setDataEditor] = useState<any>(null);
  const actionType = useDrawerState("actionType");
  const fillData = useDrawerState("data");
  React.useEffect(() => {
    register({ name: "category" });
  }, [register]);
  const [createCamp, { loading: loadingCreateCampain }] = useMutation(
    MUTATION_CREATE_CAMP,
    {
      fetchPolicy: "no-cache",
    }
  );
  const [updateCamp, { loading: loadingUpdateCampain }] = useMutation(
    MUTATION_UPDATE_CAMP,
    {
      fetchPolicy: "no-cache",
    }
  );
  const [createCoupon] = useMutation(CREATE_COUPON, {
    update(cache, { data: { createCoupon } }) {
      const { coupons } = cache.readQuery({
        query: GET_COUPONS,
      });

      cache.writeQuery({
        query: GET_COUPONS,
        data: { coupons: coupons.concat([createCoupon]) },
      });
    },
  });

  const onSubmit = (data) => {
    if (actionType === "create") {
      createCamp({
        variables: {
          createCampaignInput: {
            name: data.name,
            content: dataEditor,
          },
        },
      })
        .then((res) => {
          Notification({
            type: "success",
            message: "成功",
            description: " 概要テンプレート作成に成功しました。",
          });
          closeDrawer();
        })
        .catch(({ graphQLErrors }) => {
          Notification({
            type: "error",
            message: " エラー",
            description: graphQLErrors[0]?.message,
          });
        });
    }
    if (actionType === "edit") {
      updateCamp({
        variables: {
          updateCampaignInput: {
            id: fillData?.id,
            name: data?.name,
            content: dataEditor,
          },
        },
      })
        .then((res) => {
          Notification({
            type: "success",
            message: "成功",
            description: "概要テンプレート更新に成功しました。",
          });
          closeDrawer();
        })
        .catch(({ graphQLErrors }) => {
          Notification({
            type: "error",
            message: " エラー",
            description: graphQLErrors[0]?.message,
          });
        });
    }
  };
  const handleCategoryChange = ({ value }) => {
    setValue("category", value);
    setCategory(value);
  };
  const ChangeContent = (e: any) => {
    setDataEditor(e);
  };
  React.useEffect(() => {
    if (actionType === "edit") {
      setValue("id", fillData?.id);
      setValue("name", fillData?.name);
      setDataEditor(fillData?.content);
    }
  }, [actionType]);
  React.useEffect(() => {
    register({ name: "name" }, { required: true, maxLength: 100 });
  }, [register]);
  return (
    <>
      <DrawerTitleWrapper>
        <DrawerTitle>
          {actionType === "create"
            ? "概要テンプレート作成"
            : "概要テンプレート編集"}
        </DrawerTitle>
      </DrawerTitleWrapper>

      <Form onSubmit={handleSubmit(onSubmit)} style={{ height: "100%" }}>
        <Scrollbars
          autoHide
          renderView={(props) => (
            <div {...props} style={{ ...props.style, overflowX: "hidden" }} />
          )}
          renderTrackHorizontal={(props) => (
            <div
              {...props}
              style={{ display: "none" }}
              className="track-horizontal"
            />
          )}
        >
          <Row>
            <RequiredCol lg={3}>
              <FieldDetails>
                必要な情報を記入して下さい。
                <br />
                <span style={{ color: "red" }}>(*)は必須の項目です。</span>
              </FieldDetails>
            </RequiredCol>

            <Col lg={9}>
              <DrawerBox>
                <FormFields>
                  <FormLabel>
                    概要テンプレート名&nbsp;
                    <span style={{ color: "red" }}>*</span>
                  </FormLabel>
                  <Input
                    inputRef={register({ required: true, maxLength: 100 })}
                    name="name"
                  />
                  {errors.name && errors.name.type === "required" && (
                    <Error>(*)は必須の項目です。</Error>
                  )}
                  {errors.name && errors.name.type === "maxLength" && (
                    <Error>
                      概要テンプレートは100文字以下設定してください。
                    </Error>
                  )}
                </FormFields>
                <FormFields>
                  <FormLabel>内容</FormLabel>
                  <QuillEditorWrapper>
                    <CustomQuill>
                      <QuillToolbar />
                      <ReactQuill
                        theme="snow"
                        modules={modules}
                        formats={formats}
                        value={dataEditor ? dataEditor : ""}
                        onChange={ChangeContent}
                      />
                    </CustomQuill>
                  </QuillEditorWrapper>
                </FormFields>
              </DrawerBox>
            </Col>
          </Row>
        </Scrollbars>

        <ButtonGroup>
          <Button
            kind={KIND.minimal}
            onClick={closeDrawer}
            overrides={{
              BaseButton: {
                style: ({ $theme }) => ({
                  width: "50%",
                  borderTopLeftRadius: "3px",
                  borderTopRightRadius: "3px",
                  borderBottomRightRadius: "3px",
                  borderBottomLeftRadius: "3px",
                  borderColor: "#f5f5f5",
                  marginRight: "15px",
                  backgroundColor: "#ddd",
                  color: $theme.colors.red400,
                }),
              },
            }}
          >
            キャンセル
          </Button>

          <Button
            type="submit"
            overrides={{
              BaseButton: {
                style: ({ $theme }) => ({
                  width: "50%",
                  borderTopLeftRadius: "3px",
                  borderTopRightRadius: "3px",
                  borderBottomRightRadius: "3px",
                  borderBottomLeftRadius: "3px",
                }),
              },
            }}
          >
            {actionType === "create" ? "追加" : "更新する"}
          </Button>
        </ButtonGroup>
      </Form>
    </>
  );
};

export default OverviewTemplateForm;
