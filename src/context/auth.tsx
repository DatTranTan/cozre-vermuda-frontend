import React from 'react'
import { useMutation } from '@apollo/react-hooks'
import { MUTATION_LOGIN } from '../graphql/mutation/login'
import { Notification } from '../components/Notification/NotificationCustom'
type AuthProps = {
  isAuthenticated: boolean
  role: string
  authenticate: Function
  signout: Function
}

export const AuthContext = React.createContext({} as AuthProps)
export const Role = {
  admin: 'admin',
  agency: 'agency',
  client: 'client',
  guest: 'guest'
}
const isValidToken = () => {
  const token = localStorage.getItem('access_token')
  // JWT decode & check token validity & expiration.
  if (token) return true
  return false
}
const checkRole = () => {
  const role = localStorage.getItem('role')
  // JWT decode & check token validity & expiration.
  if (role) return role
  return Role.guest
}
const AuthProvider = (props: any) => {
  const [loginMutation] = useMutation(MUTATION_LOGIN)
  const [isAuthenticated, makeAuthenticated] = React.useState(isValidToken())
  const [role, setRole] = React.useState(checkRole())
  function authenticate({ username, password }, cb) {
    loginMutation({
      variables: {
        input: {
          id: username,
          password: password
        }
      }
    })
      .then(
        async ({
          data: {
            login: {
              id: userId,
              accessToken,
              userType: { role, name },
              nameKanji
            }
          }
        }) => {
          makeAuthenticated(true)
          setRole(role)
          localStorage.setItem('access_token', `${accessToken}`)
          localStorage.setItem('role', `${role}`)
          localStorage.setItem('userId', `${userId}`)
          localStorage.setItem('display_name', `${nameKanji}`)
          setTimeout(cb(true), 100) // fake async
          Notification({
            type: 'success',
            message: '成功',
            description: 'ログインに成功しました。'
          })
        }
      )
      .catch(({ graphQLErrors }) => {
        setTimeout(cb(false), 100) // fake async
        Notification({
          type: 'error',
          message: ' エラー',
          description: graphQLErrors[0]?.message
        })
      })
  }
  function signout(cb) {
    makeAuthenticated(false)
    localStorage.removeItem('access_token')
    localStorage.removeItem('role')
    Notification({
      type: 'success',
      message: '成功',
      description: 'ログアウトに成功しました。'
    })
    setTimeout(cb, 100)
  }
  return (
    <AuthContext.Provider
      value={{
        isAuthenticated,
        role,
        authenticate,
        signout
      }}
    >
      <>{props.children}</>
    </AuthContext.Provider>
  )
}

export default AuthProvider
