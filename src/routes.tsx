import React, { useContext, lazy, Suspense } from "react";
import { Route, Switch, Redirect, useLocation } from "react-router-dom";
import * as qs from "query-string";
import {
  LOGIN,
  PRIZE_WINNERS,
  USERS,
  EVENT,
  VIDEO,
  PDF_EXPORT,
  FORMPRIZE,
  SUCCESSFORM,
  LANDING_PAGE,
  QUESTIONNAIRE,
  OVERVIEWTEMPLATE,
  LOTTERY,
} from "./settings/constants";
import AuthProvider, { AuthContext } from "./context/auth";
import { InLineLoader } from "./components/InlineLoader/InlineLoader";
import Videos from "./containers/Videos/Videos";

const Login = lazy(() => import("./containers/Login/Login"));
const Users = lazy(() => import("./containers/Users/Users"));
const Questionnaire = lazy(
  () => import("./containers/Questionnaire/Questionnaire")
);
const OverviewTemplate = lazy(
  () => import("./containers/OverviewTemplate/OverviewTemplate")
);
const AdminLayout = lazy(() => import("./containers/Layout/Layout"));
const PrizeWinners = lazy(
  () => import("./containers/PrizeWinners/PrizeWinners")
);

const Events = lazy(() => import("./containers/Events/Events"));

const EventForm = lazy(() => import("./containers/Events/EventForm"));
const ExportQRCode = lazy(() => import("./containers/ExportQRCode"));

const specialLayoutUrl =
  "./landing-new-template/containers/LandingPageSpecial/";
const LandingPageSpecial = lazy(() => import(`${specialLayoutUrl}LandingPage`));
const LandingLotterySpecial = lazy(() => import(`${specialLayoutUrl}Lottery`));
const FormSpecial = lazy(
  () => import(`${specialLayoutUrl}Lottery/VideoSpin/Form/Contact`)
);
const SuccessFormSpecial = lazy(
  () => import(`${specialLayoutUrl}Lottery/VideoSpin/Form/Thanks`)
);
/**
 *
 *  A wrapper for <Route> that redirects to the login
 * screen if you're not yet authenticated.
 *
 */

function PrivateRoute({ children, ...rest }) {
  const { isAuthenticated } = useContext(AuthContext);
  const checkClient = localStorage.role === "client";
  return (
    <Route
      {...rest}
      render={({ location }) =>
        !isAuthenticated ? (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location },
            }}
          />
        ) : checkClient && rest.path === USERS ? (
          <Redirect
            to={{
              pathname: "/event",
              state: { from: location },
            }}
          />
        ) : (
          children
        )
      }
    />
  );
}

const Routes = () => {
  const location = useLocation();
  const query = qs.parse(location.search);
  return (
    <AuthProvider>
      <Suspense fallback={<InLineLoader />}>
        <Switch>
          <PrivateRoute exact={true} path={PRIZE_WINNERS}>
            <AdminLayout>
              <Suspense fallback={<InLineLoader />}>
                <PrizeWinners />
              </Suspense>
            </AdminLayout>
          </PrivateRoute>
          <PrivateRoute path={USERS}>
            <AdminLayout>
              <Suspense fallback={<InLineLoader />}>
                <Users />
              </Suspense>
            </AdminLayout>
          </PrivateRoute>
          <PrivateRoute path={EVENT}>
            <AdminLayout>
              <Suspense fallback={<InLineLoader />}>
                {query.type ? <EventForm /> : <Events />}
              </Suspense>
            </AdminLayout>
          </PrivateRoute>
          <PrivateRoute path={QUESTIONNAIRE}>
            <AdminLayout>
              <Suspense fallback={<InLineLoader />}>
                <Questionnaire />
              </Suspense>
            </AdminLayout>
          </PrivateRoute>
          <PrivateRoute path={OVERVIEWTEMPLATE}>
            <AdminLayout>
              <Suspense fallback={<InLineLoader />}>
                <OverviewTemplate />
              </Suspense>
            </AdminLayout>
          </PrivateRoute>
          <Route path={FORMPRIZE}>
            <Suspense fallback={<InLineLoader />}>
              {/* <Form /> */}
              <FormSpecial />
            </Suspense>
          </Route>

          <Route path={SUCCESSFORM}>
            <Suspense fallback={<InLineLoader />}>
              {/* <SuccessForm /> */}
              <SuccessFormSpecial />
            </Suspense>
          </Route>
          <PrivateRoute path={PDF_EXPORT}>
            <Suspense fallback={<InLineLoader />}>
              <ExportQRCode />
            </Suspense>
          </PrivateRoute>
          <PrivateRoute path={VIDEO}>
            <AdminLayout>
              <Suspense fallback={<InLineLoader />}>
                <Videos />
              </Suspense>
            </AdminLayout>
          </PrivateRoute>

          <Route exact={true} path={LANDING_PAGE}>
            <Suspense fallback={<InLineLoader />}>
              <LandingPageSpecial />
            </Suspense>
          </Route>
          <Route exact={true} path={LOTTERY}>
            <Suspense fallback={<InLineLoader />}>
              <LandingLotterySpecial />
            </Suspense>
          </Route>

          <Route path={LOGIN}>
            <Login />
          </Route>
          <Redirect to={localStorage.role !== "client" ? USERS : EVENT} />
          {/* <Route component={NotFound} /> */}
        </Switch>
      </Suspense>
    </AuthProvider>
  );
};

export default Routes;
