import React, { useContext, lazy, Suspense } from "react";
import { Route, Switch, Redirect, useLocation } from "react-router-dom";
import * as qs from "query-string";
import {
  CONTACT,
  THANKS,
  LANDING_PAGE,
  LOTTERY,
} from "./settings/constants";
import { InLineLoader } from "./components/InlineLoader/InlineLoader";

// Special layout
const specialLayoutUrl = "./containers/LandingPageSpecial/";
const LandingPageSpecial = lazy(
  () => import(`${specialLayoutUrl}LandingPage`)
);
const LandingLotterySpecial = lazy(
  () => import(`${specialLayoutUrl}Lottery`)
);
const FormSpecial = lazy(
  () => import(`${specialLayoutUrl}Lottery/VideoSpin/Form/Contact`)
);
const SuccessFormSpecial = lazy(
  () => import(`${specialLayoutUrl}Lottery/VideoSpin/Form/Thanks`)
);
/**
 *
 *  A wrapper for <Route> that redirects to the login
 * screen if you're not yet authenticated.
 *
 */

const LandingRoutes = () => {
  const location = useLocation();
  const query = qs.parse(location.search);
  return (
      <Suspense fallback={<InLineLoader />}>
        <Switch>
          <Route path={CONTACT}>
            <Suspense fallback={<InLineLoader />}>
              <FormSpecial />
            </Suspense>
          </Route>
          <Route path={THANKS}>
            <Suspense fallback={<InLineLoader />}>
              <SuccessFormSpecial />
            </Suspense>
          </Route>
          <Route exact={true} path={LANDING_PAGE}>
            <Suspense fallback={<InLineLoader />}>
              <LandingPageSpecial />
            </Suspense>
          </Route>
          <Route exact={true} path={LOTTERY}>
            <Suspense fallback={<InLineLoader />}>
              <LandingLotterySpecial />
            </Suspense>
          </Route>
        </Switch>
      </Suspense>
  );
};

export default LandingRoutes;
