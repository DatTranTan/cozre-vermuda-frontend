import gql from "graphql-tag";

export const MUTATION_INCREASE_WINNER = gql`
  mutation ($spinPrizeId: String!) {
    increaseWinner(spinPrizeId: $spinPrizeId)
  }
`;
