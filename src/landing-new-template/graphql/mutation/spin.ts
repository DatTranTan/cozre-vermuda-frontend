import gql from "graphql-tag";

export const MUTATION_SPIN = gql`
  mutation spin($eventId: String!, $userId: String!) {
    spin(eventId: $eventId, userId: $userId) {
      id
      name
      rank
      rate
      quantity
      numberOfWinner
      message
      imageUrl
      createdAt
      SpinPrizeID
      event {
        name
        endTime
        timeOut
        spinLimit
        txtBtnWin
        txtBtnLost
        prizes {
          id
          imageUrl
          url
        }
        customerLost
      }
    }
  }
`;
