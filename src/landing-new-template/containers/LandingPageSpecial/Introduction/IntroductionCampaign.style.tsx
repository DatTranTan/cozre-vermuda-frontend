import styled from "styled-components";

export const QuillEditorWrapper = styled.div`
    .ql-toolbar.ql-snow {
        display: none !important;
    }
    .ql-container.ql-snow {
        border: none !important;
    }
`

export const Container = styled.div`
  /* width: 80%; */
  background-color: white;
  margin: auto;
  border-radius: 20px;
  /* padding:60px; */
  /* margin-top: 120px; */
  @media (max-width: 1100px) {
    /* margin-top: 60px; */
    /* width: 90%; */
  }
  @media (max-width: 500px) {
    width: 100%;
    border-radius: 0px;
    /* margin-top: 40px; */
    /* margin-bottom: 0px; */
    /* padding: 20px; */
  }
`