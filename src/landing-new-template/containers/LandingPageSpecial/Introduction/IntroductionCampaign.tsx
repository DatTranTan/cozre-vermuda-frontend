import React from "react";
import { QuillEditorWrapper, Container } from "./IntroductionCampaign.style";
import { CustomQuill } from "./ToolbarQuill/CustomQuill";
import QuillToolbar, {
  modules,
  formats
} from "./ToolbarQuill/ToolbarQuill";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";

const IntroductionCampaign = ({ data }) => {
  const [dataCamp, setDataCamp] = React.useState(null);
  React.useEffect(() => {
    if (data?.event?.campaignContent) {
      setDataCamp(data?.event?.campaignContent);
    }
  }, [data]);
  return (
    <Container>
      {/* <div dangerouslySetInnerHTML={{ __html: dataCamp }}/> */}
      <QuillEditorWrapper>
        <CustomQuill>
          <QuillToolbar />
          <ReactQuill
            theme="snow"
            readOnly={true}
            modules={modules}
            formats={formats}
            value={dataCamp ? dataCamp : ""}
          />
        </CustomQuill>
      </QuillEditorWrapper>
    </Container>
  );
};

export default IntroductionCampaign;
