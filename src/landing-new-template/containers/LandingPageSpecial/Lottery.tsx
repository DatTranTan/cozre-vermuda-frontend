/* eslint-disable */
import { Modal } from "antd";
import queryString from "query-string";
import React, { useEffect, useState } from "react";
import { useHistory, useLocation } from "react-router-dom";
import { QUERY_EVENT } from "../../graphql/query/event";
import { MUTATION_INCREASE_WINNER } from "../../graphql/mutation/increase-winner";
import { useMutation, useQuery } from "@apollo/react-hooks";
import "./style/css/reset.css";
import "./style/css/common.css";
import "./style/css/sp.css";
import "./style/css/tab.css";
import "./style/css/pc.css";
import ImgCozre from "./style/img/lottery/cozre-banner.png";
import ImgWhite from "./style/img/lottery/image_white.png";
import ImgPart from "./style/img/lottery/img_bg_lottery02.jpg";
import ImgLost2 from "./style/img/lottery/img_lottery_off.png";
import ImgHeader from "./style/img/lottery/txt_hd01.svg";
import ImgLost from "./style/img/lottery/txt_hd02.svg";
import ImgHeaderPart from "./style/img/lottery/txt_hd03.svg";
import { Spin } from "antd";
import { Notification } from "../../../components/Notification/NotificationCustom";
import { isDesktop } from "react-device-detect";
import $ from "jquery";
import { InLineLoader } from "../../components/InlineLoader/InlineLoader";

function Lottery() {
  const location = useLocation();
  const history = useHistory();
  const spinPrizeId = queryString.parse(location.search).spin;
  const eventId = queryString.parse(location.search).event;
  const [prize, setPrize] = React.useState(null);
  const [loadingButtonWin, setLoadingButtonWin] = React.useState(false);

  const { data: dataEvent, loading } = useQuery(QUERY_EVENT, {
    variables: {
      id: eventId,
    },
  });

  const [increaseWinner, { loading: loadIncreaseWinner }] = useMutation(
    MUTATION_INCREASE_WINNER,
    {
      fetchPolicy: "no-cache",
    }
  );

  useEffect(() => {
    if (dataEvent) {
      setPrize(
        dataEvent?.event?.prizes.filter(
          (item) => item.rank === Number(localStorage.getItem("prizeRank"))
        )
      );
    }
  }, [dataEvent]);

  //Reload on mobile
  useEffect(() => {
    window.onpageshow = function (event) {
      if (event.persisted) {
        window.location.reload();
      }
    };
  }, []);

  if (dataEvent?.event) {
    document.title = dataEvent?.event?.name;
  }

  const detectLink = () => {
    var url = prize?.[0]?.url;
    if (!url || url === "") {
      window.open(
        `${window.location.origin}/landing-page?eventId=${eventId}`,
        "_self"
      );
      return;
    }
    if (url?.includes("http")) {
      window.open(url, "_self");
      localStorage.removeItem("prize");
      return;
    } else {
      window.open("http://" + url, "_self");
      localStorage.removeItem("prize");
      return;
    }
  };

  const goToLink = () => {
    if (prize?.[0]?.url) {
      setLoadingButtonWin(true);
      increaseWinner({
        variables: {
          spinPrizeId: spinPrizeId,
        },
      })
        .then(({ data }) => {
          detectLink();
        })
        .catch(({ graphQLErrors }) => {
          setLoadingButtonWin(false);
          Notification({
            type: "error",
            message: " エラー",
            // description: "原因不明のエラーが起きました。",
            description: graphQLErrors[0].message,
          });
        });
    } else {
      Notification({
        type: "error",
        message: " エラー",
        description: "リンクが正しくありません。",
      });
    }
  };

  return (
    <div id="wrapper">
      {loading ? (
        <InLineLoader />
      ) : (
        <>
          {dataEvent?.event ? (
            <div id="contents">
              {!localStorage.getItem("prize") ? (
                <div className="lotteryScreen off">
                  <div className="inner">
                    <div className="bn">
                      <h2 className="hd03" style={{ margin: "0 50px" }}>
                        <span className="pre">残念！</span>
                      </h2>
                      <p style={{ padding: "0 20px" }}>
                        <img src={ImgCozre} alt="残念" />
                      </p>

                      <p className="another">
                        またの挑戦をお待ちしております！
                      </p>
                    </div>
                    <p className="link">
                      <a
                        href={`landing-page?eventId=${eventId}`}
                        className="btnLink"
                      >
                        {isDesktop ? (
                          "また挑戦してね！TOPページへ戻る"
                        ) : (
                          <div style={{ display: "grid" }}>
                            <span>また挑戦してね！</span>
                            <span>TOPページへ戻る</span>
                          </div>
                        )}
                      </a>
                    </p>
                  </div>
                </div>
              ) : (
                <article>
                  {Number(localStorage.getItem("prizeRank")) === 0 &&
                  localStorage.getItem("customerLost") === "Win" ? (
                    <div className="lotteryScreen price">
                      <div className="inner">
                        <div className="bg">
                          <h2 className="hd04">
                            <img src={ImgHeaderPart} alt="参加賞" />
                          </h2>
                          <p>
                            {loading ? (
                              <Spin
                                style={{
                                  zIndex: 2,
                                }}
                              />
                            ) : (
                              <img
                                src={
                                  prize?.[0]?.imageUrl
                                    ? prize?.[0]?.imageUrl
                                    : ImgWhite
                                }
                                alt="参加賞"
                              />
                            )}
                          </p>
                          <p className="another">
                            ご参加ありがとうございます！
                          </p>
                        </div>
                        <p className="link">
                          <button onClick={detectLink} className="btnLink">
                            {dataEvent?.event?.txtBtnWin ||
                              "プレゼントのページへ"}
                          </button>
                        </p>
                      </div>
                    </div>
                  ) : (
                    <>
                      {Number(localStorage.getItem("prizeRank")) !== 0 ? (
                        <div className="lotteryScreen per">
                          <div className="inner">
                            <h2 className="hd01">
                              <img src={ImgHeader} alt="当選" />
                            </h2>
                            <div className="winner">
                              <p>
                                {loading ? (
                                  <Spin
                                    style={{
                                      zIndex: 2,
                                    }}
                                  />
                                ) : (
                                  <img
                                    src={prize?.[0]?.imageUrl || ImgWhite}
                                    alt="当選"
                                  />
                                )}
                              </p>
                            </div>
                            <h2
                              className="hd02"
                              style={{ fontFamily: "emoji" }}
                            >
                              {prize?.[0]?.name}
                            </h2>
                            <p className="link">
                              <button
                                onClick={goToLink}
                                className="btnLink"
                                disabled={loadingButtonWin}
                              >
                                {dataEvent?.event?.txtBtnWin ||
                                  "プレゼントのページへ"}
                              </button>
                            </p>
                          </div>
                        </div>
                      ) : (
                        <div className="lotteryScreen off">
                          <div className="inner">
                            <div className="bg">
                              {loading ? (
                                <Spin
                                  style={{
                                    zIndex: 2,
                                  }}
                                />
                              ) : (
                                <>
                                  {prize?.[0]?.imageUrl ? (
                                    <>
                                      <h2
                                        className="hd03"
                                        style={{ margin: "0 50px" }}
                                      >
                                        <span className="pre">残念！</span>
                                      </h2>
                                      <p style={{ padding: "0 20px" }}>
                                        <img
                                          src={prize?.[0]?.imageUrl}
                                          alt="残念"
                                        />
                                      </p>
                                    </>
                                  ) : (
                                    <>
                                      <h2 className="hd03">
                                        <span className="pre">残念！</span>
                                        <img src={ImgLost} alt="当選" />
                                      </h2>
                                      <p>
                                        <img src={ImgLost2} alt="残念" />
                                      </p>
                                    </>
                                  )}
                                </>
                              )}

                              <p className="another">
                                またの挑戦をお待ちしております！
                              </p>
                            </div>
                            <p className="link">
                              <button
                                // href={
                                //   prize?.[0]?.url
                                //     ? `${prize?.[0]?.url}`
                                //     : `lbuttonnding-page?eventId=${eventId}`
                                //   // process.env.REACT_APP_PROJECT_NAME ===
                                //   // "LANDING_PAGE"
                                //   //   ? `${window.location.origin}/?eventId=${eventId}`
                                //   //   : `landing-page?eventId=${eventId}`
                                // }
                                onClick={detectLink}
                                className="btnLink"
                              >
                                {dataEvent?.event?.txtBtnLost || "ホームへ戻る"}
                              </button>
                            </p>
                          </div>
                        </div>
                      )}
                    </>
                  )}
                </article>
              )}
            </div>
          ) : (
            <div className="lotteryScreen off">
              <div
                className="inner"
                style={{ fontSize: "4rem", color: "white" }}
              >
                入力されたURLが正しくありません。
              </div>
            </div>
          )}
        </>
      )}
    </div>
  );
}

export default Lottery;
