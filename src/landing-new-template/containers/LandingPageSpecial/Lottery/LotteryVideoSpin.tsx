import React from 'react'
import VideoSpin from './VideoSpin/VideoSpin'

function LotteryVideoSpin({dataOrderPrizes}) {
  return (
      <VideoSpin dataOrderPrizes={dataOrderPrizes}/>
  )
}

export default LotteryVideoSpin
