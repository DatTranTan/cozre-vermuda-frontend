/* eslint-disable */
import React, { useState } from "react";
import { Wrapper } from "./VideoSpin.style";
import { useMutation, useQuery } from "@apollo/client";
import { useLocation } from "react-router-dom";
import { MUTATION_SPIN } from "../../../../graphql/mutation/spin";
import { Notification } from "../../../../components/Notification/NotificationCustom";
import { QUERY_EVENT } from "../../../../graphql/query/event";
import $ from "jquery";
import * as qs from "query-string";
import { Spin, Button } from "antd";
import { use100vh } from "react-div-100vh";
import { v4 as uuidv4 } from "uuid";
import Cookies from "universal-cookie";
import {
  MUTATION_INCREASE_WIN,
  MUTATION_SPUN,
} from "../../../../graphql/mutation/spun";
import ImgLottery from "../../style/img/common/background_white.png";
import "../../style/css/reset.css";
import "../../style/css/common.css";
import "../../style/css/sp.css";
import "../../style/css/tab.css";
import "../../style/css/pc.css";

function VideoSpin({ dataOrderPrizes }) {
  const NewID = uuidv4();
  const cookies = new Cookies();
  let customerID = "";
  async function clickCookies() {
    let newDate = await cookies.get("roleDay");
    let newCustomerId = await cookies.get("userID");
    if (newCustomerId) {
      if (newDate == new Date().getDay()) {
        customerID = newCustomerId;
      } else {
        customerID = NewID;
        cookies.set("userID", NewID);
        cookies.set("roleDay", new Date().getDay());
      }
    } else {
      customerID = NewID;
      cookies.set("userID", NewID);
      cookies.set("roleDay", new Date().getDay());
    }
  }
  const height = use100vh();
  const location = useLocation();
  const eventId = qs.parse(location.search).eventId;
  const [showPrize, setShowPrize] = React.useState(true);
  const [play, setPlay] = useState(false);
  const [dataSpinByEvent, setDataSpinByEvent] = useState<any>();
  const [visibleButton, setVisibleButton] = useState(false);
  const [idSpin, setIdSpin] = useState();
  const [winPrize, setWinPrize] = useState(false);
  const videoRef = React.useRef<HTMLVideoElement>(null);

  const [videoDimension, setVideoDimension] = useState({
    width: 0,
    height: 0,
  });
  const [loadingVideo, setLoadingVideo] = useState(true);
  const [customerLost, setCustomerLost] = useState("");
  const [style, setStyle] = React.useState({});
  const [showVideo, setShowVideo] = useState("");
  const [onStartVideo, setOnStartVideo] = useState(false);
  const [spinCircle, { loading }] = useMutation(MUTATION_SPIN, {
    fetchPolicy: "no-cache",
  });

  const [checkSpun] = useMutation(MUTATION_SPUN, {
    fetchPolicy: "no-cache",
  });
  const [increaseTotalWin] = useMutation(MUTATION_INCREASE_WIN, {
    fetchPolicy: "no-cache",
  });

  const { data, loading: loader } = useQuery(QUERY_EVENT, {
    variables: {
      id: eventId,
    },
  });
  const reloadButton = () => {
    checkSpun({
      variables: {
        eventId,
        userId: customerID,
      },
    }).then(({ data }) => {
      data?.checkSpun && setPlay(false);
      setLoadingVideo(false);
    });
    spinCircle({
      variables: {
        eventId,
        userId: customerID,
      },
    })
      .then(({ data }) => {
        setCustomerLost(data?.spin?.event?.customerLost);
        setDataSpinByEvent(data.spin);
        setIdSpin(data?.spin?.SpinPrizeID);
        if (
          data.spin.rank !== 0 ||
          (data.spin.rank === 0 && data.spin.event.customerLost === "Win")
        ) {
          setWinPrize(true);
          increaseTotalWin({
            variables: {
              eventId,
            },
          });
        }
        if (data.spin.rank === 0) {
          setWinPrize(true);
        }
      })
      .catch(({ graphQLErrors }) => {
        const mess = graphQLErrors.map((item) => item.message);
        console.log("graphQLErrors", mess);
        if (mess.includes("このイベントはまだ開始されていません")) {
          Notification({
            type: "error",
            message: " エラー",
            description: "イベントはまだ開始されていません。",
          });
          setShowPrize(true);
          setPlay(false);
        } else if (mess.includes("このイベントは既に終了しました")) {
          Notification({
            type: "error",
            message: " エラー",
            description: "このイベントは既に終了しました",
          });
          setShowPrize(true);
          setPlay(false);
        } else if (mess[0].includes("抽選しました")) {
          Notification({
            type: "error",
            message: " エラー",
            description: "本日は抽選済みです！１日１回チャレンジできます。",
          });
          setShowPrize(true);
          setPlay(false);
        } else {
          Notification({
            type: "error",
            message: " エラー",
            description: graphQLErrors[0]?.message,
          });
        }
      });
    setVisibleButton(false);
  };

  React.useEffect(() => {
    if (videoDimension?.width && videoDimension?.height) {
      if (videoDimension.width > videoDimension.height) {
        setStyle({ width: "100%", height: "auto", position: "absolute" });
      } else setStyle({ width: "100%", height: "auto", position: "absolute" });
    }
  }, [videoDimension]);

  React.useEffect(() => {
    if (dataSpinByEvent) {
      let video;
      if (
        data?.event.video !== null &&
        data?.event.video !== undefined &&
        dataSpinByEvent?.rank !== 0
      ) {
        video = data?.event.video?.url;
      } else if (
        data?.event.video !== null &&
        data?.event.video !== undefined &&
        dataSpinByEvent?.rank === 0
      ) {
        video =
          data?.event.noPrizeVideo === null ||
          data?.event.noPrizeVideo?.url === ""
            ? data?.event.video?.url
            : data?.event.noPrizeVideo.url;
      } else {
        video = "https://media.giphy.com/media/XyIveZZnnuNwksEkKm/source.mp4";
      }
      setShowVideo(video);
    }
  }, [dataSpinByEvent]);

  const handleEndVideo = () => {
    if (dataSpinByEvent) {
      localStorage.setItem("prize", "true");
      localStorage.setItem("prizeRank", dataSpinByEvent?.rank);
      localStorage.setItem("customerLost", customerLost);
      window.open(
        `${window.location.origin}/lottery?spin=${idSpin}&event=${eventId}`,
        "_self"
      );
    }
  };

  if (data?.event) {
    document.title = data?.event?.name;
  }

  return (
    <Wrapper>
      <div id="lottery" className="lotteryStart">
        <h2 className="hdL small">
          <span className="jap font_cp_reven">
            ボタンを押して
            <br />
            抽選スタート！
          </span>
        </h2>
        <div className="inner">
          <div style={{ display: play === false ? "" : "none" }}>
            <div className="lottery">
              <img
                src={data?.event?.videoBackground || ImgLottery}
                alt="抽選"
              />
            </div>
            <form method="post" action="lottery_off.html" id="form" name="form">
              <p className="button">
                <button
                  className="font_cp_reven"
                  disabled={play}
                  onClick={async () => {
                    await clickCookies();
                    await reloadButton();
                    setShowPrize(true);
                    setPlay(true);
                    // myFunction();
                    videoRef.current.play();
                    videoRef.current.muted = false;
                  }}
                >
                  抽選スタート
                </button>
              </p>
            </form>
          </div>
          <>
            {loading ||
              (loadingVideo && play && (
                <div className="lottery">
                  <img
                    src={data?.event?.videoBackground || ImgLottery}
                    alt="抽選"
                  />
                  <Spin
                    style={{
                      zIndex: 2,
                      position: "absolute",
                      top: "50%",
                      left: "50%",
                    }}
                  />
                </div>
              ))}
            <div
              className="lottery"
              style={{ display: play === true ? "" : "none" }}
            >
              <video
                id="myVideo"
                onPlay={() =>
                  setTimeout(() => {
                    setOnStartVideo(true);
                  }, 1000)
                }
                onLoadedData={() => {
                  setVideoDimension({
                    width: videoRef.current.offsetWidth,
                    height: videoRef.current.offsetHeight,
                  });
                  setLoadingVideo(false);
                }}
                ref={videoRef}
                src={showVideo}
                // src={"https://media.giphy.com/media/XyIveZZnnuNwksEkKm/source.mp4"}
                autoPlay={true}
                playsInline={true}
                muted={true}
                preload="yes"
                controls={false}
                onEnded={() => handleEndVideo()}
              />
            </div>
          </>
        </div>
      </div>
    </Wrapper>
  );
}

export default VideoSpin;
