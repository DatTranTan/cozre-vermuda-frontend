/* eslint-disable */
import { useMutation, useQuery } from "@apollo/react-hooks";
import { Modal } from "antd";
import { toNumeric } from "japanese-string-utils";
import queryString from "query-string";
import React, { useEffect, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { useHistory, useLocation } from "react-router-dom";
import { Error } from "../../../../../components/FormFields/FormFields";
import { Notification } from "../../../../../components/Notification/NotificationCustom";
import { MUTATION_CONFIRM_PRIZE } from "../../../../../graphql/mutation/confirm-prize";
import { MUTATION_CREATE_CUSTOMER } from "../../../../../graphql/mutation/create-customer";
import { QUERY_EVENT } from "../../../../../graphql/query/event";
import { THANKS } from "../../../../../settings/constants";
import "../../../style/css/reset.css";
import "../../../style/css/common.css";
import "../../../style/css/sp.css";
import "../../../style/css/tab.css";
import "../../../style/css/pc.css";
import { Form } from "./Contact.style";
import { RegisterCustomerForm } from "./Interface";
import Questionnaire from "./Questionnaire";

var genders = [
  { label: "男性", id: 0 },
  { label: "女性", id: 1 },
  { label: "回答しない", id: 2 },
];
interface EventFormInput {
  name: string;
  contact_number: string;
  email: string;
  reemail: string;
  address: string;
  thirdRequired: string;
  thirdNonRequired: string;
}

type Props = any;
const Contact: React.FC<Props> = (props) => {
  const location = useLocation();
  const history = useHistory();
  const eventID = queryString.parse(location.search).spin;
  const event = queryString.parse(location.search).event;
  const userRef = React.useRef<any>(null);
  const selectGenderRef = React.useRef<HTMLInputElement | HTMLDivElement>(null);
  const [visible, setVisible] = useState(false);
  const [modal, contextHolder] = Modal.useModal();
  const [multiAnswer, setMultiAnswer] = useState({});
  const [justOneAnswer, setJustOneAnswer] = useState({ index: "", value: "" });
  const [textRequired, setTextRequired] = useState({});
  const [textNonRequired, setTextNonRequired] = useState({});
  const [isConfirm, setIsConfirm] = useState(true);
  const [formData, setFormData]: any = useState({});
  const { data: dataEvent } = useQuery(QUERY_EVENT, {
    variables: {
      id: event,
    },
  });
  const questionnaire = dataEvent?.event.questionnaire?.questionList;
  const listQuestion = questionnaire?.map((item) => ({
    id: item?.id,
    listQuestion: item?.question,
  }));
  const [listAnswer, setListAnswer] = useState([]);
  const [questionList, setQuestionList] = useState([]);
  useEffect(() => {
    if (dataEvent?.event?.questionnaire?.questionList) {
      let cloneQuestionList = [
        ...dataEvent?.event?.questionnaire?.questionList,
      ];
      const questionListOrderByASC = cloneQuestionList?.sort(
        (a, b) => a?.id - b?.id
      );
      setQuestionList(questionListOrderByASC);
    }
  }, [dataEvent]);

  const {
    register,
    handleSubmit,
    setValue,
    getValues,
    errors,
    setError,
    reset,
    control,
  } = useForm<RegisterCustomerForm>({
    defaultValues: {
      name: "",
      gender: "",
      contact_number: "",
      email: "",
      reemail: "",
      address: "",
      postalCode: "",
    },
  });

  const [type, setType] = React.useState(null);
  const [onDisable, setOnDisable] = React.useState(false);
  const [AddCustomer] = useMutation(MUTATION_CREATE_CUSTOMER, {
    fetchPolicy: "no-cache",
  });
  const handleTypeChange = (e) => {
    setType(e.target.value);
  };

  const handleFormSubmit = (data) => {
    if (!localStorage.getItem("prize")) {
      Notification({
        type: "error",
        message: " エラー",
        description: "抽選結果を取得できませんでした。",
      });
      return;
    }
    setOnDisable(true);
    const questionnaire = [];
    questionList.map((item, index) => {
      questionnaire.push({
        ...item,
        answer: data[`content${index}`],
      });
    });
    data.email === data.reemail &&
      AddCustomer({
        variables: {
          createCustomerInput: {
            name: data.name,
            age: parseFloat(toNumeric(data?.age)),
            gender: type[0]?.label,
            tel: data.contact_number,
            email: data.email,
            address: data.address,
            postalCode: data.postalCode,
            spinPrizeId: eventID,
            questionnaire: JSON.stringify(questionnaire),
          },
        },
      })
        .then(() => {
          Notification({
            type: "success",
            message: "ご応募頂き有難うございました。",
          });
          localStorage.removeItem("prize");
          localStorage.removeItem("rank");
          localStorage.removeItem("customerLost");
          localStorage.removeItem("prizeRank");
          reset();
          history.replace(`${THANKS}?eventId=${event}`);
          setOnDisable(false);
        })
        .catch((err) => {
          Notification({
            type: "error",
            message: " エラー",
            description: err.toString().replace("Error: ", ""),
          });
          setOnDisable(false);
        });
  };
  const onSubmit = (data: any) => {
    if (data.email !== data.reemail) {
      setError("email", "notmatch", "abcxyz");
      setError("reemail", "notmatch", "abcxyz");
      return;
    }
    setIsConfirm(false);
    setFormData(data);
  };

  const handleBackForm = (data: any) => {
    setIsConfirm(true);
  };

  React.useEffect(() => {
    register({ name: "name", required: true, maxLength: 50 });
    register({ name: "tel", required: true, maxLength: 20 });
    register({ name: "email" }, { required: true, maxLength: 100 });
    register({ name: "reemail" }, { required: true, maxLength: 100 });
    register({ name: "address", required: true });
    register({ name: "age", required: true });
    register({ name: "gender" }, { required: true });
    register({ name: "postalCode", required: true });
  }, [register]);

  React.useEffect(() => {
    if (userRef.current) userRef.current.focus();
  }, [userRef]);
  let newArrQuestion = [];
  listQuestion?.forEach((item1) => {
    const code = listAnswer?.find((item2) => item2?.index === item1.id);
    if (code) {
      const array = Array.isArray(code?.value)
        ? code?.value.join(",")
        : code.value;
      newArrQuestion.push(item1.listQuestion, array);
    }
  });

  if (dataEvent?.event) {
    document.title = dataEvent?.event?.name;
  }

  return (
    <>
      <>
        {!localStorage.getItem("prize") ? (
          <div id="wrapper">
            <div id="contents">
              <div className="lotteryScreen off">
                <div
                  className="inner"
                  style={{ fontSize: "4rem", color: "white" }}
                >
                  抽選結果を取得できませんでした。{" "}
                </div>
              </div>
            </div>
          </div>
        ) : (
          <Form onSubmit={handleSubmit(onSubmit)} id="myForm">
            <div id="wrapper">
              <article>
                <div id="contents">
                  <div
                    className="contactForm"
                    style={{ display: isConfirm ? "block" : "none" }}
                  >
                    <div className="inner">
                      <div className="bg">
                        <h2 className="hdM">当選者様情報入力フォーム</h2>
                        {dataEvent?.event?.timeOut > 0 && (
                          <p style={{ color: "red" }}>
                            当選時の応募情報の入力は時間制限がございます。
                            {dataEvent?.event?.timeOut}
                            分を越えての送信は当選無効となりますのでお早めにご入力・送信ください。
                          </p>
                        )}
                        <p>
                          必要な情報を入力してください。
                          <span className="colorRed">
                            ( * )は必須の項目です。
                          </span>
                        </p>
                        <table>
                          <tbody>
                            <tr>
                              <th>
                                氏名<span className="colorRed">*</span>
                              </th>
                              <td>
                                <input
                                  type="text"
                                  name="name"
                                  ref={(e: any) => {
                                    register(e, {
                                      required: true,
                                      maxLength: 50,
                                      minLength: 2,
                                    });
                                    userRef.current = e;
                                  }}
                                />
                                {errors.name &&
                                  errors.name.type === "required" && (
                                    <Error>これは必須の項目です。</Error>
                                  )}
                                {errors.name &&
                                  errors.name.type === "minLength" && (
                                    <Error>
                                      氏名は2文字以上設定してください。
                                    </Error>
                                  )}
                                {errors.name &&
                                  errors.name.type === "maxLength" && (
                                    <Error>
                                      氏名は50文字以下設定してください。
                                    </Error>
                                  )}
                              </td>
                            </tr>
                            <tr>
                              <th>年齢</th>
                              <td>
                                <input
                                  type="text"
                                  placeholder="年齢を入力してください(半角/全角)"
                                  ref={register({
                                    maxLength: 2,
                                    minLength: 0,
                                    pattern:
                                      /^[^a-z\^@\^!\^#\^$\^%\^^\^&\^*\^+\^=\^.\^<\^>\^:\^,\^"\^~\^`\^_\^-\^ぁ-ゔ\^一-龠\^ァ-ヴー\^＠\^！\^＃\^＄\^％\^＾\^＆\^＊\^（\^）\^＿\^-]+$/,
                                  })}
                                  name="age"
                                />
                                {errors.age &&
                                  errors.age.type === "pattern" && (
                                    <Error>
                                      年齢には100以下入力してください。
                                    </Error>
                                  )}
                                {errors.age &&
                                  errors.age.type === "minLength" && (
                                    <Error>
                                      年齢は0歳以上入力してください。
                                    </Error>
                                  )}
                                {errors.age &&
                                  errors.age.type === "maxLength" && (
                                    <Error>
                                      年齢には100以下入力してください。
                                    </Error>
                                  )}
                              </td>
                            </tr>
                            <tr>
                              <th>
                                性別<span className="colorRed">*</span>
                              </th>
                              <td>
                                <select
                                  name="gender"
                                  style={{ color: type ? "black" : "" }}
                                  ref={register({
                                    required: true,
                                  })}
                                  onChange={handleTypeChange}
                                >
                                  <option value="" disabled selected hidden>
                                    性別を選択する
                                  </option>
                                  {genders.map((item) => {
                                    return (
                                      <option value={item.id}>
                                        {item.label}
                                      </option>
                                    );
                                  })}
                                </select>
                                {errors.gender &&
                                  errors.gender.type === "required" && (
                                    <Error>これは必須の項目です。</Error>
                                  )}
                              </td>
                            </tr>
                            <tr>
                              <th>
                                郵便番号<span className="colorRed">*</span>
                              </th>
                              <td>
                                <input
                                  type="text"
                                  ref={register({
                                    maxLength: 50,
                                    required: true,
                                  })}
                                  name="postalCode"
                                />
                                {errors.postalCode &&
                                  errors.postalCode.type === "required" && (
                                    <Error>これは必須の項目です。</Error>
                                  )}
                                {errors.postalCode &&
                                  errors.postalCode.type === "maxLength" && (
                                    <Error>
                                      {" "}
                                      郵便番号は50文字以下設定してください。
                                    </Error>
                                  )}
                              </td>
                            </tr>
                            <tr>
                              <th>
                                住所<span className="colorRed">*</span>
                              </th>
                              <td>
                                <input
                                  type="text"
                                  ref={register({
                                    maxLength: 50,
                                    required: true,
                                  })}
                                  name="address"
                                />
                                {errors.address &&
                                  errors.address.type === "required" && (
                                    <Error>これは必須の項目です。</Error>
                                  )}
                                {errors.address &&
                                  errors.address.type === "maxLength" && (
                                    <Error>
                                      住所は50文字以下設定してください。
                                    </Error>
                                  )}
                              </td>
                            </tr>
                            <tr>
                              <th>
                                メールアドレス
                                <span className="colorRed">*</span>
                              </th>
                              <td>
                                <input
                                  type="text"
                                  ref={register({
                                    required: true,
                                    maxLength: 100,
                                    pattern:
                                      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                                  })}
                                  name="email"
                                />
                                {errors.email &&
                                  errors.email.type === "required" && (
                                    <Error>これは必須の項目です。</Error>
                                  )}
                                {errors.email &&
                                  errors.email.type === "pattern" && (
                                    <Error>
                                      メールアドレスの形式は正しくありません。
                                    </Error>
                                  )}
                                {errors.email &&
                                  errors.email.type === "maxLength" && (
                                    <Error>
                                      メールアドレス100文字以下設定してください。
                                    </Error>
                                  )}
                                {errors.reemail &&
                                  errors.reemail.type === "notmatch" && (
                                    <Error>
                                      確認用のメールアドレスは一致していません
                                    </Error>
                                  )}
                              </td>
                            </tr>
                            <tr>
                              <th>
                                メールアドレス (確認用)
                                <span className="colorRed">*</span>
                              </th>
                              <td>
                                <input
                                  type="text"
                                  ref={register({
                                    required: true,
                                    maxLength: 100,
                                    pattern:
                                      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                                  })}
                                  name="reemail"
                                />
                                {errors.reemail &&
                                  errors.reemail.type === "required" && (
                                    <Error>これは必須の項目です。</Error>
                                  )}
                                {errors.reemail &&
                                  errors.reemail.type === "pattern" && (
                                    <Error>
                                      メールアドレスの形式は正しくありません。
                                    </Error>
                                  )}
                                {errors.reemail &&
                                  errors.reemail.type === "maxLength" && (
                                    <Error>
                                      メールアドレス100文字以下設定してください。
                                    </Error>
                                  )}
                                {errors.reemail &&
                                  errors.reemail.type === "notmatch" && (
                                    <Error>
                                      確認用のメールアドレスは一致していません
                                    </Error>
                                  )}
                              </td>
                            </tr>
                            <tr>
                              <th>
                                電話番号<span className="colorRed">*</span>
                              </th>
                              <td>
                                <input
                                  type="text"
                                  name="contact_number"
                                  ref={register({
                                    maxLength: 20,
                                    required: true,
                                  })}
                                />
                                {errors.contact_number &&
                                  errors.contact_number.type === "required" && (
                                    <Error>これは必須の項目です。</Error>
                                  )}
                                {errors.contact_number &&
                                  errors.contact_number.type ===
                                    "maxLength" && (
                                    <Error>
                                      電話番号は20文字以下設定してください。
                                    </Error>
                                  )}
                              </td>
                            </tr>
                          </tbody>
                        </table>

                        <Questionnaire
                          questionList={questionList}
                          formData={formData}
                          isConfirm={isConfirm}
                          errors={errors}
                          register={register}
                          setValue={setValue}
                          Controller={Controller}
                          control={control}
                        />

                        <div
                          className="teamBtns"
                          style={{ display: "flex", justifyContent: "center" }}
                        >
                          <input
                            type="submit"
                            className="submit btnLink"
                            defaultValue="次へ"
                            value="次へ"
                          />
                        </div>
                      </div>
                    </div>
                  </div>

                  <div
                    className="contactForm confirm"
                    style={{ display: isConfirm ? "none" : "block" }}
                  >
                    <div className="inner">
                      <div className="bg">
                        <h2 className="hdM">入力内容の確認</h2>
                        {dataEvent?.event?.timeOut > 0 && (
                          <p style={{ color: "red" }}>
                            当選時の応募情報の入力は時間制限がございます。
                            {dataEvent?.event?.timeOut}
                            分を越えての送信は当選無効となりますのでお早めにご入力・送信ください。
                          </p>
                        )}

                        <form
                          method="post"
                          action="thanks.html"
                          id="mailform"
                          name="mailform"
                        >
                          <table>
                            <tbody>
                              <tr>
                                <th>氏名</th>
                                <td>{formData?.name}</td>
                              </tr>
                              <tr>
                                <th>年齢</th>
                                <td>{formData?.age}</td>
                              </tr>
                              <tr>
                                <th>性別</th>
                                <td>
                                  {formData?.gender == "0"
                                    ? genders[0].label
                                    : formData?.gender == "1"
                                    ? genders[1].label
                                    : genders[2].label}
                                </td>
                              </tr>
                              <tr>
                                <th>郵便番号</th>
                                <td>{formData?.postalCode}</td>
                              </tr>
                              <tr>
                                <th>住所</th>
                                <td>{formData?.address}</td>
                              </tr>
                              <tr>
                                <th>メールアドレス</th>
                                <td>{formData?.email}</td>
                              </tr>
                              <tr>
                                <th>電話番号</th>
                                <td>{formData?.contact_number}</td>
                              </tr>
                              <Questionnaire
                                questionList={questionList}
                                errors={errors}
                                register={register}
                                setValue={setValue}
                                Controller={Controller}
                                control={control}
                                formData={formData}
                                isConfirm={isConfirm}
                              />
                            </tbody>
                          </table>
                          <div
                            className="teamBtns"
                            style={{
                              display: "flex",
                              justifyContent: "center",
                            }}
                          >
                            <input
                              type="button"
                              className="edit btnLink"
                              defaultValue="戻る"
                              onClick={() => handleBackForm(formData)}
                            />
                            <input
                              disabled={onDisable}
                              type="button"
                              className="btnLink"
                              defaultValue="送信"
                              onClick={() => handleFormSubmit(formData)}
                            />
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </article>
            </div>
          </Form>
        )}
      </>
    </>
  );
};
export default Contact;
