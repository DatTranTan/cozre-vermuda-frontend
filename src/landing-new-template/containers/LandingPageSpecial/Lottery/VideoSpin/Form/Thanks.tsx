/* eslint-disable */
import React from "react";
import "../../../style/css/reset.css";
import "../../../style/css/common.css";
import "../../../style/css/sp.css";
import "../../../style/css/tab.css";
import "../../../style/css/pc.css";

import { useHistory, useLocation } from "react-router-dom";
import queryString from "query-string";
import { QUERY_EVENT } from "../../../../../graphql/query/event";
import { useMutation, useQuery } from "@apollo/react-hooks";

type Props = any;
const Thanks: React.FC<Props> = (props) => {
  const location = useLocation();
  const event = queryString.parse(location.search).eventId;

  const { data: dataEvent } = useQuery(QUERY_EVENT, {
    variables: {
      id: event,
    },
  });

  if (dataEvent?.event) {
    document.title = dataEvent?.event?.name;
  }

  return (
    <>
      <div id="wrapper">
        <article>
          <div id="contents">
            <div className="contactForm">
              <div className="inner">
                <div className="bg">
                  <h2 className="hdM">送信完了</h2>
                  <p className="thanks">ご応募ありがとうございました。</p>
                  <div className="teamBtns">
                    <a
                      href={
                        process.env.REACT_APP_PROJECT_NAME === "LANDING_PAGE"
                          ? `${window.location.origin}/?eventId=${event}`
                          : `landing-page?eventId=${event}`
                      }
                      className="btnLink"
                    >
                      ホームに戻る
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </article>
      </div>
    </>
  );
};
export default Thanks;
