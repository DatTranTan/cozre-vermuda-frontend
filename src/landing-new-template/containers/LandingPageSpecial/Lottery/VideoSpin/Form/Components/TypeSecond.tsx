/* eslint-disable */
import React, { useState, useEffect } from "react";
import { useHistory, useLocation } from "react-router-dom";
import {
  Error,
} from "../../../../../../components/FormFields/FormFields";

type Props = any;
const Questionnaire: React.FC<Props> = (props) => {
  const { data, index, setValue, register, errors, Controller, control } =
    props;
  const [content, setContent] = useState([]);
  useEffect(() => {
    if (data) {
      const list = data.content.split(",");
      setContent(list);
    }
  }, [data]);

  return (
    <>
      <tr>
        <th>
          {data.question}
          <span className="colorRed">*</span>
        </th>
        <td>
          <div className="radioStyle">
            {content.map((itemQ, indexQ) => {
              return (
                <label>
                  <input type="radio" value={itemQ} 
                   name={`content${index}`}
                   ref={register({
                    required: true,
                  })}
                  />
                  {itemQ}
                </label>
              );
            })}
          </div>
          {errors[`content${index}`] &&
          errors[`content${index}`].type === "required" && (
            <Error>これは必須の項目です。</Error>
          )}
        </td>
      </tr>
     
    </>
  );
};
export default Questionnaire;
