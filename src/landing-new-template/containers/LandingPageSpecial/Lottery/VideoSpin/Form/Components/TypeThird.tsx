/* eslint-disable */
import React, { useState, useEffect } from "react";
import {
  Error,
} from "../../../../../../components/FormFields/FormFields";

type Props = any;
const Questionnaire: React.FC<Props> = (props) => {
  const { data, index, setValue, register, errors } = props;

  return (
    <>
      <tr>
        <th>
          {data.question}
          {data.content.toUpperCase() === "TRUE" && (
            <>
              <span className="colorRed">*</span>
            </>
          )}
        </th>
        <td >
          <textarea defaultValue={""} 
          ref={register({
            required: data.content.toUpperCase() === "TRUE",
          })}
          name={`content${index}`}
          />
          {errors[`content${index}`] &&
          errors[`content${index}`].type === "required" && (
            <Error>これは必須の項目です。</Error>
          )}
        </td>
      </tr>
    </>
  );
};
export default Questionnaire;
