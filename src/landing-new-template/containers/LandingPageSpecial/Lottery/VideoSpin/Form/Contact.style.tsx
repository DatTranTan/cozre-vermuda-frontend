import { styled } from 'baseui'

export const ContainerForm = styled('div', () => ({
  width: '100%',
  height: '70vh',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center'
}))

export const Form = styled('form', ({ $theme }) => ({
  // height: '70vh',
  // height: '30.8rem',
  height: '100%',
  backgroundColor: $theme.colors.backgroundF0,
  '@media only screen and (max-width: 1150px) and (max-height:1400px)': {
    // height: '25.6rem'
    height: '80%'
  },
  '@media only screen and (max-width: 1050px) and (max-height:1200px) ': {
    // height: '25.6rem'
    height: '95%'
  },
  '@media only screen and (max-width: 500px)': {
    // height: '25.6rem'
    height: '95%'
  },
  '@media only screen and (max-width: 320px)': {
    height: '20.8rem'
  }
}))

export const BlockForm = styled('div', () => ({
  width: '50%',
  height: '100%',
  // display: 'flex',
  // alignItems: 'center',
  // jutifyContent: 'center',
  '@media only screen and (max-width: 1024px)': {
    width: '80%'
  },
  '@media only screen and (max-width: 500px)': {
    width: '90%',
    height: '90%'
  }
}))

export const BlockTitle = styled('div', () => ({
  textAlign: 'center',
  fontSize: '2.5rem',
  fontWeight: '400',
  color: '#e08444',
  marginBottom: '1rem',
  marginTop: '1rem',
  '@media only screen and (max-width: 500px)': {
    fontSize: '1.5rem',
    fontWeight: '600',
    color: '#e08444',
    marginTop: '0',
    marginBottom: '1rem'
  }
}))

export const ButtonGroup = styled('div', ({ $theme }) => ({
  // position: 'absolute',
  bottom: 0,
  paddingTop: '24px',
  display: 'flex',
  width: '100%',
  alignItems: 'center',
  // width: 'calc(100% - 48px)',
  backgroundColor: '#ffffff'
  // boxShadow: '0 0 3px rgba(0, 0, 0, 0.1)'
  // borderTop: '1px solid rgba(0, 0, 0, 0.1)'
}))

export const ButtonLinkGroup = styled('div', ({ $theme }) => ({
  // position: 'absolute',
  bottom: 0,
  padding: '24px 5vw',
  display: 'flex',
  width: '100%',
  alignItems: 'center',
  // width: 'calc(100% - 48px)',
  backgroundColor: '#ffffff'
  // boxShadow: '0 0 3px rgba(0, 0, 0, 0.1)'
  // borderTop: '1px solid rgba(0, 0, 0, 0.1)'
}))

export const Container = styled('div', () => ({
  overflowX: 'hidden',
  display: 'flex',
  flexDirection: 'row',
  // height: '100%',
  // height: '50rem',
  overflowY: 'auto',

  '@media only screen and (max-width: 1280px)': {
    flexDirection: 'column'
  }
}))

export const Wrapper = styled('div', () => ({
  display: 'flex',
  position: 'relative',
  flexDirection: 'column',
  textAlign: 'left',
  // height: '50rem',
  paddingBottom: '1rem',
  // width: '481px',
  // overflowY: 'scroll',
  '@media only screen and (max-width: 1024px)': {
    width: '100%'
  }
  // ".ant-button": {
  //   backgroundColor: 'black'
  // }
}))

export const RequiredWrapper = styled('div', () => ({
  display: 'flex',
  width: '100%',
  margin: '0.1rem 0 0.5rem 1.25rem ',
  zIndex: 10,
  flexDirection: 'row',
  alignItems: 'flex-end',
  justifyContent: 'flex-start'
}))

export const FieldDetails = styled('span', ({ $theme }) => ({
  ...$theme.typography.font14,
  padding: '28px 0 15px',
  color: $theme.colors.textNormal,
  display: 'block',

  '@media only screen and (max-width: 991px)': {
    padding: '30px 0'
  }
}))

export const ContainerSuccess = styled('div', () => ({
  width: '100%',
  height: '100vh',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  flexFlow: 'column',
  color: '#82b564',
  fontSize: '2rem',
  fontWeight: '600',
  '@media only screen and (max-width: 500px)': {
    fontSize: '1rem'
  }
}))

export const ButtonToLink = styled('div', () => ({
  width: '100%',
  height: '100vh',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  flexFlow: 'column',
  color: '#82b564',
  fontSize: '2rem',
  fontWeight: '600',
  '@media only screen and (max-width: 500px)': {
    fontSize: '1rem'
  }
}))
