/* eslint-disable */
import { useMutation, useQuery } from "@apollo/react-hooks";
import queryString from "query-string";
import React, { useEffect, useRef, useState } from "react";
import { useLocation } from "react-router-dom";
import { MUTATION_VISIT } from "../../graphql/mutation/total-visit";
import { QUERY_EVENT } from "../../graphql/query/event";
import { InLineLoader } from "../../components/InlineLoader/InlineLoader";

import ImgSample from "./style/img/top/image_white_600x500.png";
import ImgPageTop from "./style/img/common/pagetop.svg";
import "./style/css/reset.css";
import "./style/css/common.css";
import "./style/css/sp.css";
import "./style/css/tab.css";
import "./style/css/pc.css";
import IntroductionCampaign from "./Introduction/IntroductionCampaign";
import Lottery from "./Lottery/LotteryVideoSpin";

function LandingPage() {
  const location = useLocation();
  const eventID = queryString.parse(location.search).eventId;
  const refWidth = useRef(null);

  const [dataOrderPrizes, setDataOrderPrizes] = useState([]);

  const { data, loading, error } = useQuery(QUERY_EVENT, {
    variables: {
      id: eventID,
    },
  });

  // if (!eventID || !data) {
  //   return (
  //     <>
  //       <div className="lotteryScreen off">
  //         <div className="inner" style={{ fontSize: "4rem", color: "white" }}>
  //           抽選結果を取得できませんでした。{" "}
  //         </div>
  //       </div>
  //     </>
  //   );
  // }

  useEffect(() => {
    if (data?.event) {
      let dataPrizes = eventID && data?.event.prizes;
      let losePrizes =
        dataPrizes && dataPrizes?.filter((item) => item.rank === 0);
      let winPrizes =
        dataPrizes && dataPrizes?.filter((item) => item.rank !== 0);
      setDataOrderPrizes([...winPrizes, ...losePrizes]);
    }
  }, [data]);

  const [increaseTotalVisit] = useMutation(MUTATION_VISIT, {
    fetchPolicy: "no-cache",
  });
  useEffect(() => {
    if (data?.event) {
      increaseTotalVisit({
        variables: {
          eventId: eventID,
        },
      });
      if (process.env.REACT_APP_PROJECT_NAME === "LANDING_PAGE") {
        document.title = "Landingpage";
        const favicon = document.querySelector("link");
        favicon.href = "https://www.google.com/favicon.ico";
      }
    }
  }, []);

  return (
    <div id="top">
      {loading ? (
        <InLineLoader />
      ) : (
        <>
          {data?.event ? (
            <div id="wrapper">
              <article>
                <div id="mainVisual">
                  <picture>
                    <img
                      src={data?.event?.banner || ImgSample}
                      alt="SPECIAL PRESENT CAMPAIGN"
                      style={
                        data?.event?.bannerUrl === ""
                          ? {}
                          : { cursor: "pointer" }
                      }
                      onClick={() => {
                        if (data?.event?.bannerUrl !== "")
                          window.open(data?.event?.bannerUrl, "_blank");
                      }}
                    />
                  </picture>
                  <a href="#lottery" className="scrollTxt">
                    すぐ抽選!
                  </a>
                  <a href="#lottery" className="scroll" />
                </div>
                <div id="contents">
                  <div className="prizeList">
                    <h2 className="hdL">
                      <span className="jap font_cp_reven">賞品一覧</span>
                    </h2>
                    <div className="inner">
                      <ul>
                        {dataOrderPrizes
                          ?.filter((item) =>
                            data?.event?.customerLost === "Win"
                              ? item
                              : item?.rank !== 0
                          )
                          ?.map((item, index) => (
                            <li>
                              <p className="tit font_cp_reven">{item?.name}</p>
                              <p>
                                <img
                                  src={item?.imageUrl || ImgSample}
                                  alt="景品名サンプル"
                                />
                              </p>
                              <p className="people font_cp_reven">
                                <span
                                  className="num "
                                  style={{ fontFamily: "CP_Revenge" }}
                                >
                                  {item?.rank !== 0 ? item?.quantity : ""}
                                </span>
                                <span style={{ fontFamily: "CP_Revenge" }}>
                                  {item?.rank !== 0 ? "名様" : ""}
                                </span>
                              </p>
                            </li>
                          ))}
                      </ul>
                    </div>
                  </div>
                  <Lottery dataOrderPrizes={dataOrderPrizes} />
                  <div className="campaignOverview">
                    <h2 className="hdL">
                      <span className="jap font_cp_reven">
                        キャンペーン概要
                      </span>
                    </h2>
                    <div className="inner">
                      <div className="txt">
                        <IntroductionCampaign data={data} />
                      </div>
                    </div>
                  </div>
                </div>
              </article>
              <p className="pageTop">
                <a href="#">
                  <img src={ImgPageTop} alt="" />
                </a>
              </p>
            </div>
          ) : (
            <div className="lotteryScreen off">
              <div
                className="inner"
                style={{ fontSize: "4rem", color: "white" }}
              >
                 入力されたURLが正しくありません。
              </div>
            </div>
          )}
        </>
      )}
    </div>
  );
}

export default LandingPage;
