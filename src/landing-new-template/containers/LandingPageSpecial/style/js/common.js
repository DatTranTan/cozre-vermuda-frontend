/**
 * common.js
 *
 *  version --- 1.0
 *  updated --- 2017/11/30
 */

/* !stack ------------------------------------------------------------------- */
jQuery(document).ready(function($) {
  var os = function (){
      var ua = navigator.userAgent,
      isWindowsPhone = /(?:Windows Phone)/.test(ua),
      isSymbian = /(?:SymbianOS)/.test(ua) || isWindowsPhone,
      isAndroid = /(?:Android)/.test(ua),
      isFireFox = /(?:Firefox)/.test(ua),
      isChrome = /(?:Chrome|CriOS)/.test(ua),
      isTablet = /(?:iPad|PlayBook)/.test(ua) || (isAndroid && !/(?:Mobile)/.test(ua)) || (isFireFox && /(?:Tablet)/.test(ua)),
      isPhone = /(?:iPhone)/.test(ua) && !isTablet,
      isPc = !isPhone && !isAndroid && !isSymbian;
      return {
          isTablet: isTablet,
          isPhone: isPhone,
          isAndroid: isAndroid,
          isPc: isPc
      };	
  }();
  if (os.isAndroid || os.isPhone) {
      $('head').append('<meta name="viewport" content="width=375,user-scalable=no">');
  } else if (os.isTablet) {
      $('head').append('<meta name="viewport" content="width=1024,user-scalable=no">');
  } else if (os.isPc) {
      $('head').append('<meta name="viewport" content="width=device-width,user-scalable=no">');
  }
});

/* !isUA -------------------------------------------------------------------- */
var isUA = (function(){
	var ua = navigator.userAgent.toLowerCase();
	indexOfKey = function(key){ return (ua.indexOf(key) != -1)? true: false;}
	var o = {};
	o.ie      = function(){ return indexOfKey("msie"); }
	o.fx      = function(){ return indexOfKey("firefox"); }
	o.chrome  = function(){ return indexOfKey("chrome"); }
	o.opera   = function(){ return indexOfKey("opera"); }
	o.android = function(){ return indexOfKey("android"); }
	o.ipad    = function(){ return indexOfKey("ipad"); }
	o.ipod    = function(){ return indexOfKey("ipod"); }
	o.iphone  = function(){ return indexOfKey("iphone"); }
	return o;
})();

/* !pageScroll -------------------------------------------------------------- */
jQuery.easing.easeInOutCubic = function (x, t, b, c, d) {
	if ((t/=d/2) < 1) return c/2*t*t*t + b;
	return c/2*((t-=2)*t*t + 2) + b;
}; 

$(window).on('load resize',function(){
	$('a.scroll, .scroll a').each(function(){
		$(this).unbind('click').bind("click keypress",function(e){
			e.preventDefault();
			var target  = $(this).attr('href');
			var targetY = $(target).offset().top;
			var parent  = ( isUA.opera() )? (document.compatMode == 'BackCompat') ? 'body': 'html' : 'html,body';
			$(parent).animate(
				{scrollTop: targetY },
				400
			);
			return false;
		});
	});
});




/* !common --------------------------------------------------- */
$(function(){
	$('.pageTop a').click(function(){
		$('html,body').animate({scrollTop: 0}, 'slow','swing');
		return false;
	});
});

function lottery(){
	jQuery(function($){
		var num = Math.floor(Math.random()*10+1);
		if(num == 1 || num == 5){
			document.form.action='lottery.html';
		}else if(num == 2 || num == 4 || num == 9){
			document.form.action='participation.html';
		}else{
			document.form.action='lottery_off.html';
		}
		document.form.submit();
	});
}


