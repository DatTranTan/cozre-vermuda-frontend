// **************** ROUTE CONSTANT START **************************
// General Page Section

export const LANDING_PAGE = '/'
export const CONTACT = '/contact'
export const FORMCONFIRM = '/form-confirm'
export const THANKS = '/thanks'
export const LOTTERY = '/lottery'
// **************** ROUTE CONSTANT END **************************

export const CURRENCY = '$'
