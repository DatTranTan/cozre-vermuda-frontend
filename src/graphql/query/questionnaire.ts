import gql from 'graphql-tag'

export const QUERY_QUESTIONNAIRE = gql`
  query ($name: String, $offset: Float, $limit: Float) {
    getAllQuestionnaire(name: $name, limit: $limit, offset: $offset) {
      count
      questionnaires {
        id
        question
        questionList {
          id
          question
          type
          content
          createdAt
        }
        type
        content
        createdAt
        updatedAt
        owner {
          id
          nameKanji
          companyName
        }
      }
    }
  }
`
export const QUERY_AGENCYS = gql`
  query ($searchText: String, $skip: Float, $take: Float, $userType: String) {
    agencys: users(
      searchText: $searchText
      skip: $skip
      take: $take
      userType: $userType
    ) {
      count
      users {
        id
        nameKanji
      }
    }
  }
`
