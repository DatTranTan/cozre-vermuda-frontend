import gql from 'graphql-tag'

export const QUERY_USER = gql`
  query($id: String!) {
    user(id: $id) {
      id
      pwd
      nameKanji
      companyName
      userType {
        id
        role
        name
      }
      agency {
        id
        pwd
        nameKanji
        companyName
        userType {
          id
          role
          name
        }
        tel
        email
        address
        accessToken
        expiresTime
        refreshToken
        maxClient
      }
      tel
      email
      address
      accessToken
      expiresTime
      refreshToken
      maxClient
    }
  }
`

export const QUERY_MAXCLIENT = gql`
  query($id: String!) {
    user(id: $id) {
      maxClient
    }
  }
`
