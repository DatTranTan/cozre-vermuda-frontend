import gql from 'graphql-tag'

export const QUERY_CAMPAIGNS = gql`
  query ($name: String, $offset: Float = 0, $limit: Float = 0) {
    Campaigns(name: $name,offset: $offset, limit: $limit) {
     count
     campaign {
       id
       name
       content
       createdAt
       updatedAt
     }
    }
  }
`

export const QUERY_CAMPAIGN = gql`
  query ($Id: String!) {
    Campaign(Id: $Id) {
      id
      name
      content
    }
  }
`