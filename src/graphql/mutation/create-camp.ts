import gql from 'graphql-tag'

export const MUTATION_CREATE_CAMP = gql`
mutation($createCampaignInput: CreateCampaignInput!) {
    createCampaign(createCampaignInput: $createCampaignInput) {
        id
        name
        content
    }
}
`
export const MUTATION_UPDATE_CAMP = gql`
mutation($updateCampaignInput: UpdateCampaignInput!) {
    updateCampaign(updateCampaignInput: $updateCampaignInput) {
        id
        name
        content
    }
}
`