import gql from 'graphql-tag'

export const MUTATION_VISIT = gql`
  mutation($eventId: String!) {
    increaseTotalVisit(eventId: $eventId)
  }
`
