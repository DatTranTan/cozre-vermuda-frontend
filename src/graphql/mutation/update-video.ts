import gql from 'graphql-tag'

export const MUTATION_UPDATE_VIDEO = gql`
  mutation($updateVideoInput: UpdateVideoInput!) {
    updateVideo(updateVideoInput: $updateVideoInput) {
      id
      name
      url
      updatedAt
    }
  }
`
