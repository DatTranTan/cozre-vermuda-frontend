import gql from 'graphql-tag'

export const MUTATION_DELETE_EVENT = gql`
  mutation($ids: [String!]!) {
    deleteEvents(ids: $ids)
  }
`
