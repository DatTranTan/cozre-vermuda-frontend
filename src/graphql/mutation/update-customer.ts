import gql from 'graphql-tag'

export const MUTATION_UPDATE_CUSTOMER = gql`
  mutation($updateCustomerInput: UpdateCustomerInput!) {
    updateCustomer(updateCustomerInput: $updateCustomerInput) {
      name
      tel
      email
      address
      createdAt
      updatedAt
      prize {
        id
        name
        rank
        rate
        quantity
      }
    }
  }
`
