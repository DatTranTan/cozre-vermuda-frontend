import gql from 'graphql-tag'

export const MUTATION_CONFIRM_PRIZE = gql`
  mutation ($prizeId: String!, $eventId: String!) {
    minusPrizeQuantity(prizeId: $prizeId, eventId: $eventId)
  }
`
