import gql from 'graphql-tag'

export const MUTATION_DELETE_CUSTOMER = gql`
  mutation($ids: [String!]!) {
    deleteCustomers(ids: $ids)
  }
`
