import gql from 'graphql-tag'

export const MUTATION_CHANGE_PRIZE_STATUS = gql`
  mutation($prizeStatus: PrizeStatus!, $customerId: String!) {
    updatePrizeStatus(prizeStatus: $prizeStatus, customerId: $customerId) {
      prizeStatus
    }
  }
`
