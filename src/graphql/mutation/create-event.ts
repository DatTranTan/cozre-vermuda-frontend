import gql from "graphql-tag";

export const MUTATION_CREATE_EVENT = gql`
  mutation ($createEventInput: CreateEventInput!) {
    createEvent(createEventInput: $createEventInput) {
      id
      name
      startTime
      endTime
      memo
      timeOut
      spinLimit
      txtBtnWin
      txtBtnLost
      videoBackground
      video {
        id
        name
        url
      }
      noPrizeVideo {
        id
        name
        url
      }
      banner
      bannerUrl
      footerBanner
      footerBannerUrl
      prizes {
        id
        name
        imageUrl
        rate
        quantity
        url
      }
      owner {
        id
        nameKanji
      }
      questionnaire {
        id
        question
        type
        content
      }
      customerLost
      template {
        id
        name
      }
      campaignContent
    }
  }
`;
