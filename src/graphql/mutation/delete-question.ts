import gql from 'graphql-tag'

export const DELETE_QUESTIONNAIRE = gql`
  mutation($ids: [String!]!) {
    deleteQuestionnaires(ids: $ids)
  }
`
