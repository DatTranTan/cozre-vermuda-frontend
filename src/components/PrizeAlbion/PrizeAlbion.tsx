import React from 'react'
import PropTypes from 'prop-types'
import {
  WrapperPrize,
  Contents,
  ContentTop,
  ContentBot,
  BlockImage
} from './PrizeAlbion.style'

import ImageNull from '../../assets/images/box2.png'
function PrizeAlbion(props) {
  // const Data = { value: '1', namePrize: ' 景品1', numberPrize: '1名様' }
  // console.log('testprizename', props?.namePrize.length)
  // var img = document.getElementById('imageid')
  // useEffect(() => {
  //   var width = img?.clientWidth
  //   var height = img?.clientHeight
  //   console.log(width, '-', height)
  // }, [img])
  return (
    <WrapperPrize>
      <Contents>
        <ContentTop>{props.namePrize}</ContentTop>
        <ContentBot>
          {props.rank !== 0 ? `${props.numberPrize} 名様` : ''}
        </ContentBot>
      </Contents>
      <BlockImage>
        <img
          // id='imageid'
          src={props.imagePrize ? props.imagePrize : ImageNull}
          alt='imagePrize'
          style={{
            width: '100%',
            height: '100%',
            objectFit: 'unset'
          }}
        />
      </BlockImage>
    </WrapperPrize>
  )
}

PrizeAlbion.propTypes = {
  namePrize: PropTypes.string,
  numberPrize: PropTypes.string,
  imagePrize: PropTypes.string
}

export default PrizeAlbion
