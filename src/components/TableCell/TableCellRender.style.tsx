import styled from 'styled-components'

export const VideoURL = styled.a`
  @media (max-width: 1024px) {
    width: 100%;
    overflow : hidden;
    textOverflow: ellipsis;
    whiteSpace: nowrap;
    display: block;
  }
` 
