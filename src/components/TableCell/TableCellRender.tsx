/* eslint-disable */
import React, { useState, useCallback } from 'react'
import { Tooltip, Popover, Button } from 'antd'
import dayjs from 'dayjs'
import InputPassword from '../Input/PasswordInput'
import { VideoURL } from './TableCellRender.style'
import { TestShowImage } from '../Uploader/BannerUploader.style'
import { useDrawerDispatch } from '../../context/DrawerContext'

var isJSON = require('is-json')

export const colTitleRender = (value: any) => (
  <div style={{ fontWeight: 700, textAlign: 'center' }}>{value}</div>
)

export const renderRowText = (value: any) => (
  <Tooltip placement='topLeft' title={value} key={value}>
    <div
      style={{
        overflow: 'hidden',
        textOverflow: 'ellipsis'
      }}
    >
      {value}
    </div>
  </Tooltip>
)

export const renderRowNumber = (value: any) => (
  <Tooltip placement='topRight' title={value} key={value}>
    <div
      style={{
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        textAlign: 'right'
      }}
    >
      {value}
    </div>
  </Tooltip>
)

export const renderRowImage = (value: any) => (
  <>
    {value && (
      <div
        style={{
          textAlign: 'center',
          display: 'flex',
          justifyContent: 'center'
        }}
      >
        <a>
          <TestShowImage src={value} />
        </a>
      </div>
    )}
  </>
)

export const renderRowCenter = (value: any) => (
  <Tooltip placement='top' title={value} key={value}>
    <div
      style={{
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        textAlign: 'center'
      }}
    >
      {value}
    </div>
  </Tooltip>
)

export const renderRowLink = (value: any) => (
  <Tooltip placement='top' title={value} key={value}>
  <a
    style={{
      overflow: 'hidden',
      textOverflow: 'ellipsis'
    }}
    href={value}
    target='_blank'
  >
    {value}
  </a>
  </Tooltip>
)

export const renderURL = (value: any) => (
  <Tooltip placement='topLeft' title={value} key={value}>
    <VideoURL href={value} target='_blank'>
      {decodeURI(value)}
    </VideoURL>{' '}
  </Tooltip>
)

export const renderCreateDate = (value: any) => (
  <Tooltip
    placement='top'
    title={dayjs(parseFloat(value)).format('YYYY.MM.DD')}
  >
    <div
      style={{
        overflow: 'hidden',
        textOverflow: 'ellipsis'
      }}
    >
      {dayjs(parseFloat(value)).format('YYYY.MM.DD')}
    </div>
  </Tooltip>
)

export const renderPass = (value: any) => (
  <InputPassword size='compact' disabled value={value} />
)

export const renderMaximum = (value: any) => {
  if (value < 0)
    return (
      <Tooltip placement='top' title={'なし'} key={value}>
        <div
          style={{
            overflow: 'hidden',
            textOverflow: 'ellipsis',
            textAlign: 'center'
          }}
        >
          なし
        </div>
      </Tooltip>
    )
  return (
    <Tooltip placement='top' title={value} key={value}>
      <div
        style={{
          overflow: 'hidden',
          textOverflow: 'ellipsis',
          textAlign: 'center'
        }}
      >
        {value}
      </div>
    </Tooltip>
  )
}

const type = {
  First: '選択形式(複数選択)',
  Second: '選択形式(単一選択)',
  Third: '自由入力形式'
}

export const renderContentCampaign = (value: any) => {
  const dispatch = useDrawerDispatch()

  const openCampaignDetail = useCallback(
    () => {
      dispatch({
        type: 'OPEN_DRAWER',
        drawerComponent: 'CAMPAIGN_DETAIL',
        data: {value},
      })
    },
    [dispatch]
  )

  return(
    <Tooltip placement='top' title={''} key={value}>
        <div
          style={{
            overflow: 'hidden',
            textOverflow: 'ellipsis',
            textAlign: 'center',
            cursor: 'pointer'
          }}
          onClick={openCampaignDetail}
        >
          <a>詳細を見る</a>
        </div>
      </Tooltip>
  )
}

export const renderRowPopover = (value: any) => {
  const [visible, setVisible] = useState(false)
  return (
    <Tooltip placement='top' title={'詳細を見る'} key={value}>
      <div
        style={{
          whiteSpace: 'nowrap',
          overflow: 'hidden',
          textOverflow: 'ellipsis',
          textAlign: 'center',
          cursor: 'pointer'
        }}
      >
        <Popover
          style={{ maxHeight: '500px !important' }}
          placement='leftTop'
          trigger='click'
          visible={visible}
          onVisibleChange={() => setVisible(!visible)}
          overlayStyle={{
            maxHeight: '500px',
            overflowY: 'scroll'
          }}
          content={value.map((item, index) => {
            return (
              <div
                style={{
                  marginTop: 15,
                  maxWidth: 500
                }}
              >
                <p
                  style={{
                    fontWeight: 700
                  }}
                >
                  アンケート項目{index + 1}
                </p>
                <p
                  style={{
                    paddingLeft: 20,
                    wordBreak: 'break-all'
                  }}
                >
                  項目名 : {item?.question}
                </p>
                <p
                  style={{
                    paddingLeft: 20
                  }}
                >
                  入力形式 : {type[`${item?.type}`]}
                </p>
                {item?.type !== 'Third' ? (
                  <p
                    style={{
                      paddingLeft: 20,
                      wordBreak: 'break-all'
                    }}
                  >
                    選択項目: {item?.content}
                  </p>
                ) : (
                  <p
                    style={{
                      paddingLeft: 20
                    }}
                  >
                    必須入力 :{' '}
                    {item?.type !== 'Third'
                      ? item?.content
                      : item?.content?.toUpperCase() === 'FALSE'
                      ? '必須ではない'
                      : '必須'}
                  </p>
                )}
              </div>
            )
          })}
        >
          <a>詳細を見る</a>
        </Popover>
      </div>
    </Tooltip>
  )
}

export const renderRowPopoverCustomer = (value: any) => {
  const [visible, setVisible] = useState(false)
  const List = []
  if (value.length) {
    if (isJSON.strict(value)) {
      let newItem: any = JSON.parse(value)
      if (newItem.length > 0) {
        newItem.map((item) => List.push(item))
      }
    } else {
      for (let i = 0; i < value.length; i = i + 2) {
        List.push({ question: value[i], answer: value[i + 1] })
      }
    }
  }

  return (
    <Tooltip placement='top' title={'詳細を見る'} key={value}>
      <div
        style={{
          whiteSpace: 'nowrap',
          overflow: 'hidden',
          textOverflow: 'ellipsis',
          textAlign: 'center',
          cursor: 'pointer'
        }}
      >
        <Popover
          style={{ maxHeight: '500px !important' }}
          placement='leftTop'
          trigger='click'
          visible={visible}
          onVisibleChange={() => setVisible(!visible)}
          overlayStyle={{
            maxHeight: '500px',
            overflowY: 'scroll'
          }}
          content={
            List.map((item, index) => {
            return (
              <div
                style={{
                  marginTop: 15,
                  maxWidth: 500
                }}
              >
                <p
                  style={{
                    fontWeight: 700,
                    wordBreak: 'break-all'
                  }}
                >
                  項目名 : {item?.question}
                </p>
                <p
                  style={{
                    paddingLeft: 20,
                    wordBreak: 'break-all'
                  }}
                >
                  回答 :{' '}
                  {Array.isArray(item?.answer) 
                    ? item?.answer?.length > 0 ? item?.answer?.join() : item?.answer
                    : item?.answer || ''}
                </p>
              </div>
            )
          })
        }
        >
          <a>詳細を見る</a>
        </Popover>
      </div>
    </Tooltip>
  )
}
