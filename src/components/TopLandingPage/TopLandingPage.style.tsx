import { styled } from "baseui";

export const WrapperBlock = styled("div", () => ({
  width: "25%",
  display: "flex",
  justifyContent: "center",
  flexDirection: "column",
  alignItems: "center",
  marginTop: "2rem",
  boxShadow: "rgba(33,33,33,.06) 0 4px 24px 5px",
  marginLeft: "1rem",
  marginRight: "1rem",
  height: "300px",
  backgroundColor: "#fff",
  borderRadius: "30px",
  fontFamily: "Roboto",
  "@media only screen and (max-width: 1024px)": {
    width: "29%",
  },
  "@media only screen and (max-width: 768px)": {
    width: "40%",
    // ':first-child': {
    //   width: '100%'
    // }
  },
  "@media only screen and (max-width: 500px)": {
    width: "80%",
  },
}));

export const BlockTitle = styled("div", () => ({}));

export const BlockElement = styled("div", () => ({
  width: "100%",
  display: "flex",
  justifyContent: "center",
  flexDirection: "column",
  alignItems: "center",
}));

export const TopElement = styled("div", () => ({
  height: "20%",
  fontWeight: "600",
  fontSize: "1.4rem",
  color: "rgb(39, 38, 82)",
  marginBottom: "0.2rem",
}));

export const Content = styled("div", () => ({
  height: "20%",
  width: "70%",
  textAlign: "center",
  color: "red",
  fontWeight: "700",
}));
export const WrapperImg = styled("div", () => ({
  // width: '70%',
  // textAlign: 'center'
  height: "165px",
  width: "90%",
  padding: "0.5rem",

  // '@media only screen and (max-width: 1280px)': {
  //   width: '250px'
  // },
  // '@media only screen and (max-width: 320px)': {
  //   width: '240px'
  // }
}));
