import styled from "styled-components";

export const CustomQuill = styled.div`

    /* Set content for font-families */
    .ql-font-monospace,
    .ql-font span[data-value="monospace"]::before {
        font-family: Monaco, Courier New, monospace !important;
    }
    .ql-font-arial,
    .ql-font span[data-value="arial"]::before {
        font-family: Arial, sans-serif !important;
    }
    .ql-font-comic-sans,
    .ql-font span[data-value="comicsans"]::before {
        font-family: "Comic Sans MS", cursive, sans-serif !important;
    }
    .ql-font-courier-new,
    .ql-font span[data-value="couriernew"]::before {
        font-family: "Courier New" !important;
    }
    .ql-font-georgia,
    .ql-font span[data-value="georgia"]::before {
        font-family: Georgia, serif !important;
    }
    .ql-font-helvetica,
    .ql-font span[data-value="helvetica"]::before {
        font-family: Helvetica, sans-serif !important;
    }
    .ql-font-lucida,
    .ql-font span[data-value="lucida"]::before {
        font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif !important;
    }

    /* Set content for sizes */
    .ql-size-extra-small,
    .ql-size span[data-value="extra-small"]::before {
        font-size: 13px !important;
    }
    .ql-size-small,
    .ql-size span[data-value="small"]::before {
        font-size: 16px !important;
    }
    .ql-size-medium,
    .ql-size span[data-value="medium"]::before {
        font-size: 20px !important;
    }
    .ql-size-large,
    .ql-size span[data-value="large"]::before {
        font-size: 27px !important;
    }
`