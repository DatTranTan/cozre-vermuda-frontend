/* eslint-disable */
import React, { useEffect, useState, useCallback } from 'react'
import { useDropzone } from 'react-dropzone'
import { styled } from 'baseui'
import { UploadIcon } from '../AllSvgIcon'

const Text = styled('span', ({ $theme }) => ({
  ...$theme.typography.font14,
  fontFamily: $theme.typography.primaryFontFamily,
  color: $theme.colors.textDark,
  marginTop: '15px',
  textAlign: 'center'
}))

const TextHighlighted = styled('span', ({ $theme }) => ({
  color: $theme.colors.primary,
  fontWeight: 'bold'
}))

const Container = styled('div', ({ props }) => ({
  width: 'calc(100% - 100px)',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  padding: '30px',
  margin: '0 -8px',
  borderWidth: '2px',
  borderRadius: '2px',
  borderColor: '#E6E6E6',
  borderStyle: 'dashed',
  backgroundColor: '#ffffff',
  color: '#bdbdbd',
  outline: 'none',
  transition: 'border 0.24s ease-in-out',
  cursor: 'pointer'
}))
const ContainerVideo = styled('div', ({ props }) => ({
  width: '150px',
  flexDirection: 'column',
  alignItems: 'center',
  padding: '0.3rem',
  marginRight: '-8px',
  marginLeft: '1rem',
  borderWidth: '2px',
  borderRadius: '2px',
  borderColor: '#E6E6E6',
  borderStyle: 'dashed',
  backgroundColor: '#ffffff',
  color: '#bdbdbd',
  outline: 'none',
  transition: 'border 0.24s ease-in-out',
  cursor: 'pointer'
}))

const ThumbsContainer = styled('aside', () => ({
  display: 'flex',
  jutifyContent: 'center',
  flexDirection: 'row',
  flexWrap: 'wrap',
  marginTop: '0.2rem',
  marginRight: '-0.3rem',
  marginBottom: '-0.3rem',
  marginLeft: '0.2rem'
}))

const Thumb = styled('div', ({ $theme }) => ({
  ...$theme.borders.borderEA,
  display: 'inline-flex',
  borderRadius: '2px',
  marginBottom: '8px',
  marginRight: '8px',
  width: '130px',
  height: '130px',
  padding: '4px',
  boxSizing: 'border-box'
}))

const thumbInner = {
  display: 'flex',
  minWidth: 0,
  overflow: 'hidden'
}

const video = {
  display: 'block',
  width: 'auto',
  height: '100%'
}

function Uploader({ onChange, imageURL, disabled }: any) {
  const [files, setFiles] = useState(
    imageURL ? [{ name: 'demo', preview: imageURL }] : []
  )
  const { getRootProps, getInputProps } = useDropzone({
    accept: 'video/*',
    multiple: false,
    disabled: disabled,
    onDrop: useCallback(
      (acceptedFiles) => {
        setFiles(
          acceptedFiles.map((file) =>
            Object.assign(file, {
              preview: URL.createObjectURL(file)
            })
          )
        )
        // onChange(acceptedFiles)
      },
      [onChange]
    )
  })

  const thumbs = files.map((file) => (
    <Thumb key={file.name}>
      <div style={thumbInner}>
        <video src={file.preview} style={video} typeof={file.name} />
      </div>
    </Thumb>
  ))

  useEffect(
    () => () => {
      // Make sure to revoke the data uris to avoid memory leaks
      files.forEach((file) => URL.revokeObjectURL(file.preview))
    },
    [files]
  )

  // useEffect(() => {
  //   // Make sure to revoke the data uris to avoid memory leaks
  //   if (isReset) {
  //     setFiles([])
  //     dispatch({
  //       type: 'RESET_PRIZE_FIELDS',
  //       isReset: false
  //     })
  //   }
  // }, [isReset])
  return (
    <section className='container uploader'>
      <div style={{ maxWidth: '1184', width: '100%', display: 'flex' }}>
        <Container {...getRootProps()}>
          <input {...getInputProps()} />
          <UploadIcon />
          <Text>
            <TextHighlighted>
              ここにクリック、あるいは動画をドラッグ＆ドロップしてください。
            </TextHighlighted>
          </Text>
        </Container>
        <ContainerVideo>
          {thumbs && <ThumbsContainer>{thumbs}</ThumbsContainer>}
        </ContainerVideo>
      </div>
    </section>
  )
}

export default Uploader
