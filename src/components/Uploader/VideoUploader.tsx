/* eslint-disable */
import { CloseOutlined, LoadingOutlined } from '@ant-design/icons'
import { Progress } from 'antd'
import React, { useEffect, useState, useCallback } from 'react'
import { useDropzone } from 'react-dropzone'
import { CloseIcon, UploadIcon } from '../AllSvgIcon'
import { Notification } from '../Notification/NotificationCustom'
import { TestShowImage } from './BannerUploader.style'

import {
  LoadingContainer,
  VideoUpContainer,
  Text,
  TextHighlighted,
  Thumb,
  ThumbInner,
  ThumbsContainer,
  Video,
  ThumbButton,
  ImageName,
  FileName,
  FileSize,
  VideoName
} from './Uploader.style'

type Props = {
  onChange?: any
  disabled?: boolean
  onDelete?: any
  videoURL?: string
  progress: object
}
const VideoUploader: React.FC<Props> = ({
  onChange,
  disabled,
  onDelete,
  videoURL,
  progress: { loaded, total, unit }
}: any) => {
  const [files, setFiles] = useState(
    videoURL ? [{ name: 'demo', preview: videoURL }] : []
  )
  const { getRootProps, getInputProps } = useDropzone({
    accept: 'video/*',
    multiple: false,
    disabled: disabled,
    onDrop: useCallback(
      (acceptedFiles) => {
        if (acceptedFiles.length === 0) {
          Notification({
            type: 'error',
            message: ' エラー',
            description: 'このファイル種類のサポートがありません。'
          })
          return
        }
        setFiles(
          acceptedFiles.map((file) =>
            Object.assign(file, {
              preview: URL.createObjectURL(file)
            })
          )
        )
        onChange(acceptedFiles)
      },
      [onChange]
    )
  })
  const deleteVideo = () => {
    setFiles([])
    onDelete()
  }
  const progressPercent = total ? `${loaded}/${total} ${unit}` : '0/0'
  const totalPercent = total && `${total} ${unit}`
  let progressPercentage = Math.round((loaded / total) * 100)
  const thumbs = files.map((file, index) => (
    <Thumb key={file.name}>
      <ThumbInner>
        <Video src={file.preview} typeof={file.name} />
      </ThumbInner>
      {files.length > 0 && !disabled && (
        <ThumbButton onClick={deleteVideo}>
          <CloseOutlined style={{ fontSize: '12px' }} />
        </ThumbButton>
      )}
    </Thumb>
  ))
  useEffect(
    () => () => {
      // Make sure to revoke the data uris to avoid memory leaks
      files.forEach((file) => URL.revokeObjectURL(file.preview))
    },
    [files]
  )
  useEffect(() => {
    // Make sure to revoke the data uris to avoid memory leaks
    if (!!videoURL)
      setFiles([{ name: videoURL?.split('/')[5], preview: videoURL }])
  }, [videoURL])

  return (
    <section className='uploader'>
      <VideoUpContainer {...getRootProps()}>
        <input {...getInputProps()} />
        <UploadIcon />
        <Text>
          <TextHighlighted>
            ここをクリック、あるいは動画をドラッグ＆ドロップしてください。
          </TextHighlighted>
        </Text>
      </VideoUpContainer>
      {files.length > 0 && (
        <div style={{ display: 'flex', flexDirection: 'row' }}>
          <Thumb key={files[0].name}>
            <ThumbInner>
              <Video src={files[0].preview} typeof={files[0].name} />
            </ThumbInner>
            {!disabled && (
              <ThumbButton onClick={deleteVideo}>
                <CloseOutlined width={6} height={6} />
              </ThumbButton>
            )}
          </Thumb>
          {disabled ? (
            <LoadingContainer>
              <FileSize>{progressPercent}</FileSize>
              <Progress percent={progressPercentage} status='active' />
            </LoadingContainer>
          ) : (
            <VideoName href={files[0]?.preview} target={'_blank'}>
              <FileName>
                {files[0].name}&nbsp;{totalPercent.length > 0 && `(${totalPercent})`}
              </FileName>
            </VideoName>
          )}
        </div>
      )}
    </section>
  )
}

export default VideoUploader
