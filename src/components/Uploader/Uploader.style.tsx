import { styled } from 'baseui'

export const Text = styled('span', ({ $theme }) => ({
  ...$theme.typography.font14,
  fontFamily: $theme.typography.primaryFontFamily,
  color: $theme.colors.textDark,
  margin: '0 5px',
  textAlign: 'center',
  display: 'flex',
  flexDirection: 'column'
}))

export const TextHighlighted = styled('span', ({ $theme }) => ({
  color: $theme.colors.primary,
  fontWeight: 'bold'
}))
export const PrizeHighlighted = styled('span', ({ $theme }) => ({
  color: $theme.colors.primary,
  fontWeight: 'bold',
  padding: '0 1rem'
}))

export const BannerContainer = styled('div', ({ props }) => ({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: '2px',
  borderColor: '#E6E6E6',
  borderStyle: 'dashed',
  backgroundColor: '#ffffff',
  color: '#bdbdbd',
  outline: 'none',
  transition: 'border 0.24s ease-in-out',
  cursor: 'pointer',
  height: '100px',
  width: '100%'
}))

export const PrizeContainer = styled('div', ({ props }) => ({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: '2px',
  borderColor: '#E6E6E6',
  borderStyle: 'dashed',
  borderLeft: 0,
  backgroundColor: '#ffffff',
  color: '#bdbdbd',
  outline: 'none',
  transition: 'border 0.24s ease-in-out',
  cursor: 'pointer',
  height: '48px',
  width: '100%'
}))

export const CloseButton = styled('button', ({ $theme }) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  flexDirection: 'column',
  cursor: 'pointer',
  width: '25px',
  height: '25px',
  borderRadius: '50%',
  color: 'red',
  outline: '0',
  border: 'none',
  // marginLeft: '0.5rem'
}))

export const FileItemWrapper = styled('div', ({ $theme }) => ({
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'space-between',
  marginTop: '10px',
  fontSize: '15px',
  border: '1px solid #ddd',
  padding: '5px 10px'
}))

export const ImageName = styled('a', ({ $theme }) => ({
  display: 'flex',
  maxWidth: '90%',
  widht: '90%',
  overflow: 'hidden',
  textOverflow: 'ellipsis',
  whiteSpace: 'nowrap',
  marginRight: '10px'
}))

// export const TestShowImage = styled('img', ({ $theme }) => ({
//   width: '40px',
//   height: '40px',
//   objectFit: 'cover',
//   marginRight: '10px'
// }))

export const VideoName = styled('a', ({ $theme }) => ({
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'center',
  marginTop: '10px',
  width: 'calc(100% - 90px)'
}))

export const FileName = styled('span', ({ $theme }) => ({
  overflow: 'hidden',
  textOverflow: 'ellipsis',
  whiteSpace: 'nowrap',
  display: 'flex',
  alignItems: 'center'
}))

export const FileSize = styled('span', ({ $theme }) => ({
  // alignItems: 'base-line',
  display: 'flex',
  alignItems: 'center'
}))

export const ButtonContainer = styled('a', ({ $theme }) => ({
  display: 'flex',
  justifyContent: 'flex-end',
  padding: '5px'
}))

export const CloseContainer = styled('a', ({ $theme }) => ({
  width: '15px',
  height: '15px'
}))
export const LoadingContainer = styled('div', ({ $theme }) => ({
  display: 'flex',
  flexDirection: 'column',
  marginTop: '10px',
  alignItems: 'center',
  justifyContent: 'center',
  height: '75px',
  width: 'calc(100% - 90px)'
}))
export const VideoUpContainer = styled('div', ({ props }) => ({
  width: '100%',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  padding: '30px',
  borderWidth: '2px',
  borderRadius: '2px',
  borderColor: '#E6E6E6',
  borderStyle: 'dashed',
  backgroundColor: '#ffffff',
  color: '#bdbdbd',
  outline: 'none',
  transition: 'border 0.24s ease-in-out',
  cursor: 'pointer'
}))
export const ContainerVideo = styled('div', ({ props }) => ({
  width: '100%',
  position: 'relative',
  flexDirection: 'column',
  alignItems: 'center',
  padding: '0.3rem',
  marginLeft: '1rem',
  borderWidth: '2px',
  borderRadius: '2px',
  borderColor: '#E6E6E6',
  borderStyle: 'dashed',
  backgroundColor: '#ffffff',
  color: '#bdbdbd',
  outline: 'none',
  transition: 'border 0.24s ease-in-out',
  cursor: 'pointer'
}))

export const ThumbsContainer = styled('aside', () => ({
  display: 'flex',
  jutifyContent: 'center',
  flexDirection: 'row',
  flexWrap: 'wrap',
  marginTop: '0.2rem',
  marginRight: '-0.3rem',
  marginBottom: '-0.3rem',
  marginLeft: '0.2rem'
}))

export const Thumb = styled('div', ({ $theme }) => ({
  ...$theme.borders.borderEA,
  display: 'inline-flex',
  position: 'relative',
  borderRadius: '2px',
  marginTop: '10px',
  marginRight: '10px',
  width: '75px',
  height: '75px',
  padding: '4px',
  boxSizing: 'border-box'
}))

export const ThumbInner = styled('div', ({ $theme }) => ({
  display: 'flex',
  minWidth: 0,
  overflow: 'hidden'
}))

export const Video = styled('video', ({ $theme }) => ({
  display: 'block',
  // width: '75px',
  height: '75px',
}))

export const ThumbButton = styled('button', ({ $theme }) => ({
  position: 'absolute',
  top: '7px',
  right: '7px',
  outline: '0',
  border: 'none',
  padding: '0',
  display: 'flex',
  margin: 'auto',
  alignItems: 'center',
  justifyContent: 'center',
  cursor: 'pointer',
  // backgroundColor: 'rgb(0,0,0,0.35)',
  width: '18px',
  height: '18px',
  borderRadius: '50%',
  color: 'red'
}))
export const LabelUpload = styled('div', ({ $theme }) => ({
  height: '48px',
  minWidth: '100px',
  borderRadius: '0',
  backgroundColor: 'rgb(221,221,221)',
  justifyContent: 'center',
  alignItems: 'center',
  display: 'flex',
  zIndex: 1
}))
