import * as React from 'react'
import { Select } from 'baseui/select'
import { CarretDownIcon } from '../AllSvgIcon'

export const getContainerFontStyle = ({ $theme }) => {
  return $theme.typography.fontBold14
}
export default ({ ...props }) => {
  return (
    <Select
      noResultsMsg='データがありません。'
      overrides={{
        SelectArrow: () => {
          return <CarretDownIcon />
        },
        Popover: {
          props: {
            overrides: {
              Body: {
                style: { zIndex: 1 }
              }
            }
          }
        },
        Input: {
          style: ({ $theme, $isFocused, $width, $value }) => ({
            width: $width !== "19px" ? $width : "2px",
          }),
        },
        Placeholder: {
          style: ({ $theme }) => ({
            ...getContainerFontStyle({ $theme })
          })
        },
        SingleValue: {
          style: ({ $theme }) => ({
            ...getContainerFontStyle({ $theme }),
            color: $theme.colors.textDark,
            lineHeight: '1.5'
          })
        },
        DropdownListItem: {
          style: ({ $theme }) => ({
            fontSize: $theme.typography.fontBold14,
            color: $theme.colors.textDark,
            overflowX: 'hidden'
          })
        },
        OptionContent: {
          style: ({ $theme, $selected }) => {
            return {
              ...$theme.typography.fontBold14,
              color: $selected
                ? $theme.colors.textDark
                : $theme.colors.textNormal
            }
          }
        },
        DropdownOption: {
          style: ({ $theme }) => ({
            ...$theme.typography.fontBold14,
            color: $theme.colors.textDark
          })
        },
        ClearIcon: {
          props: {
          title:"データ削除",
          }
        }
      }}
      {...props}
    />
  )
}
