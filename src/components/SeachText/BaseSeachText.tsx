import React from 'react'
import { Input } from 'baseui/input'
import { styled } from 'baseui'
import { SearchOutlined } from '@ant-design/icons'
import { SIZE } from "baseui/select";

const getInputFontStyle = ({ $theme }) => {
  return {
    color: $theme.colors.textDark,
    ...$theme.typography.fontBold14,
    ":-ms-input-placeholder": { /* Internet Explorer 10-11 */
      color: 'gray'
     }
  }
}

const BaseSearch = styled(Input, ({ $theme }) => ({
  ...$theme.typography.fontBold18,
  color: $theme.colors.textDark,
  margin: 0
}))

const SearchText = ({ setText, setPageIndex, pageIndex }) => {
  const debounce = (fn: any, delay: number) => {
    return (args: any) => {
      clearTimeout(fn.id)

      fn.id = setTimeout(() => {
        fn.call(this, args)
      }, delay)
    }
  }

  const searchHandler = (value: any) => {
    if (pageIndex && pageIndex !== 0) setPageIndex(0)
    setText(value)
  }

  const debounceAjax = debounce(searchHandler, 600)

  return (
    <BaseSearch
      placeholder='検索する'
      size={SIZE.compact}
      onChange={(e: any) => {
        debounceAjax(e.target.value)
      }}
      clearable
      endEnhancer={<SearchOutlined />}
      overrides={{
        Root: {
          style: ({ $theme, $isFocused }) => ({
            border: $isFocused
              ? `2px solid ${$theme.colors.primary}`
              : '2px solid #EEEEEE',
            background: $isFocused ? '#F6F6F6' : '#EEEEEE',
            height: '40px'
          })
        },
        InputContainer: {
          style: ({ $theme, $isFocused }) => ({
            borderTopColor: $isFocused ? '#F6F6F6' : '#EEEEEE',
            borderBottomColor: $isFocused ? '#F6F6F6' : '#EEEEEE',
            borderLeftColor: $isFocused ? '#F6F6F6' : '#EEEEEE',
            borderRightColor: $isFocused ? '#F6F6F6' : '#EEEEEE'
          })
        },
        Input: {
          style: ({ $theme }) => {
            return {
              ...getInputFontStyle({ $theme }),
            }
          }
        },
        EndEnhancer: {
          style: ({ $theme, $isFocused }) => ({
            paddingLeft: 0,
            color: $isFocused ? '#000' : $theme.colors.textDark,
            backgroundColor: $isFocused ? '#F6F6F6' : '#EEEEEE',
            borderTopColor: $isFocused ? '#F6F6F6' : '#EEEEEE',
            borderBottomColor: $isFocused ? '#F6F6F6' : '#EEEEEE',
            borderLeftColor: $isFocused ? '#F6F6F6' : '#EEEEEE',
            borderRightColor: $isFocused ? '#F6F6F6' : '#EEEEEE'
          })
        }
      }}
    />
  )
}

export default SearchText
