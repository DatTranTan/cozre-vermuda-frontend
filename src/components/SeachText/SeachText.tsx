import React from 'react'
import Search from 'antd/lib/input/Search'
import { styled } from 'baseui'

const BaseSearch = styled(Search, ({ $theme }) => ({
  ...$theme.typography.fontBold18,
  color: $theme.colors.textDark,
  margin: 0
}))

const SeachText = ({ setText, setPageIndex, pageIndex }) => {
  const debounce = (fn, delay) => {
    return (args) => {
      clearTimeout(fn.id)

      fn.id = setTimeout(() => {
        fn.call(this, args)
      }, delay)
    }
  }

  const searchHandler = (value) => setText(value)

  const debounceAjax = debounce(searchHandler, 1000)

  return (
    <BaseSearch
      // style={{ width: window.innerWidth <= 1440 ? '90%' : '100%' }}
      style={{ width: '100%' }}
      placeholder='検索する'
      onChange={(e) => {
        if (pageIndex && pageIndex !== 0) setPageIndex(0)
        debounceAjax(e.target.value)
      }}
      allowClear
    />
  )
}

// SeachText.propTypes = {
//   setText: PropTypes.func,
//   setPageIndex: PropTypes.func,
//   pageIndex: PropTypes.number
// }

export default SeachText
