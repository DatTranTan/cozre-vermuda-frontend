import React from "react";
import PropTypes from "prop-types";
import { WrapperPrize } from "./PrizeAlbion.style";
import { Card } from "antd";
import $ from 'jquery'

const { Meta } = Card;
function PrizeAlbion(props) {
  const { image, description, name, quantity , key} = props.productInfo;
  let bodyHeight = 0
  const a = $( ".ant-card-body" ).each(function() {
    let number = $(this).height()
    if(number > bodyHeight){
      bodyHeight = number
    } 
 });

  return (
    <>
      <WrapperPrize bodyHeight={bodyHeight+48}>
        <Card
          style={{ height: "100%", border: "none", margin: "auto" }}
          cover={<img className="card-image" src={image} alt="imagePrize" />}
          actions={[
            <h2
              key="setting"
              className="header-text-number"
              style={{
                textAlign: "left",
                paddingLeft: "24px",
                fontWeight: 900,
                color: "rgb(103 103 103)",
              }}
            >
              {quantity}名様
            </h2>,
          ]}
        >
          <Meta
            title={
              <h2
                className="header-text"
                style={{ fontWeight: 900, textAlign: "left" }}
              >
                {name}
              </h2>
            }
            description={Array.isArray(description)?description.map(item => <>{item}<br/></>):description}
          />
        </Card>
      </WrapperPrize>
    </>
  );
}

PrizeAlbion.propTypes = {
  productInfo: PropTypes.any,
};

export default PrizeAlbion;
