import styled from "styled-components";
import { isDesktop } from "react-device-detect";

const WrapperPrize = styled.div`
  @media (max-width: 1110px) {
      padding: 20px 0px;
    }
  .card-image {
    width: 100%;
    object-fit: cover;
    /* height: 430px;
    @media (max-width: 1750px) {
      height: 390px;
    }
    @media (max-width: 1550px) {
      height: 340px;
    }
    @media (max-width: 1300px) {
      height: 280px;
    }
    @media (max-width: 1110px) {
      height: 230px;
    }
    @media (max-width: 950px) {
      height: ${isDesktop ? "620px" : "561px"};
    }
    @media (max-width: 780px) {
      height: ${isDesktop ? "620px" : "531px"};
    }
    @media (max-width: 500px) {
      height: ${isDesktop ? "620px" : "260px"};
    } */
  }
  .header-text{
    font-size: 1.5rem;
    color: #333;
    @media (max-width: 1350px) {
        font-size: 1.25rem;
      }
      /* @media (max-width: 1110px) {
        font-size: 1.05rem;
      }  */
      @media (max-width: 950px) {
        font-size: 1.8rem;
      } 
      @media (max-width: 500px) {
        font-size: 1.2rem;
      } 
  }
  .header-text-number{
    font-size: 1.25rem;
    @media (max-width: 1350px) {
        font-size: 1rem;
      }
      /* @media (max-width: 1110px) {
        font-size: 1.2rem;
      }  */
      @media (max-width: 950px) {
        font-size: 1.5rem;
      } 
      @media (max-width: 500px) {
        font-size: 1rem;
      } 
  }
  .ant-card-meta-description{
    font-size: 1.2rem;
    @media (max-width: 1350px) {
        font-size: 0.8rem;
      }
      @media (max-width: 1110px) {
        font-size: 0.8rem;
      } 
      @media (max-width: 950px) {
        font-size: 1.5rem;
      } 
      @media (max-width: 500px) {
        font-size: 1rem;
      } 
  }
  .ant-card-meta-title{
    white-space: break-spaces;
  }
  .ant-card-body{
    // week1
    /* min-height: 210px;
    @media (max-width: 1750px) {
      min-height: 210px;
      }
    @media (max-width: 1500px) {
      min-height: 210px;
      }
    @media (max-width: 1350px) {
      min-height: 180px;
      }
      @media (max-width: 1110px) {
        min-height: ${isDesktop ? "200px" : "200px"};
      } 
      @media (max-width: 950px) {
        min-height: 225px;
      } 
      @media (max-width: 500px) {
        min-height: 175px;
      }  */
      // week2
          /* min-height: 230px;
    @media (max-width: 1750px) {
      min-height: 230px;
      }
    @media (max-width: 1500px) {
      min-height: 230px;
      }
    @media (max-width: 1350px) {
      min-height: 190px;
      }
      @media (max-width: 1110px) {
        min-height: ${isDesktop ? "200px" : "200px"};
      } 
      @media (max-width: 950px) {
        min-height: 225px;
      } 
      @media (max-width: 500px) {
        min-height: 175px;
      }  */
      // week3
          /* min-height: 300px;
    @media (max-width: 1750px) {
      min-height: 300px;
      }
    @media (max-width: 1500px) {
      min-height: 360px;
      }
    @media (max-width: 1350px) {
      min-height: 280px;
      }
      @media (max-width: 1110px) {
        min-height: ${isDesktop ? "324px" : "324px"};
      } 
      @media (max-width: 950px) {
        min-height: 225px;
      } 
      @media (max-width: 500px) {
        min-height: 175px;
      }  */
      // week4
      min-height: ${prop => `${prop.bodyHeight}px`};
      @media (max-width: 950px) {
        min-height: 225px;
      } 
      @media (max-width: 500px) {
        min-height: 175px;
      } 
  }
  .ant-card-actions{
    border: none;
  }
  .ant-card{
    width: 92%;
    @media (max-width: 900px) {
      width: 100%;
      } 
  }
`;
const WrapperPrizeSpin = styled.div`
  @media (max-width: 1110px) {
      padding: 20px 0px;
    }
  .card-image {
    width: 100%;
    object-fit: cover;
    /* height: 430px;
    @media (max-width: 1750px) {
      height: 390px;
    }
    @media (max-width: 1550px) {
      height: 340px;
    }
    @media (max-width: 1300px) {
      height: 280px;
    }
    @media (max-width: 1110px) {
      height: 230px;
    }
    @media (max-width: 950px) {
      height: ${isDesktop ? "620px" : "561px"};
    }
    @media (max-width: 780px) {
      height: ${isDesktop ? "620px" : "531px"};
    }
    @media (max-width: 500px) {
      height: ${isDesktop ? "620px" : "260px"};
    } */
  }
  .header-text{
    font-size: 1.5rem;
    color: #333;
    @media (max-width: 1350px) {
        font-size: 1.25rem;
      }
      /* @media (max-width: 1110px) {
        font-size: 1.05rem;
      }  */
      @media (max-width: 950px) {
        font-size: 1.8rem;
      } 
      @media (max-width: 500px) {
        font-size: 1.2rem;
      } 
  }
  .ant-card-meta-description{
    font-size: 1.2rem;
    @media (max-width: 1300px) {
        font-size: 0.8rem;
      }
      @media (max-width: 1110px) {
        font-size: 0.8rem;
      } 
      @media (max-width: 950px) {
        font-size: 1.5rem;
      } 
      @media (max-width: 500px) {
        font-size: 1rem;
      } 
  }
  .ant-card-meta-title{
    white-space: break-spaces;
  }
`;
const Contents = styled.div`
  width: 100%;
  /* height: 100%; */
  display: flex;
  justify-content: center;
  align-items: center;
  flex-flow: column;
  font-family: Hiragino Sans;
`;
const ContentTop = styled.div`
  text-align: center;
  font-size: 2rem;
  font-weight: 900;
  color: #707070;
  max-width: 80%;
  overflow: hidden;
  text-overflow: ellipsis;
  word-break: break-word;
  display: -webkit-box;

  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
  @media (max-width: 1500px) {
    font-size: 1.9rem;
  }
  @media (max-width: 1300px) {
    font-size: 1.5rem;
  }
  @media (max-width: 1110px) {
    font-size: 1.2rem;
  }
  @media (max-width: 950px) {
    font-size: ${isDesktop ? "1.1rem" : "1.4rem"};
  }
  @media (max-width: 850px) {
    font-size: 1.4rem;
  }
  @media (max-width: 500px) {
    margin-top: 15px;
    font-size: 0.8rem;
  }
`;
const ContentBot = styled.div`
  font-size: 1.5rem;
  font-weight: 700;
  color: #ffcc5a;
  @media (max-width: 1500px) {
    font-size: 1.5rem;
  }
  @media (max-width: 1300px) {
    font-size: 1.5rem;
  }
  @media (max-width: 1110px) {
    font-size: 1.2rem;
  }
  @media (max-width: 950px) {
    font-size: ${isDesktop ? "1.1rem" : "1.4rem"};
  }
  @media (max-width: 850px) {
    font-size: 1.4rem;
  }
  @media (max-width: 500px) {
    font-size: 0.8rem;
  }
`;
const BlockImage = styled.div`
  width: 80%;
  height: 65%;
  @media (max-width: 1500px) {
    height: 60%;
  }
  @media (max-width: 1300px) {
    height: 61%;
  }
  @media (max-width: 1110px) {
    height: 57%;
  }
  @media (max-width: 950px) {
    height: ${isDesktop ? "58%" : "50%"};
  }
  @media (max-width: 850px) {
    height: 57%;
  }
  @media (max-width: 500px) {
    height: 66% !important;
    margin-top: 0;
  }

  img {
    @media (max-width: 3120px) {
      margin-top: -1.5rem;
    }
    @media (max-width: 1650px) {
      margin-top: -1rem;
    }
    @media (max-width: 850px) {
      margin-top: -1rem;
    }
    @media (max-width: 500px) {
      height: 90% !important;
      margin-top: 2px;
    }
  }
`;

export { WrapperPrize, Contents, ContentTop, ContentBot, BlockImage, WrapperPrizeSpin };
