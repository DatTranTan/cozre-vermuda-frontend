import React from 'react'
import { Datepicker } from 'baseui/datepicker'

const getInputFontStyle = ({ $theme }) => {
  return {
    color: $theme.colors.textDark,
    ...$theme.typography.fontBold14
  }
}

export const DatePicker = ({ ...props }) => {
  return (
    <Datepicker
      overrides={{
        Popover: {
          props: {
            overrides: {
              Body: {
                style: { zIndex: props.zIndex ? props.zIndex : 10 }
              }
            }
          }
        },
        Input: {
          props: {
            overrides: {
              Input: {
                style: ({ $theme }) => {
                  return {
                    ...getInputFontStyle({ $theme })
                  }
                }
              }
            }
          }
        },
        MonthYearSelectPopover: {
          props: {
            overrides: {
              Body: {
                style: ({ $theme }) => ({
                    zIndex: 10001,
                })
              }
            }
          }
        }
      }}
      {...props}
    />
  )
}
