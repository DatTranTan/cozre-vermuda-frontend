import React from 'react'
import {
  DeleteOutlined,
  EditOutlined,
  FullscreenOutlined,
  PlusCircleFilled,
  QrcodeOutlined,
  FundFilled,
  DownloadOutlined
} from '@ant-design/icons'
import { IconWrapper, Wrapper } from './ActionIcon.style'
import { SiteSettings } from '../AllSvgIcon'
import { useStyletron } from 'baseui'
type ActionProps = {
  disabled?: boolean
  color?: any
  margin?: any
  title?: any
  onClick?: any
}

const AddIcon: React.FC<ActionProps> = ({
  disabled,
  color,
  title,
  margin,
  onClick,
  ...props
}) => {
  const [css] = useStyletron()
  return (
    <PlusCircleFilled
      style={{
        fontSize: '1.35rem',
        margin: margin ? margin : '0 0.5rem 0 0',
        color: color
      }}
      className={css({
        ':hover': {
          transform: !disabled ? 'scale(1.2)' : '',
          transition: 'all 0.3s'
        }
      })}
      title={title}
      onClick={onClick}
    />
  )
}
const ExcelExportIcon: React.FC<ActionProps> = ({
  disabled,
  color,
  title,
  margin,
  onClick,
  ...props
}) => {
  const [css] = useStyletron()
  return (
    <DownloadOutlined
      style={{
        fontSize: '1.35rem',
        margin: margin ? margin : '0 0.5rem 0 0',
        color: color
      }}
      className={css({
        ':hover': {
          transform: !disabled ? 'scale(1.2)' : '',
          transition: 'all 0.3s'
        }
      })}
      title={title}
      onClick={onClick}
    />
  )
}
const AnalyticIcon: React.FC<ActionProps> = ({
  disabled,
  color,
  title,
  margin,
  onClick,
  ...props
}) => {
  const [css] = useStyletron()
  return (
    <FundFilled
      style={{
        fontSize: '1.35rem',
        margin: margin ? margin : '0 0.5rem 0 0',
        color: color
      }}
      className={css({
        ':hover': {
          transform: !disabled ? 'scale(1.2)' : '',
          transition: 'all 0.3s'
        }
      })}
      title={title}
      onClick={onClick}
    />
  )
}

const EditIcon: React.FC<ActionProps> = ({
  disabled,
  color,
  title,
  onClick,
  margin,
  ...props
}) => {
  const [css] = useStyletron()
  return (
    <EditOutlined
      style={{
        fontSize: '1.4rem',
        color: color,
        margin: margin ? margin : '0rem 0.5rem'
      }}
      className={css({
        ':hover': {
          transform: !disabled ? 'scale(1.2)' : '',
          transition: 'all 0.3s'
        }
      })}
      title={title}
      onClick={onClick}
    />
  )
}
const DeleteIcon: React.FC<ActionProps> = ({
  disabled,
  color,
  title,
  margin,
  onClick,
  ...props
}) => {
  const [css] = useStyletron()
  return (
    <DeleteOutlined
      style={{
        fontSize: '1.4rem',
        margin: margin ? margin : '0 0 0 0.5rem',
        color: color
      }}
      className={css({
        ':hover': {
          transform: !disabled ? 'scale(1.2)' : '',
          transition: 'all 0.3s'
        }
      })}
      onClick={onClick}
      title={title}
    />
  )
}

const ExportIcon: React.FC<ActionProps> = ({
  disabled,
  color,
  title,
  margin,
  onClick,
  ...props
}) => {
  const [css] = useStyletron()
  return (
    <QrcodeOutlined
      style={{
        fontSize: '1.4rem',
        color: color,
        margin: margin ? margin : '0 0 0 0.5rem'
      }}
      className={css({
        ':hover': {
          transform: !disabled ? 'scale(1.2)' : '',
          transition: 'all 0.3s'
        }
      })}
      onClick={onClick}
      title={title}
    />
  )
}

const FullScreenIcon: React.FC<ActionProps> = ({
  disabled,
  color,
  title,
  margin,
  onClick,
  ...props
}) => {
  const [css] = useStyletron()
  return (
    <FullscreenOutlined
      style={{
        fontSize: '1.4rem',
        color: color,
        margin: margin ? margin : '0 0 0 0.5rem'
      }}
      className={css({
        ':hover': {
          transform: !disabled ? 'scale(1.2)' : '',
          transition: 'all 0.3s'
        }
      })}
      title={'賞品'}
      onClick={onClick}
    />
  )
}
const StaticticIcon: React.FC<ActionProps> = ({
  disabled,
  color,
  title,
  margin,
  onClick,
  ...props
}) => {
  return (
    <Wrapper onClick={onClick}>
      <IconWrapper>
        <SiteSettings height={'1.4rem'} width={'1.4rem'} color={'#00C58D'} />
      </IconWrapper>
    </Wrapper>
  )
}
export {
  AddIcon,
  EditIcon,
  DeleteIcon,
  ExportIcon,
  FullScreenIcon,
  StaticticIcon,
  AnalyticIcon,
  ExcelExportIcon
}
