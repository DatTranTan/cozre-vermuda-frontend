import { styled } from 'baseui'

export const Wrapper = styled('div', ({ $theme, $isFocused }) => ({
  display: 'flex',
  justifyContent: 'flex-start',
  alignItems: 'center',
  width: '10%',
  paddingLeft: '1rem',

  '@media only screen and (max-width: 1024px)': {
    paddingLeft: '0.5rem'
  }
}))

export const IconWrapper = styled('div', ({ $theme, $isFocused }) => ({
  cursor: 'pointer',
  borderRadius: '50%',
  height: '2.4rem',
  width: '2.4rem',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  border: $isFocused ? `2px solid ${$theme.colors.primary}` : '2px solid #ccc',

  ':hover': {
    color: $theme.colors.warning,
    border: `0.1rem solid ${$theme.colors.primary}`,
    transform: 'scale(1.2)',
    transition: 'all 0.2s'
  }
}))
