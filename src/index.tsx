import "react-app-polyfill/ie11";
import "react-app-polyfill/stable";
import "core-js/stable/symbol";
import "isomorphic-unfetch";
import "core-js/features/string/repeat";
import smoothscroll from "smoothscroll-polyfill";
import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import { Client as Styletron } from "styletron-engine-atomic";
import { Provider as StyletronProvider } from "styletron-react";
import { BaseProvider } from "baseui";
import { theme } from "./theme";
import Routes from "./routes";
import LandingRoutes from "./landing-new-template/routes";
// import ApolloClient from 'apollo-boost'
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  createHttpLink,
  from
} from "@apollo/client";
import { onError } from "@apollo/client/link/error";
import { setContext } from "@apollo/client/link/context";
import * as serviceWorker from "./serviceWorker";
import "./theme/global.css";

import "core-js/features/string/repeat";
// import { Notification } from 'baseui/notification'
import { ConfigProvider } from "antd";
import jaJP from "antd/lib/locale/ja_JP";
import "antd/dist/antd.css";
import "dayjs/locale/ja";
import dayjs from "dayjs";

// import 'promise-polyfill/src/polyfill';
// const notify = (value) => {
//   return <Notification>{() => value}</Notification>
// }
const httpLink = createHttpLink({
  // eslint-disable-next-line no-undef
  uri: process.env.REACT_APP_API_URL
});

const authLink = setContext((_, { headers }) => {
  const token =
    (typeof window !== "undefined" && localStorage.getItem("access_token")) ||
    sessionStorage.getItem("access_token");

  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : ""
    }
  };
});

const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors)
    graphQLErrors.map(({ message, locations, path }) => {
      if (message === "Unauthorized") {
        localStorage.removeItem("access_token");
      }
      return console.log(
        `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
      );
    });
  if (networkError) console.log(`[Network error]: ${networkError}`);
});
// const client = new ApolloClient({
//   fetchOptions: { fetch },
//   uri: process.env.REACT_APP_API_URL,
//   request: (operation) => {
//     const token = localStorage.getItem('access_token')
//     operation.setContext({
//       headers: {
//         authorization: token ? `Bearer ${token}` : ''
//       }
//     })
//   },
//   onError: (props) => {
//     if (props.graphQLErrors[0]?.message === 'Unauthorized') {
//       setTimeout(() => {
//         window.location.replace('/')
//       }, 2000)
//       localStorage.removeItem('access_token')
//       notify('This is a notification.')
//     }
//   }
// })

function App() {
  smoothscroll.polyfill();
  if (!Element.prototype.matches) {
    Element.prototype.matches = Element.prototype["msMatchesSelector"];
  }
  const engine = new Styletron();
  dayjs.locale("ja");

  const client = new ApolloClient({
    cache: new InMemoryCache({
      typePolicies: {
        Query: {
          fields: {
            feed: {
              // Don't cache separate results based on
              // any of this field's arguments.
              keyArgs: false,
              // Concatenate the incoming list items with
              // the existing list items.
              merge(existing = [], incoming) {
                return [...existing, ...incoming];
              }
            }
          }
        }
      }
    }),
    link: from([errorLink, authLink, httpLink])
  });

  return (
    <ApolloProvider client={client as any}>
      <StyletronProvider value={engine}>
        <BaseProvider theme={theme}>
          <ConfigProvider locale={jaJP}>
            <BrowserRouter>
              {process.env.REACT_APP_PROJECT_NAME === "LANDING_PAGE" ? (
                <LandingRoutes />
              ) : (
                <Routes />
              )}
            </BrowserRouter>
          </ConfigProvider>
        </BaseProvider>
      </StyletronProvider>
    </ApolloProvider>
  );
}

ReactDOM.render(<App />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
